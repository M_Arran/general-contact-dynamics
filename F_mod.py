"""
file name: F_mod.py
language: Python3.6
date: 14/06/2018
author: Hugo Martin
email: martin.hugo@live.com
This module python contains the functions used to solve the friction problem.
"""

# imports
from gcd.classes import real_t

from gcd.get_F_DBT import get_DBT
import numpy as np
from sys import exit
from scipy.sparse import coo_matrix, eye, hstack, vstack, dia_matrix, csc_matrix, csr_matrix
from scipy.sparse import block_diag
from math import sqrt, pow
from sys import stdout
import mosek # the optimization solver
from time import time

# F_mod
def F_mod( Prm, Stn, Buff, Fmj ):
    """
    The functions F_mod makes a loop of Friction models posed over a set of contacts which
    is updated between two loop iterations.

    Arguments:
    Prm  : =
    Stn  : =
    Buff : =
    Fmj  : =
    """

    J = 0. #
    Tu     = 0. #
    diff   = 0. # initialization

    # for scaling
    if Prm.Gc:
        isqgd = 1./sqrt(Prm.Gc*(2.*Prm.SRf))
    else:
        isqgd = 1

    if not Prm.NPRO:

        # loop over sets of contacts
        while( Fmj.wtd ):
            # find new contacts, and update Dzvec, Bzmat and Tzmat
            get_DBT( Stn, Prm, Buff, Fmj )

            if Fmj.wtd: # if there is new contacts, we must work
                # make the matrices objects for penalized model
                make_pen( Prm, Stn, Fmj, isqgd )

                # solve friction model
                J = solve_pen( Fmj, Prm.SD, Prm.EPS, Prm.INFO )

                # update the configuration velocity
                np.multiply(Prm.DT, Fmj.MiAzt.dot(Fmj.lambdaz), out=Stn.u_o, dtype=real_t )
                np.subtract(Stn.Uvec, Stn.u_o, out=Stn.u_o, dtype=real_t)

                # Test post optimization problem
                Tu = isqgd*np.linalg.norm(Fmj.Tzmat.dot(Stn.u_o))

            else:
                Fmj.Tzmat = None
                Fmj.Dzvec = None

            # checks the number of iter
            check_loop( Fmj )

        # end of loop
        end_loop( ((Stn.PLOT)and(Fmj.Nbz>0)), Fmj.Nbz, J, Tu )

    else:
        Fmj.Tzmat = None
        Fmj.Dzvec = None
        Fmj.wtd   = False

# make_obj
def make_pen( Prm, Stn, Fmj, isqgd ):
    """
    The function make_obj makes the objects necessary to create a minimization problem.
    See below for their definitions.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
    Tzmat : sparse float matrix = the matrix T of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      Nbz : int = number of effective contacts. It is also the size of indi and indj,
     Uvec : float array = non optimized velocity configuration,
       NS :   int = number of discs,
       DT : float = time step value,
     iEPS : float = inverse of EPS,
       SM : float sparse matrix = mass matrix,
       MU : float = Coulomb's law friction coefficient.

    Returns:
       Az : sparse float matrix = the matrix involved in the linear constraint,
    MiAzt : sparse float matrix = see definition below,
       bz : float array = the vector involved in the linear constraint.

    Definitions:
    The linear coeeficiont in objectivr functional is cz.
    cz := ( 1 0 ... ... 0 ) is directly given in the mosek function

    The linear constraint is
            Az*X <= bz for first Nbz equations,
            Az*X  = bz for last 2 Nbz equations.

    MiAzt := [ M^{-1} * [ B + mu. T ]^t ]

                1  <- SD Nbz ->             <--------SD Nbz--------->
              _                                                              _
             |  0  0.........0                                                |    ^
             |  :  :         :                                                |    |
             |  :  :         :                 [ DT^2. B *MiAzt ]             |    | Nbz
             |  :  :         :                                                |    |
             |  :  0.........0                                                |    v
       Az := |  :                                                             |
             |  :  1 0.......0                                                |  ^
             |  :  0 1 0     :                                                |  |
             |  :  : 0 .     : isqgd*[ DT. T *MiAzt + 1/EPS. DT^2. B *MiAzt ] |  |
             |  :  :     .   :                                                |  | SD Nbz
             |  :  :       . 0                                                |  |
             |  0  0.......0 1                                                |  v
             |_                                                              _|

       bz := ( DT. B*U - D ,  isqgd*(T*U + 1/EPS. (DT. B*U - D)) )

    """

    SD   = Prm.SD             #
    NS   = Prm.NS             #
    DT   = Prm.DT             #
    EPS  = Prm.EPS            #
    if EPS!=0.:                #
        iEPS = real_t(1./EPS)  # whether we want try without penalization
    else:                      #
        iEPS = 0.              #
    iSM  = Prm.iSM            #
    MU   = Prm.MU             #
                              #
    Uvec = Stn.Uvec           #
                              #
    Bzmat = Fmj.Bzmat         #
    Tzmat = Fmj.Tzmat         #
    Dzvec = Fmj.Dzvec         #
    Nbz   = Fmj.Nbz           # pointers

    # MiAzt := [ M^{-1} * [ B + mu. T ]^t ]
    MiAzt = iSM*((Bzmat+MU*Tzmat).transpose())

    # bz := ( DT. B*U - D ,  T*U + 1/EPS. (DT. B*U - D) )
    bz = np.resize( pow(DT,0)*Bzmat.dot(Uvec), Nbz )
    np.subtract( bz, pow(DT,-1)*Dzvec, out=bz, dtype=real_t )
    bz = np.hstack(( bz, isqgd*pow(DT,1)*iEPS*bz ))
    bz = np.hstack(( bz, isqgd*pow(DT,0)*(Tzmat.dot(Uvec))[Nbz:] ))

    # matrix Az
    Az = -eye((SD+1)*Nbz,SD*Nbz+1,-Nbz+1,format="csc") #
    Az.data[0]=0.                                      # left part of Az with all the zeros

    Az = hstack([   Az,                                                            \
                    vstack([pow(DT,1)*Bzmat*MiAzt, csr_matrix((Nbz,SD*Nbz)) ],     \
                    format="csc")   +                                              \
                    vstack([csr_matrix((Nbz,SD*Nbz)),                              \
                    isqgd*pow(DT,1)*Tzmat*MiAzt  +                                 \
                    isqgd*iEPS*(pow(DT,2)*Bzmat*MiAzt) ],                          \
                    format="csc")                                                  \
                ],                                          format="coo"           )

    Fmj.Az    = Az    #
    Fmj.MiAzt = MiAzt #
    Fmj.bz    = bz    # pointers

# solve_pen
def solve_pen(Fmj, SD, EPS, INFO):
    """
    The function solve_pen returns the solution of the minimization problem which is
    described by penalized model.

    Arguments:
      Az : sparse float matrix = the matrix involved in the linear constraint, see make_obj
                                function,
      bz : float array = the vector involved in the linear constraint, see make_obj function,
     Nbz : int = number of effective contacts. It is also the size of indi and indj,
    INFO : boolean = True whether we want to plot information when solving min problem.

    Retunrs:
    lambdaz : float array = solution of minimization problem.

    Problem description:
    The minimization problem is defined as:

                minimize  cz . (t y lambdaz)
                s.t.
                      Az *(t y lambdaz) <= bz  | (for first Nbz equations)
                      Az *(t y lambdaz)  = bz  | (for last 2 Nbz equations)
                      (t, y) in Quad{ 2Nbz+1 }
                      (lambdaz[k], lambdaz[k+Nbz]) for k = 2Nbz+1 ... 3Nbz+1
    """

    Az  = Fmj.Az  #
    bz  = Fmj.bz  #
    Nbz = Fmj.Nbz # pointers

    param_prec = 1.E-08 # 0.00003125 # 0.00015625
    inf    = 0.0         # a value for the optimizer
    numvar = Az.shape[1] # number of variables
    numcon = Az.shape[0] # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task
            task.puttaskname("Friction problem")

            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )
            # increase precision
            task.putdouparam( mosek.dparam.intpnt_co_tol_pfeas  , param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_dfeas  , param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_rel_gap, 10.*param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_infeas , 0.01*param_prec )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( Az.row, Az.col, Az.data ) # put Az to task
            del Az
            Fmj.Az = None

            """ 3/ append cones """
            indfun = (np.linspace(0, SD*Nbz,SD*Nbz+1).astype(int)).tolist()
            task.appendcone( mosek.conetype.quad, 0.0, indfun ) # this is the cone for
                                                               # objective function
            del indfun
            if SD==2:
                for alpha in range(2*Nbz+1, 3*Nbz+1): # cones for Coulomb constraints
                    task.appendcone( mosek.conetype.quad, 0.0, [alpha, alpha+Nbz ] )
            else:
                k=0
                for alpha in range(SD*Nbz+1, (SD+1)*Nbz+1): # cones for Coulomb constraints
                    task.appendcone( mosek.conetype.quad, 0.0, [alpha, alpha+Nbz+k, alpha+Nbz+k+1])
                    k=k+1

            """ 4/ append bounds for variables and constraint """
            # type of bounds
            if EPS!=0.:
                bkc = [ mosek.boundkey.lo ]*Nbz + [ mosek.boundkey.fx ]*SD*Nbz
            else:
                bkc = [ mosek.boundkey.fx ]*Nbz + [ mosek.boundkey.fx ]*SD*Nbz
            bkx = [ mosek.boundkey.fr ]*numvar
            # bounds for variables
            task.putboundlist( mosek.accmode.var,                                   \
                               (np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, [inf]*numvar , [inf]*numvar                     )
            del bkx
            # bounds for linear constraints
            task.putboundlist( mosek.accmode.con,                                   \
                               (np.linspace(0,numcon-1,numcon,dtype=int)).tolist(), \
                               bkc, bz, bz                                          )
            del bz
            Fmj.bz = None
            del bkc

            """ 5/ input linear objective """
            task.putclist((np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), [0.]*numvar)
            task.putcj(0,1.) # c = ( 1 0 ... ... ... ... 0 )

            """ 6/ solvation """
            task.optimize() # optimize

            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                Fmj.lambdaz = np.zeros(SD*Nbz)
                sol = [0.]
                task.getxxslice(mosek.soltype.itr, SD*Nbz+1, numvar, Fmj.lambdaz) # get the solution value
                task.getxxslice(mosek.soltype.itr, 0, 1, sol) # get the solution value
                Fmj.lambdaz = Fmj.lambdaz.astype(dtype=real_t)
            else:
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status in Frition")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                exit()
    return sol[0]

# check_loop
def check_loop( Fmj ):
    """
    The function check_loop does a check on the number of iterations.
    """
    Fmj.Iloop = Fmj.Iloop + 1
    if (Fmj.Iloop>100):
        print("\n> Number of integrations is too high.")
        print("> Iloop = %d" % Fmj.Iloop)
        exit()


# end_loop
def end_loop( ifplot, Nbz, J, Tu ):
    if ifplot:
        print("      Nbz = %d" % Nbz )
        print("        J = %.3e\n     |Tu| = %.2f" % (J, Tu/J*100. ),"% J")

# streamprinter
def streamprinter(text):
    '''
    Define a streamer for mosek used for information ploting.
    '''
    stdout.write(text)
    stdout.flush()

























