"""
file name: elast.py
language: Python3.6
date: 10/05/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains the functions used to take into account a elastic behavior.
"""

# imports
from scipy.sparse import eye, hstack, vstack, csr_matrix, csc_matrix, dia_matrix
from numpy import concatenate, linspace, asarray, float64, sqrt, zeros, ones, dot, sqrt
from numpy import sum as npsum
from sys import stdout, exit
import mosek

# v_elast
def v_elast(Bzmat, DM, vuo, vel, en, INFO, Nbz, ETn, EPnp):
    """
    The function v_elast construct a velocity with elastic collisions.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
       DM : float sparse matrix = mass matrix,
      vuo : float array = the optimized velocity,
      vel : float array = the collision velocity,
       en : float = the restitution coefficient,
     INFO : boolean = True whether we want min information, False otherwise,
      Nbz : int = number of contacts,
      ETn : float = total energy at time tn,
     EPnp : float = potiential energy at time tn+1.

    Returns:
    vel : float array = velocity with elastic collisions.

    """
    if ((en>0.)and(Nbz>0)):
        vA, vb, ND, Nbz, Kvel = make_el( Bzmat, DM, vuo, vel, en, ETn, EPnp) # make elast problem

        if Nbz>0:                                          #
            sol, vel = solve_el(vA, vb, INFO, ND, Nbz, en) # solve elast problem
            if not sol:                                    #
                if ((Kvel/EPnp)<1.E-4):    #
                    vel = vuo.copy()       # there are sometimes problems when Kvel is small
                else:
                    print("\> Elast problem unsolved...")
                    exit()
        else:
            vel = vuo.copy()
    else:
        vel = vuo.copy()

    return vel

# make_el
def make_el( Bzmat, DM, vuo, vel, en, ETn, EPnp):
    """
    The function make_el creates the objects necessary to solve the elastic problem.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
       DM : float sparse matrix = mass matrix,
      vuo : float array = the optimized velocity,
      vel : float array = the collision velocity,
       en : float = the restitution coefficient,
      ETn : float = total energy at time tn,
     EPnp : float = potiential energy at time tn+1.

    Returns:
    vA : sparse float matrix = the constraint matrix, see below,
    vb : float array = the constraint vector, see below,
    ND : int = number of discs,
    Nbz : int = number of contacts,
    kinvel : float = kinetic energy of old elast velocity

    Description:

    vb = ( 0.....0, - DM^1/2 *vuo, -1./eps *en, sqrt(2.*(ETn-EPnp)), 0......0, vuo[2*ND:])

                1  <-3 ND+Nbz->  <- 3 ND ->    <- Nbz ->    1  <- 3 ND ->
              _                                                            _
             |  0  0.........0                              0  0.........0  |  ^
             |  :  :         :                              :  :         :  |  |
             |  :  :         :      Bzmat    diag(B*uel)    :  :         :  |  | Nbz
             |  :  :         :                              :  :         :  |  |
             |  0  0.........0                              0  0.........0  |  v
             |                                                              |
             |  0  1 0.......0                 0...0        0  0.........0  |  ^
             |  :  0 1 0     :   -DM^1/2,      :   :        :  :         :  |  | 3 *ND
             |  :  : 0 .     :                 0...0        0  0.........0  |  v
             |  :  :     .   :    0...0                     0  0.........0  |  ^
             |  :  :       . 0    :   : , -1/eps *Id_Nbz    :  :         :  |  | Nbz
      Az :=  |  0  0.......0 1    0...0                     0  0.........0  |  v
             |                                                              |
             |  0  0.........0  0..........0  0..........0  1  0.........0  |  | 1
             |                                                              |
             |  0  0.........0                0..........0  0  1 0.......0  |  ^
             |  :  :         :                :          :  :  0 1 0     :  |  |
             |  :  :         :     -DM^1/2    :          :  :  : 0 .     :  |  |
             |  :  :         :                :          :  :  :     .   :  |  | 3 ND
             |  :  :         :                :          :  :  :       . 0  |  |
             |  0  0.........0                0..........0  0  0.......0 1  |  v
             |                                                              |
             |  0  0.........0 0....0 1 0...0 0..........0  0  0.........0  |  ^
             |  :  :         : :    : 0 1.  : :          :  :  :         :  |  | ND
             |  :  :         : :    : :   . : :          :  :  :         :  |  |
             |_ 0  0.........0 0....0 0.....1 0..........0  0  0.........0 _|  V
    """
    tmp0 = sqrt(2.*(ETn-EPnp))      # kinetic energy allowed for new vel
    kinvel = 0.5*dot(vel,DM.dot(vel)) # kinetic energy of old vel

    vA = None #
    vb = None #
    ND = None #
    Nbz = 0   # initialization

    if kinvel>1.E-4:
        ND = vuo.size//3        #
        Nbz = Bzmat.shape[0]//2 # get sizes
        ieps = 1.E-4 #
        eps2 = 1.    # coeffs

        DM.data = sqrt(DM.data) # squareroot of mass matrix

        # matrix vA
        vA = eye(2*Nbz+3*ND,3*ND+1+Nbz,-Nbz+1,format="csc")
        vA.data[0]=0.
        vA= hstack([    vA,                                                                 \
                    vstack([   vstack([ Bzmat, csr_matrix((3*ND-Nbz,3*ND)) ],format="csr") +\
                               vstack([ csr_matrix(( Nbz,3*ND)), -eps2*DM],format="csr"),   \
                               csr_matrix((Nbz,3*ND))                                       \
                           ],format="csc"),                                                 \
                    vstack([   dia_matrix((Bzmat.dot(vel),[0]),shape=(Nbz,Nbz)),            \
                               -ieps*eye(Nbz+3*ND,Nbz,-3*ND,format="csr")                   \
                           ],format="csc"),                                                 \
                   csc_matrix((2*Nbz+3*ND,3*ND+1))                                          \
                ],format="csr"                                                              )

        vA = vstack([   vA,
                        hstack([    csc_matrix((3*ND+1,3*ND+Nbz+1)),                \
                                    vstack([csr_matrix((1,3*ND)),-DM],format="csc"), \
                                    csc_matrix((3*ND+1,Nbz)),                         \
                                    eye(3*ND+1,3*ND+1,0,format="csc")                  \
                        ],format="csr")                                                 \
                    ],format="csr"                                                       )
        vA = vstack([   vA,                                          \
                        eye(ND,9*ND+2*Nbz+2,1+5*ND+Nbz,format="csr")  \
                    ],format="coo"                                     )

        # vector vb
        vb = concatenate(( zeros(Nbz) , -eps2*DM.dot(vuo) ),axis=0)
        vb = concatenate(( vb, -ieps*en*ones(Nbz) ),axis=0)
        DM.data = (DM.data)**2 # reset mass matrix
        tmp = zeros(3*ND+1)
        tmp[0] = tmp0
        vb = concatenate((vb, tmp),axis=0)
        vb = concatenate((vb, vuo[2*ND:]),axis=0)

    return vA, vb, ND, Nbz, kinvel

# solve_el
def solve_el(vA, vb, INFO, ND, Nbz, en):
    """
    This function solves the elastic problem.

    Returns:
    vA : sparse float matrix = the constraint matrix, see make_el function,
    vb : float array = the constraint vector, see make_el function,
    ND : int = number of discs,
    Nbz : int = number of contacts,
    en : float = normal elastic coefficient,
    INFO : boolean = True whether we want min information, False otherwise.

    Returns:
    sol : boolean = True wheter we have found a solution, False otherwise,
    vel : float array = velocity with elastic collisions.

    Problem description:
    The minimization problem is defined as:

                minimize  (1, 0...0, 0...0, 0...0, 0, 0...0) . (t, y, v, En, s, w)
                s.t.
                      vA *(t, y, v, En, s, w) = bz    but line 2Nbz+3ND <= bz
                      (t, y) in Quad{ 3ND+Nbz+1 }
                      (s, w) in Quad{ 3ND+1 }
                      0 <= En <= en
    """

    inf = 0.0 # a value for the optimizer
    sol = True

    numvar = vA.shape[1] # number of variables
    numcon = vA.shape[0] # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task

            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( vA.row, vA.col, vA.data ) # put vA to task
            del vA

            """ 3/ append cone """
            indfun = (linspace(0, 3*ND+Nbz,3*ND+Nbz+1).astype(int)).tolist()
            task.appendcone( mosek.conetype.quad, 0.0, indfun ) # this is the cone for
                                                                # objective function
            indfun = (linspace(6*ND+2*Nbz+1,9*ND+2*Nbz+1,3*ND+1).astype(int)).tolist()
            task.appendcone( mosek.conetype.quad, 0.0, indfun ) # this is the second
                                                                # cone
            del indfun

            """ 4/ append bounds for variables and constraint """
            # bounds for variables
            bkx = [ mosek.boundkey.fr ]*numvar
            bkx[Nbz+6*ND+1:2*Nbz+6*ND+1] = [ mosek.boundkey.ra ]*Nbz # for 0 <= En <= en
            bkl = [inf]*numvar
            bku = [inf]*numvar
            bkl[Nbz+6*ND+1:2*Nbz+6*ND+1] = [ 0.9999*en ]*Nbz
            bku[Nbz+6*ND+1:2*Nbz+6*ND+1] = [ en ]*Nbz
            task.putboundlist( mosek.accmode.var,                               \
                               (linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, bkl , bku                                     )
            del bkx, bkl, bku
            # bounds for linear constraints
            bkc = [ mosek.boundkey.fx ]*numcon
            bkc[3*ND+2*Nbz] = mosek.boundkey.up # for <= vb inequality
            task.putboundlist( mosek.accmode.con,                                \
                               (linspace(0,numcon-1,numcon,dtype=int)).tolist(),  \
                               bkc, vb, vb                                         )
            del vb, bkc

            """ 5/ input linear objective """
            task.putclist((linspace(0,numvar-1,numvar,dtype=int)).tolist(), [0.]*numvar)
            task.putcj(0,1.) # c = ( 1 0 ... ... ... ... 0 )

            """ 6/ solvation """
            task.optimize() # optimize

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary
            xx = [0.]*numvar                 #
            task.getxx(mosek.soltype.itr,xx) # get the solution value

            """ 8/ prints """
            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes
                print("  En\n",xx[6*ND+Nbz+1:6*ND+2*Nbz+1])
                print(" s = %.15f" % xx[1+6*ND+2*Nbz],3*ND+2*Nbz)

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                vel = xx[3*ND+Nbz+1:6*ND+Nbz+1]
            else:
                print("Elast problem")
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                sol = False
                vel = [0]
    return sol, asarray(vel, dtype=float64)

# streamprinter
def streamprinter(text):
    '''
    Define a streamer for mosek used for information ploting.
    '''
    stdout.write(text)
    stdout.flush()






































