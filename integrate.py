"""
file name: integrate.py
language: Python3.6
date: 18/06/2018
author: Hugo Martin
email: martin.hugo@live.com
The functions in this module are used to integrate the velocity from t^n to t^{n+1} through a
set of minimization problems setted up over a contact network.
"""

# gcd imports
from gcd.classes import real_t, uint_t, bool_t, FM_obj
from gcd.get_F_DBT import NewNBGl
from gcd.dat_t import save_dat, save_en
from gcd.E_mod import E_mod
from gcd.F_mod import F_mod
from gcd.N_mod import N_mod
# other imports
from scipy.sparse import csr_matrix
from math import sin, cos
import numpy as np
from sys import exit
from time import time

# model pen function
def integrate( Prm, Stn, Buff ):
    """
    The function integrate computes the velocity at time t^{n+1}. It is made by a loop over
    the set of contact. At each new contact found, we add it to the contact network and
    solve the penalized model over this new set. When all the overlaps between the beads are
    prevented, the optimized velocity is then returned.

    Arguments:
     Prm : gcd class = the parameters of the problem,
     Stn : gcd class = the state of the problem at time t^n,
    Buff : gcd class = the buffer class.
    """

    # glued beads
    Stn.glued( Prm.GLCRI )

    # compute the non optimized velocity (or free fall velocity)
    Stn.ini_Uu( Prm.DT, Prm.Gc, Prm.SLOPE )

    # we update the list of neighbourgs
    NewNBGl( Prm, Stn, Buff )

    # loop initialization
    Fmj = FM_obj( Prm.SD, Prm.NS )

    # normal model is used is there is no friction
    N_mod( Prm, Stn, Buff, Fmj )

    # friction model (A loop of friction models) # LOOP OVER CONTACT NETWORK
    F_mod( Prm, Stn, Buff, Fmj )

    # integrate q_n with optimized velocity u_o
    Stn.integrate( Prm.SX, Prm.SY, Prm.DT, Prm.WV, Buff.v3ND )

    # compute energy with optimized velocity
    Stn.set_en( True, Prm.SM, Prm.Gc, Prm.SLOPE, Buff.vND1 )

    # compute uel, the elastic velocity
    E_mod( Prm, Stn, Fmj )

    # compute energy with elastic velocity
    Stn.set_en( False, Prm.SM, Prm.Gc, Prm.SLOPE, Buff.vND1 )

    if Stn.PLOT: # save velocity and position
        get_CFvec( Buff, Fmj, Prm.SM )
        save_dat( Stn, Buff.v3ND, Fmj.Cind, Fmj.lambdaz, Prm.NAME )
        save_en( False, Stn.CTIME, Stn.E_n, Prm.NAME )

# get_CFvec
def get_CFvec( Buff, Fmj, SM ):
    """
    The function get_CFvec returns the contact forces vector.

    Arguments:
        MiAzt and lambdaz are stocked in the gcd class Fmj.

      MiAzt : sparse float matrix = see definition below,
    lambdaz : float array = solution of minimization problem,
         SM : float sparse matrix = mass matrix,

    Retruns:
    CFvec is stocked in gcd class Buff.v3ND
    CFvec : float array = contact force vector.
    """
    if(Fmj.lambdaz.size>0):
        Buff.v3ND = -SM*( (Fmj.MiAzt).dot( Fmj.lambdaz ))
    else:
        Buff.v3ND = np.zeros( SM.shape[0] )





