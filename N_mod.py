"""
file name: N_mod.py
language: Python3.6
date: 16/06/2018
author: Hugo Martin
email: martin.hugo@live.com
This module python contains the functions used to solve the frictionless problem.
"""

# gcd imports
from gcd.classes import real_t, Buffer
from gcd.get_F_DBT import get_DB_Npro, get_indi_j
# other imports
import numpy as np
from numpy import linspace
from numpy import asarray, sort, max as npmax
from scipy.sparse import block_diag, coo_matrix, hstack, eye, dia_matrix, csr_matrix, vstack
from sys import stdout, exit
import mosek

def N_mod( Prm, Stn, Buff, Fmj ):
    """
    The function N_pro solves the normal problem and return a contact network Cind for the
    tangential problem.

    Returns:
    Cind

    DT = 1.E-4 => tmp1=1., tmp2=5.E-5


    """

    if Prm.NPRO:

        DT = Prm.DT
        NS = Prm.NS
        SD = Prm.SD
        u_ofls = Stn.u_o

        # get new contacts and update Dzvec and dtBzmat
        Cind, dtBzmat, Dzvec, wtd = get_DB_Npro( Prm, Stn, Buff, u_ofls )
        Fmj.wtd = wtd  # update wtd

        if wtd: # if there is new contacts, we must work
            Np = Dzvec.size
            Az, bz = make_Npro( dtBzmat,            \
                                Dzvec,            \
                                Np,               \
                                Stn.Uvec,         \
                                Prm.NS,           \
                                Prm.DT, Buff.DM2, \
                                Prm.SD            ) # make the min problem
                                                    # objects

            # solve minimization problem
            lambdaz = solve_Npro(Az, bz, Prm.SD, Np, Prm.NS, Prm.INFO)

            # compute the optimized velocity
            u_ofls[:SD*NS] = (dtBzmat.transpose()).dot(lambdaz)
            np.subtract(Stn.Uvec[:SD*NS], (Prm.iSM*u_ofls)[:SD*NS], out=u_ofls[:SD*NS], dtype=real_t)

            # create a contact network if elasticity
            if(Prm.EN>0.):
                iamax = real_t(1./(np.amax(lambdaz, axis=0)))
                np.multiply(iamax, lambdaz, out=lambdaz, dtype=real_t) # normalize
                tmp = np.less_equal(lambdaz, 1., dtype=real_t)
                np.multiply(tmp, np.greater_equal(lambdaz, 1.E-4, dtype=real_t), out=tmp, dtype=bool)
                # All contact forces which are below the criteria are removed
                Cind = Cind[tmp]
                Fmj.Bzmat = dtBzmat[tmp,:]
                Fmj.Nbz   = Cind.shape[0]
            del Dzvec
    else:
        Fmj.Cind = np.zeros((0,2), dtype=int)
        Fmj.Nbz = 0 # update pointers

# make_Npro
def make_Npro( Bzmat, Dzvec, Np, Uvec, NS, DT, DM2, SD ):
    """
    The function make_Npro makes the objects necessary to create a minimization problem,
    devoted to solve the frictionless problem.
    See below for their definitions.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      Np : int = number of effective contacts. It is also the size of indi and indj,
     Uvec : float array = non optimized velocity configuration,
       NS :   int = number of discs,
       DT : float = time step value,
       SM : float sparse matrix = mass matrix,

    Returns:
       Az : sparse float matrix = the matrix involved in the linear constraint,
       cz : float array = the vector involved in the linear objective function.

    Definitions:
    The linear coeeficiont in objectivr functional is cz.
    cz := ( 1 0 0...0 [(Dzvec - DT B *Uvec)] )  -> SD*NS+2+Nbz variables

    The linear constraint is
            Az*X  = (1 0...0) -> SD*NS+1 constraints (vector is directly given in mosek fun)

                1  1  <- SD NS -> <--------Nbz --------->
              _                                         _
             |                                            |  ^
             |  0  1  0.................................0 |  | 1
             |  :  :  -1 0.......0                        |  V
             |  :  :  0 -1 0     :                        |  ^
        Az = |  :  :  : 0  .     :    DT* M^{-1/2} B^t    |  |
             |  :  :  :      .   :                        |  | SD NS
             |  :  :  :        . 0                        |  |
             |  0  0  0.......0 -1                        |  v
             |_                                          _|

    """

    cz    = np.zeros(2+SD*NS+Np, dtype=real_t)
    cz[0] = real_t(1.)

    np.subtract(Dzvec, Bzmat.dot(Uvec[:SD*NS]), out=cz[2+SD*NS:], dtype=real_t)

    Az = ( DM2*(Bzmat.transpose()) ).tocoo()
    Az = hstack([ -eye(SD*NS, format="coo", dtype=real_t), Az ],format="coo")
    Az = block_diag(( coo_matrix(np.asarray([0.,1.]), dtype=real_t), Az ),format="coo" )

    return Az, cz

# solve_Npro
def solve_Npro(Az, cz, SD, Nbz, NS, INFO):
    """
    The function solve_Npro returns the solution of the minimization problem which is
    given by frictionless model.

    Arguments:
      Az : sparse float matrix = the matrix involved in the linear constraint, see make_obj
                                function,
      cz : float array = the vector involved in the linear objective function,
     Nbz : int = number of effective contacts,
    INFO : boolean = True whether we want to plot information when solving min problem.

    Retunrs:
    lambdaz : float array = solution of minimization problem.

    Problem description:
    The minimization problem is defined as:

                minimize  cz . (t s y lambdaz)
                s.t.
                        Az *(t s y lambdaz) = (1 0...0 )
                        lambdaz >= 0
                        (t,s,y) \in \Quad_r{ 2*NS+2 } <- rotational quadratic cone
    """

    param_prec = 1.E-08 # 0.00015625
    inf = 0.0 # a value for the optimizer
    numvar = Az.shape[1] # number of variables
    numcon = Az.shape[0] # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task

            task.puttaskname("Normal problem")
            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )
            # increase precision
            task.putdouparam( mosek.dparam.intpnt_co_tol_pfeas  , param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_dfeas  , param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_rel_gap, 10.*param_prec )
            task.putdouparam( mosek.dparam.intpnt_co_tol_infeas , 0.01*param_prec )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( Az.row, Az.col, Az.data ) # put Az to task
            del Az

            """ 3/ append cones """
            indices = (linspace(0, SD*NS+1, SD*NS+2).astype(int)).tolist()
            task.appendcone(mosek.conetype.rquad, 0.0, indices ) # append the cone

            """ 4/ append bounds for variables and constraint """
            # type of bounds
            bkc = [ mosek.boundkey.fx ]*numcon
            bkx = [ mosek.boundkey.fr ]*(SD*NS+2) + [ mosek.boundkey.lo ]*Nbz
            # bounds for variables
            task.putboundlist( mosek.accmode.var,                               \
                               (linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, [inf]*numvar , [inf]*numvar                   )
            del bkx
            # bounds for linear constraints
            bz = np.zeros(numcon)
            bz[0] = 1.
            task.putboundlist( mosek.accmode.con,                                \
                               (linspace(0,numcon-1,numcon,dtype=int)).tolist(),  \
                               bkc, bz, bz                                         )
            del bz, bkc

            """ 5/ input linear objective """
            task.putclist((linspace(0,numvar-1,numvar,dtype=int)).tolist(), cz.tolist())
            del cz

            """ 6/ solvation """
            task.optimize() # optimize

            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary
            xx = [0.]*numvar                 #
            task.getxx(mosek.soltype.itr,xx) # get the solution value

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                lambdaz = asarray(xx[SD*NS+2:], dtype=real_t)
            else:
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status in Normal")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                exit()

    return lambdaz


# streamprinter
def streamprinter(text):
    '''
    It Defines a stream printer to grab output from MOSEK.
    '''
    stdout.write(text)
    stdout.flush()













