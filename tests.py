"""
file name: tests.py
language: Python3.6
date: 30/04/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains functions to initialize predefined simulations and make test on them.
"""

"""
IMPORTS
"""
from gcd.classes import Parameters, State, real_t
from gcd.dat_t import load_quf
import numpy as np
from numpy.random import permutation, normal, random
from math import pi, sqrt, cos, sin, atan
from sys import exit

"""
simudict is the dictionnary to set predefined simulations.
The different predefined simulations are:
   ff : free fall of one disc
  rds : rolling disc with slip
 rdws : rolling disc without slip
 3xds : 3 discs stacked static
 3xdd : 3 discs stacked and collapse
   bc : binary collision
  obc : orthogonal binary collision
poela : polydispersity and elasticity
Npend : Newton's pendulum
"""

simudict = { "ff":0, "rds":1,  "rdws":2,  "3xds":3, "3xdd":4,
             "bc":5, "obc":6, "poela":7, "Npend":8           }

# ini_test
def ini_test( simu, SD ):
    """
    The function ini_test returns two dictionnaries used to set up a predefined simulation.

    Arguments:
    SD   :    int = space dimension,
    simu : string = name of a predefined simulation (see simudict).

    Returns:
    params : python class = see ini_params function
    state  : python class = see ini_state function
    """

    # get simu number
    SIMU = simudict[simu]

    # create parameters class
    prm = ini_params_test(SIMU, SD)

    # create state class
    state =  ini_state_test( SIMU, SD, prm.NS, prm.DT, \
                             prm.SR, prm.SX, prm.SY,   \
                             prm.GLCRI, prm.SM         )

    return prm, state

# ini_params
def ini_params_test( SIMU, SD ):
    """
    The function ini_params returns a dictionnary with parameters necessary to set up a
    GCD Problem.

    Arguments:
    SIMU = integer : integer corresponding to a predefined simulation.
      SD = integer : space dimension

    Returns:
    params = python class : its members are the same than the ones of the class
                            Pb_Params, see Pb_Params class in file cobj.pxd in
                            repository ./gcd/obj
    """
    """
    beads of glass
    pho = 2500 kg/m3 => 2.5E3 kg/ 1.E9 mm3 => 2.5E-6 kg.mm-3 => 2.5E-3 g.mm-3
    MU = 0.95 ( 0.9 - 1.  )
    EN = 0.9375

    """

    NW    = 1
    NS   = 1

    FT    = 0.001 # s
    DT    = 1.e-4 # s

    RHO   = 2.5E-3       # g.mm-3
    SR    = 0.35         # mm
    SM    = RHO*pi*SR*SR # g
    MU    = 0.95
    EN    = 0.

    Gc    = 9.81E3  # mm.s-2

    SX    = 15.*SR  # mm
    SY    = SR      # mm
    SLOPE = 0.      # rad
    WV    = 0.      # mm.s-1

    EPS   = 1.e-6
    OCRI  = 1.E-8
    OSTEP = 10
    INFO  = False
    GLCRI = -1.E6   # -0.9*SR
    VARDT = False

    repobc = 0
    if(SIMU==0): # free fall of one disc
        SNAME = "ff"
        FT = 0.04
        NS = 1
        SLOPE = pi/8.

    elif((SIMU==1)or(SIMU==2)): # rolling disc
        FT = 0.03
        NS = 1
        alpha = atan(MU)

        if(SIMU==1):
            SNAME = "rds"
            SLOPE = 1.05*alpha # with slip
        else:
            SNAME = "rdws"
            SLOPE = 0.95*alpha # without slip

    elif((SIMU==3)or(SIMU==4)): # static and dynamic 3xdiscs
        NS = 3

        if(SIMU==3): # static
            SNAME = "3xds"
            FT = 0.05

        else: # dynamic
            SNAME = "3xdd"
            MU = 0.2
            FT = 0.05

    elif((SIMU==5)or(SIMU==6)): # binary collision or triple collision
        SNAME = "bc"
        FT = 0.07
        NS = 2
        EN = 0.

        if(SIMU==6): # othogonal binary collision or triple
            SNAME = "obc"
            EN = float(input("> Enter the En value: "))
            repobc = int(input("> test elast ? 0, 1, 2, 3: "))
            Gc = 0.
            NW = 3
            SX = 15.
            MU = 0.5
            if repobc==1:
                print("> Discs have same mass")
            elif repobc==2:
                SR = np.asarray([2., 0.5]).astype(dtype=real_t)
            elif repobc==3:
                SR = np.asarray([0.5, 2.]).astype(dtype=real_t)

    elif(SIMU==7): # test with different radius
        SNAME = "poela"
        OCRI  = 0.
        NW    = 1
        WV    = 0. # 2.E1   # mm.s-1
        NS    = 20
        OSTEP = 25
        FT    = 0.2
        SR = permutation(normal(SR, 0.08, NS)).astype(dtype=real_t)
        MU    = 0.5
        EN    = 0.6
        EPS   = 2.5E-6


    elif(SIMU==8): # Newton's pendulum
        SNAME = "Npend"
        DT    = 1.E-2
        EPS   = 0.
        MU    = 0.
        OSTEP = 1
        OCRI  = 0.
        NS    = 5
        NW    = 3
        Gc    = 0.
        SX    = 18*SR
        EN    = 0.9
        FT    = 1.

    params = Parameters( SNAME, SD, NS, NW, SX, SY, SLOPE, WV,  \
                         DT, FT, RHO, EN, SR, MU, EPS, Gc,      \
                         GLCRI, OCRI, OSTEP, INFO, VARDT )

    params.set_mass_matrix() # update the mass matrix

    return params

# ini_state
def ini_state_test( SIMU, SD, NS, DT, SR, SX, SY, GLCRI, SM ):
    """
    The function ini_state returns a dictionary with evolutive values initialization.
    It is equivalent to an initial state initialization.

    Arguments:
    SIMU = integer : integer corresponding to a predefined simulation,
      SD = integer : space dimension,
      NS = integer : number of discs,
      DT =  double : time step value,
      SR =  float array : radii of discs,
      SX =  double : side box length,
      SY =  double : side box width,
    GLCRI = double : criteria for glued beads.

    Returns:
    state = python class : its members are the same than the ones of the class
                                Pb_State, see Pb_State class in file cobj.pxd in
                                repository ./gcd/obj
    """


    if SD==2:
        qnb = 3 # number of spherical coordinates
    elif SD==3:
        qnb = 7 # number of spherical coordinates
    else:
        print("\n Problem with space dimension")
        exit()

    qsize = qnb*NS
    usize = 3*(SD-1)*NS

    q_n  = np.zeros( qsize )  # postions
    u_n  = np.zeros( usize )  # velocities
    cf_n = np.zeros( usize )  # contact forces

    if SD==3:
        for i in range(NS):
            q_n[3*NS+4*i+3] = 1.

    time_n = 0.
    iter_n = 0
    file_n = 0

    WH     = 0.

    """POUR L INSTANT, JE N'AI CODE QUE LES CAS 0 1 et 2 EN 3D."""
    if((  not((SIMU<=2)or(SIMU==5))   )and(SD==3)):
        print("L'initialisation de",SIMU,"n'est pas codee en 3 dimensions.")
        exit()

    if(SIMU==0): # free fall
        q_n[SD-1] = 4.*SR[0]
        u_n[1] = -4.

    elif((SIMU==1)or(SIMU==2)): # rolling disc
        q_n[SD-1] = SR[0]

    elif((SIMU==3)or(SIMU==4)): # 3xdiscs
        q_n[0] = 0.5*SX/3.
        q_n[1] = SR[0]
        q_n[2] = q_n[0]+2.*SR[0]
        q_n[3] = q_n[1]
        q_n[4] = q_n[0]+SR[0]
        q_n[5] = q_n[1]+SR[0]*sqrt(3.)

    elif((SIMU==5)or(SIMU==6)): # binar collision
        q_n[0] = 0.5*SX/3. # x axis

        if(SIMU==5):                     #
            if SD==2:                    #
                mini   = SR[0]           #
                q_n[1] = 3.-SR[0]/2.     #
                q_n[3] = q_n[1]+SR[0]    #
                q_n[2] = q_n[0]+3.*mini  #
            else:                        # non orthogonal
                dr     = np.max(SR)        #
                q_n[1] = 0.              #
                q_n[2] = 3.*dr           #
                q_n[3] = q_n[0] + 5.*dr  #
                q_n[4] = q_n[1] + 0.5*dr #
                q_n[5] = q_n[2] - 0.3*dr #
        else:
            mini = np.max(SR)        #
            q_n[0] = 2.2*mini       #
            q_n[1] = 3.*mini        # orthogonal
            q_n[3] = q_n[1]         #
            q_n[2] = q_n[0]+3.*mini #

        u_n[0] = 500.

    elif (SIMU==7): # polydispersity and elasticiy
        DRM = np.max(SR)
        step = 2.*DRM + DRM/2. # distance between two grains
        posx_0 = 1.1*DRM         #
        posz_0 = 1.1*DRM         # position of the current grain
        h = 0 # number of vertical lines
        posx = posx_0 - step
        posz = posz_0
        for i in range(NS): # loop over the set of grains
            posx = posx + (1 + 0.5*( i % 2))*step # change x position
            if( (posx+DRM) > SX-1.1*DRM): #
                posz = posz + step  #
                posx = posx_0       # if we are at the end of the horizontal line then we
                                    # start a new line
            q_n[2*i] = posx + 0.02*SR[i]*random()*pow(-1,i)
            q_n[2*i+1] = posz + 0.02*SR[i]*random()*pow(-1,i)

    elif(SIMU==8): # Newton's pendulum
        eps = 1.E-4
        rep = int(input("> Type of pendulum, 1, 2, 3 or 4: "))
        dr = SR[0]
        q0 = 5*dr
        q1 = 3*dr
        for i in range(5):
            q_n[2*i] = q0+i*(2*dr+eps)
            q_n[2*i+1] = q1
        q_n[0] = dr
        u_n[0] = dr/(0.005)
        if rep>=2:
            q_n[2] = q_n[0]+(2*dr+eps)
            u_n[2] = u_n[0]
        if rep>=3:
            q_n[4] = q_n[2]+(2*dr+eps)
            u_n[4] = u_n[0]
        if rep>=4:
            q_n[6] = q_n[4]+(2*dr+eps)
            u_n[6] = u_n[0]

    # end of if

    GLUED = q_n[1:2*NS:2] < GLCRI
    state = State( time_n, iter_n, file_n, WH, SD, NS, q_n, u_n)

    return state

# make_test
def make_test( Startf, Endf, simu, params ):
    """
    Loop over a set of files and tests things.

    Arguments:
    Startf : int = number of fisrt file,
      Endf : int = number of last file,
      simu : string = name of a predefined simulation (see simudict),
    params : python class = parameters of the GCD problem.
    """
    print("\n> Make test:")

    SIMU = simudict[simu] #
    SD = params.SD        #
    SR = params.SR        #
    SM = params.SM        #
    MU = params.MU        #
    Gc = params.Gc        #
    SLOPE = params.SLOPE  # get parms values

    if(((1<=SIMU)and(SIMU<=3))):
        for i in range(Startf, Endf+1): # loop over set of file
            ns, q, u, f = load_quf( i, SD, simu ) # get data
            test(f, u, SIMU, SM, Gc, SLOPE, MU, i) # make tests
    else:
        print("\n> Test does not exist for this predefined configuration.\n\n")

# test
def test(f_n, u_n, SIMU, SM, Gc, SLOPE, MU, fnum):
    """
    The function test does a test on data files.

    Arguments:
     f_n : contact force vector,
     u_n : velocity vector,
    SIMU : int = integer corresponding of a simu name in simudict,
      SM : float = mass matrix,
      Gc : float = gravity constant,
    SLOPE : float = slope between the bottom and the horizontal axis,
       MU : float = Coullomb's law firction coefficient,
     fnum : int = number of file.
    """
    if SIMU==1:
        DMd = SM.data[0,0]
        N = DMd*Gc*cos(SLOPE)
        err = sqrt( pow( -MU*N - f_n[0] ,2) + pow( N - f_n[1] ,2) )
        print("\n %d: || Fana - Fapp || = %.3e\n" % ( fnum, err))
    elif SIMU==2:
        DMd = SM.data[0,0]
        N = DMd*Gc*cos(SLOPE)
        T = DMd*Gc*sin(SLOPE)/3.
        err = sqrt( pow( -T - f_n[0] ,2) + pow( N - f_n[1] ,2) )
        print("\n %d: || Fana - Fapp || = %.3e\n" % ( fnum, err))
    elif SIMU==3:
        EC = 0.5*np.asscalar(np.dot(SM.dot(u_n),u_n)) # kinetic energy
        print("Ec = %.15e\n" % EC)






