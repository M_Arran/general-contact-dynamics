/*
file name: gcd_C_func.c
language: C
date: 19/06/2018
author: Hugo Martin
email: martin.hugo@live.com
This is the C functions used by GCD code.
*/

// includes
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

// macro
#define max(a, b) ( (a) < (b) ? (b) : (a) )
#define min(a, b) ( (a) < (b) ? (a) : (b) )
#define M_PI acos(-1.0)

//typedef float real_t;
typedef double real_t;

// cget_Nbz
int cget_Nbz_NBG(   unsigned const SD,
                    unsigned *indi, unsigned *indj,
                    real_t const WH,
                    real_t const *q_n,
                    unsigned const *indices, unsigned const *indptr,
                    unsigned const ND,
                    real_t const *SR, real_t const SDf,
                    real_t const SX, real_t const SY, real_t const CRI        )
{
    /*
     * The function cget_Nbz loops over all the set of possible contacts and detects whether
     * there is a contact. When it is true, it saves the numbers of spheres in contact in the
     * arrays indi and indj and increments the number of contacts Nbz.
     * It returns then the number of effective contacts.
     *
     * Arguments:
     * indi : unsigned * = indi[k] is the number of sphere i in contact numbered k,
     * indj : unsigned * = indj[k] is the number of sphere j in contact numbered k,
     *   WH : real_t = right wall height,
     *  q_n :   real_t * = configuration positions,
     * indices : unsigned * = NBGl.indices in a CSR format, see scipy.sparse documentation,
     *  indptr : unsigned * = NBGl.indices in a CSR format, see scipy.sparse documentation,
     *                      = More precisely, the bead i has got a set of neighbourgs which
     *                        is described by the arrays indices and indptr. Indeed, the
     *                        beads in contact with the bead i are given by the set
     *                        indices[indptr[i]:indptr[i+1]]
     *   ND :   unsigned = number of spheres,
     *   NW :   unsigned = number of new contacts,
     *   SR : real_t array* = radii of spheres,
     *   SX :     real_t = side box length,
     *   SY :     real_t = side box width,
     *  CRI :     real_t = overlap criteria.
     *
     * Returns:
     *  Nbz : int = the number of contacts we have detected
     *
     * Description:
     * If (Dist >= 0) ====> there is no contact.
     * Elseif (-CRI < Dist < 0) ===> there is no contact.
     *
     * Else ===> there is a contact.
     */

    unsigned k, i, j, ii, jj; // counters
    unsigned Nbz=0;           //
    //~ real_t Dist, Eps=1.E-15;  // temporary variables, Nbz is the
    //~ real_t CrimEps=CRI-Eps;   // number of effective contacts. The parameter Eps is
                              // useful to handle problem of troncature. Especially
                              // when we want a contact when Dist = Cri in the
                              // equality.
    //~ unsigned contact=0; // boolean variable
    real_t Dist, SRi, SRj; // the values of radii
    real_t minD=1.E9;
    //~ real_t CrimEps=CRI;

    for(i=0; i<ND; ++i){                      // we take a bead i
        SRi = SR[i];                          // get radius of i
        for(k=indptr[i]; k<indptr[i+1]; ++k){ // we loop over the neighbourgs of bead i

            j = indices[k]; // we take the number of the neighbourg
            SRj = SR[j];    // get radius of j
            if (j<ND){      // j is a bead

                if(SD==2){
		  Dist = sqrt( pow(remainder(q_n[2*i]-q_n[2*j], SX), 2) +
                                 pow((q_n[2*i+1]-q_n[2*j+1]), 2) )-(SRi+SRj); // get the distance
                                                                             // between
                                                                             // grains i and j
                }else{
		  Dist = sqrt( pow(remainder(q_n[3*i]  -q_n[3*j]  , SX), 2) +
			       pow(remainder(q_n[3*i+1]-q_n[3*j+1], SY), 2) +
                                 pow((q_n[3*i+2]-q_n[3*j+2]), 2) )-(SRi+SRj); // get the distance
                                                                              // between
                                                                              // grains i and j
                };

                if (minD>Dist){
                    minD = min(minD, Dist);
                    ii = i;
                    jj = j;
                };
                if((Dist/SDf)<CRI){    //     D / d < Criteria
                    indi[Nbz]=i;//
                    indj[Nbz]=j;//
                    Nbz++;      //
                };              // if the distance is below the criteria we save this contact
            };

            if (j==ND){ // j is the bottom
                Dist = fabs(q_n[SD*i+(SD-1)])-SRi; // get the distance with bottom

                if (minD>Dist){
                    minD = min(minD, Dist);
                    ii = i;
                    jj = j;
                };
                if((Dist/SDf)<CRI){      //
                    indi[Nbz]=i;  //
                    indj[Nbz]=ND; // ND is the number of bottom
                    Nbz++;        //
                };                // if the distance is below the criteria we save this contact
            };
            if (j==ND+1){ // j is the left wall
                Dist = fabs(q_n[SD*i])-SRi; // get the distance with left wall

                if (minD>Dist){
                    minD = min(minD, Dist);
                    ii = i;
                    jj = j;
                };
                if((Dist/SDf)<CRI){        //
                    indi[Nbz]=i;    //
                    indj[Nbz]=ND+1; // ND+1 is the number of right wall
                    Nbz++;          //
                };                  // if the distance is below the criteria we save this contact
            };
            if (j==ND+2){ // if there are 3 walls
                Dist = fabs(q_n[SD*i]-SX)-SRi; // get the distance with right wall
                if(((SD==2)&&(q_n[SD*i+(SD-1)]>WH))||(SD==3)){ // wall with a height
                    if (minD>Dist){
                        minD = min(minD, Dist);
                        ii = i;
                        jj = j;
                    };

                    if((Dist/SDf)<CRI){        //
                        indi[Nbz]=i;    //
                        indj[Nbz]=ND+2; // ND+2 is the number of right wall
                        Nbz++;          //
                    };                  // if the distance is below the criteria we save this
                                        // contact
                };
            };
            if (j==ND+3){
                Dist = fabs(q_n[3*i+1])-SRi;
                if(minD>Dist){
                    minD = min(minD, Dist);
                    ii = i;
                    jj = j;
                };
                if((Dist/SDf)<CRI){        //
                    indi[Nbz]=i;    //
                    indj[Nbz]=ND+3; // ND+1 is the number of right wall
                    Nbz++;          //
                };                  // if the distance is below the criteria we save this contact
            };
            if (j==ND+4){
                Dist = fabs(q_n[3*i+1]-SY)-SRi;
                if(minD>Dist){
                    minD = min(minD, Dist);
                    ii = i;
                    jj = j;
                };
                if((Dist/SDf)<CRI){        //
                    indi[Nbz]=i;    //
                    indj[Nbz]=ND+4; // ND+1 is the number of right wall
                    Nbz++;          //
                };                  // if the distance is below the criteria we save this contact
            };
        }; // end of loop over the set of neighbourgs
    }; // end of the loop over the beads

    //~ printf("  min(D) = %.3e,   (%d, %d)\n",minD,ii,jj);

    return Nbz; // return the number of contacts we have detected
};

// cNew_neigh_rad
int cNew_neigh_rad( real_t *radius,
                    real_t const *norms, real_t const *q_n,
                    unsigned const ND, unsigned const SD, unsigned const NW,
                    real_t const SX, real_t const SY, real_t const CRI,
                    real_t const Ref_rad, real_t const DT                    )
{
    /*
     * The function cNewNBGl loops over all the set of possible contacts and detects whether
     * there are neighbourgs. When it is true, it saves the numbers of spheres in contact in
     * the arrays indi, indj and valij and increments the number of neighbourgs Nbz.
     * It returns then the number of neighbourgs. The criteria is fixed at 40 grains.
     *
     * Arguments:
     * radius : real_t * = the array where we will save the radius values.
     *  norms : real_t * = norms of velocities (for each beads),
     *  q_n :   real_t * = configuration positions,
     *   ND :   unsigned = number of spheres,
     *   NW :   unsigned = number of new contacts,
     *   SX :     real_t = side box length,
     *   SY :     real_t = side box width,
     *  CRI :     real_t = overlap criteria,
     *  Ref_rad : real_t = the radius given by the maximum velocity,
     *   DT : real_t = time step value.
     *
     * Returns:
     * Nbz : int = the number of the total neighbourgs we have detected with Rad_ref.
     *
     * Description:
     * If (Dist >= 0) ====> there is no contact.
     * Elseif (-CRI < Dist < 0) ===> there is no contact.
     *
     * Else ===> there is a contact.
     */

    unsigned i, j;  // counters
    unsigned Nbz=0; //
    real_t time=11*DT; // a time used to compute radius
    real_t Dist, Eps=1.E-15; // temporary variables, Nbz is the
    real_t CrimEps=CRI-Eps;  // number neighbourgs The parameter Eps is
                             // useful to handle problem of troncature. Especially
                             // when we want a neighb when Dist = Cri in the
                             // equality.
    unsigned neighb=0; // boolean variable

    for(i=0;i<ND;++i){       //
        for(j=i+1;j<ND;++j){ // the two loops for do a loop over all set of possible contacts
            if (SD==2){
	      Dist = sqrt( pow(remainder(q_n[2*i]-q_n[2*j], SX), 2) +
                           pow((q_n[2*i+1]-q_n[2*j+1]), 2))-Ref_rad; // distance minus Ref_rad
            }else{
	      Dist = sqrt( pow(remainder(q_n[3*i]  -q_n[3*j]  , SX), 2) +
                           pow(remainder(q_n[3*i+1]-q_n[3*j+1], SY), 2) +
                             pow((q_n[3*i+2]-q_n[3*j+2]), 2) )-Ref_rad; // distance minus Ref_rad
            };
            neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));

            // does the distance between i and j is included in a cricle of radius Ref_rad ?
            if(neighb){
                radius[i] = max( radius[i], time*norms[j] ); //
                Nbz++;                                       // updtate radius[i]
            };
        }; // end of loop for j

        Dist = fabs(q_n[SD*i+(SD-1)])-Ref_rad; // distance minus Ref_rad
        neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));

        // does the distance between i and bottom is included in a cricle of radius Ref_rad ?
        if(neighb){
            radius[i] = max( radius[i], time*norms[i] ); //
            Nbz++;                                       // updtate radius[i]
        };

        if(NW>=2){ // if there are at least 2 walls
            Dist = fabs(q_n[SD*i])-Ref_rad; // distance minus Ref_rad
            neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));

            // does the distance between i and left wall is included in a cricle of ...
            if(neighb){
                radius[i] = max( radius[i], time*norms[i] ); //
                Nbz++;                                       // updtate radius[i]
            };

            if(NW>=3){ // if there are 3 walls
                Dist = fabs(q_n[SD*i]-SX)-Ref_rad; // distance minus Ref_rad
                neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));

                // does the distance between i and right wall is included in a cricle of ...
                if(neighb){
                    radius[i] = max( radius[i], time*norms[i] ); //
                    Nbz++;                                       // updtate radius[i]
                };
            };

            if(SD==3){
                if(NW>=4){
                    Dist = fabs(q_n[3*i+1])-Ref_rad;// does the distance between right wall ?
                    neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));
                    // does the distance between i and right wall is included in a cricle of ...
                    if(neighb){
                        radius[i] = max( radius[i], time*norms[i] ); //
                        Nbz++;                                       // updtate radius[i]
                    };
                };

                if(NW==5){
                    Dist = fabs(q_n[3*i+1]-SY)-Ref_rad; // does the distance between right wall ?
                    neighb = !((Dist>=0.)||(fabs(Dist)<CrimEps));
                    // does the distance between i and right wall is included in a cricle of ...
                    if(neighb){
                        radius[i] = max( radius[i], time*norms[i] ); //
                        Nbz++;                                       // updtate radius[i]
                    };
                };
            };
        };
    };
    return Nbz; // return the number of neigbourgs we have detected with Ref_rad
};

// cNewNBGl
int cNewNBGl( unsigned *indi, unsigned *indj, unsigned *valij,
              real_t const *radius,
              real_t const *q_n,
              unsigned const ND, unsigned const SD, unsigned const NW,
              real_t const SX, real_t const SY, real_t const CRI        )
{
    /*
     * The function cNewNBGl loops over all the set of possible contacts and detects whether
     * there are neighbourgs. When it is true, it saves the numbers of spheres in contact in
     * the arrays indi, indj and valij and increments the number of neighbourgs Nbz.
     * It returns then the number of neighbourgs. The criteria is fixed at 40 grains.
     *
     * Arguments:
     * indi : unsigned * = indi[k] is the number of sphere i in potential contact numbered k,
     * indj : unsigned * = indj[k] is the number of sphere j in potential contact numbered k,
     * valij : unsigned * = valij[k] = 0 if potential contact is detected, 0 otherwise,
     * radius : real_t * = distances for each bead we are looking for neighbourgs,
     *  q_n :   real_t * = configuration positions,
     *   ND :   unsigned = number of spheres,
     *   NW :   unsigned = number of new contacts,
     *   SX :     real_t = side box length,
     *   SY :     real_t = side box width,
     *  CRI :     real_t = overlap criteria.
     *
     * Returns:
     *  Nbz : int = the number of contacts we have detected
     *
     * Description:
     * If (Dist >= 0) ====> there is no contact.
     * Elseif (-CRI < Dist < 0) ===> there is no contact.
     *
     * Else ===> there is a contact.
     */

    unsigned i, j;  // counters
    unsigned Nbz=0; //
    real_t temprad; // temporary radius
    real_t Dist, Eps=1.E-15; // temporary variables, Nbz is the
    real_t CrimEps=CRI-Eps;  // number of effective contacts. The parameter Eps is
                             // useful to handle problem of troncature. Especially
                             // when we want a contact when Dist = Cri in the
                             // equality.
    unsigned contact=0; // boolean variable

    for(i=0;i<ND;++i){       //

        temprad = radius[i]; // get the value of radius[i], the radius of the circle in which
                             // we are looking for new neighbourgs.

        for(j=i+1;j<ND;++j){ // the two loops for do a loop over all set of possible contacts
            if (SD==2){
	      Dist = sqrt( pow(remainder(q_n[2*i]  -q_n[2*j], SX)  , 2) +
                           pow((q_n[2*i+1]-q_n[2*j+1]), 2) )-temprad;// does the distance between
                                                                     // i and j is in a radius of
                                                                     // temprad ?
            }else{
	      Dist = sqrt( pow(remainder(q_n[3*i]  -q_n[3*j]  , SX), 2) +
                           pow(remainder(q_n[3*i+1]-q_n[3*j+1], SY), 2) +
                           pow((q_n[3*i+2]-q_n[3*j+2]), 2)
                           )-temprad;// does the distance between
            };
            contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
            if(contact){      //
                indi[Nbz]=i;  //
                indj[Nbz]=j;  //
                valij[Nbz]=1; //
                Nbz++;        //
            };                // if the distance is below the criteria we save this contact
        }; // end of loop for j
        Dist = fabs(q_n[SD*i+(SD-1)])-temprad; // does the distance between is in a radius of temprad ?
        contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
        if(contact){      //
            indi[Nbz]=i;  //
            indj[Nbz]=ND; // ND is the number of bottom
            valij[Nbz]=1; //
            Nbz++;        //
        };                // if the distance is below the criteria we save this contact

        if(NW>=2){ // if there are at least 2 walls
            Dist = fabs(q_n[SD*i])-temprad; // does the distance between left wall ... ?
            contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
            if(contact){        //
                indi[Nbz]=i;    //
                indj[Nbz]=ND+1; // ND+1 is the number of right wall
                valij[Nbz]=1;   //
                Nbz++;          //
            };                  // if the distance is below the criteria we save this contact

            if(NW>=3){ // if there are 3 walls
                Dist = fabs(q_n[SD*i]-SX)-temprad;// does the distance between right wall ?
                contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
                if(contact){        //
                    indi[Nbz]=i;    //
                    indj[Nbz]=ND+2; // ND+2 is the number of right wall
                    valij[Nbz]=1;   //
                    Nbz++;          //
                };                  // if the distance is below the criteria we save this
                                    // contact
            };

            if(SD==3){
                if(NW>=4){
                    Dist = fabs(q_n[3*i+1])-temprad;// does the distance between right wall ?
                    contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
                    if(contact){        //
                        indi[Nbz]=i;    //
                        indj[Nbz]=ND+3; // ND+3 is the number of front wall
                        valij[Nbz]=1;   //
                        Nbz++;          //
                    };                  // if the distance is below the criteria we save this
                                        // contact
                };

                if(NW==5){
                    Dist = fabs(q_n[3*i+1]-SY)-temprad; // does the distance between right wall ?
                    contact = !((Dist>=0.)||(fabs(Dist)<CrimEps));
                    if(contact){        //
                        indi[Nbz]=i;    //
                        indj[Nbz]=ND+4; // ND+4 is the number of front wall
                        valij[Nbz]=1;   //
                        Nbz++;          //
                    };                  // if the distance is below the criteria we save this
                                        // contact
                };
            };
        };
    };
    return Nbz; // return the number of contacts we have detected
};

// cget_DBT
int cget_DBT( unsigned *Bsubi, unsigned *Bsubj, unsigned *Tsubi, unsigned *Tsubj,
              real_t *Bval, real_t *Tval, real_t *Dzvec,
              unsigned const *GLUED, unsigned const *indi, unsigned const *indj,
              real_t const *q_n,
              unsigned const ND, unsigned const Nbz,
	      real_t const *SR, real_t const SX, real_t const SY,
              unsigned *Nbval )
{
    /*
     * The function cget_DBT builds the vector of distances D and the matrices B and T.
     * The matrices B and T are built following the COO sparse format.
     *
     * Arguments:
     * Bsubi : unsgined * = row indices of nonzero values in matrix B,
     * Bsubj : unsgined * = column indices of nonzero values in matrix B,
     * Tsubi : unsgined * = row indices of nonzero values in matrix T,
     * Tsubj : unsgined * = column indices of nonzero values in matrix T,
     *  Bval :   real_t * = nonzero values in matrix B,
     *  Tval :   real_t * = nonzero values in matrix T,
     * Dzvec :   real_t * = values of distances,
     * GLUED : unsgined * = GLUED[k]=1 if sphere k is glued, 0 otherwise,
     *  indi : unsgined * = indi[k] is the number of sphere i in contact numbered k,
     *  indj : unsgined * = indj[k] is the number of sphere j in contact numbered k,
     *   q_n :   real_t * = configuration positions,
     *    ND :   unsgined = number of spheres,
     *   Nbz :   unsgined = number of new contacts,
     *    SR :   real_t * = radii of spheres,
     *    SX :     real_t = side box length,
     *    SY :     real_t = side box width.
     */

    unsigned numcon, numrow=0, i, j, counterB=0, counterT=0; // counters
    unsigned gluedi=0, gluedj=0;   //
    real_t qix, qiz, qjx, qjz, norm, eijx=0., eijz=0.; // temporary variables
    real_t SRi, SRj; // values of spheres

    for(numcon=0;numcon<Nbz;++numcon) // loop over the set of new contacts
    {
        i = indi[numcon]; //
        j = indj[numcon]; // get the numbers of spheres i and j involved in contact k
        SRi = SR[i]; //
        SRj = SR[j]; // get values of radius of spheres i and j
        if(j<ND){              //
            gluedj = GLUED[j]; //
        }else{                 //
            gluedj = 0;        //
        };                     //
        gluedi = GLUED[i];     // handle glued spheres

        qix = q_n[2*i];    //
        qiz = q_n[2*i+1]; // get position of sphere i

        if(j>=ND){
            if(gluedi==0){
                if(j==ND){ // contact with bottom
                        Dzvec[numrow] = fabs(qiz)-SRi; // distance between i and bottom

                        Bsubj[counterB] = (2*i+1); //
                        Bsubi[counterB] = numrow;  //
                        Bval[counterB] = -1.;      // normal vector with bottom

                        Tsubi[counterT] = numrow;  //
                        Tsubj[counterT] = 2*i;     //
                        Tval[counterT] = -1.;      // tangential vector with bottom
                        counterB++;
                        counterT++;

                        Tsubi[counterT] = numrow; //
                        Tsubj[counterT] = 2*ND+i; //
                        Tval[counterT] = -SRi;    // axis of rotation with bottom
                        counterT++;

                }else if(j==ND+1){ // contact with left wall
                    Dzvec[numrow] = fabs(qix)-SRi; // distance between i and left wall

                    Bsubj[counterB] = 2*i;    //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB] = -1.;     // normal vector with left wall

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*i+1;  //
                    Tval[counterT] = 1.;      // tangential vector with left wall
                    counterB++;
                    counterT++;

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*ND+i; //
                    Tval[counterT] = -SRi;    // axis of rotation with left wall
                    counterT++;

                }else if(j==ND+2){ // contact with left wall
                    Dzvec[numrow] = fabs(qix-SX)-SRi; // distance between i and right wall

                    Bsubj[counterB] = 2*i;    //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB] = 1.;      // normal vector with right wall

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*i+1;  //
                    Tval[counterT] = -1.;     // tangential vector with right wall
                    counterB++;
                    counterT++;

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*ND+i; //
                    Tval[counterT] = -SRi;    // axis of rotation with right wall
                    counterT++;
                };
            }else{ // grain i is glued
                numrow--;
            };

        }else{ // contact between the spheres i and j

            if((gluedi==0)||(gluedj==0)){

                qjx = q_n[2*j];    //
                qjz = q_n[2*j+1]; // get position of sphere j

                norm = sqrt( pow(remainder(qix-qjx, SX), 2) +
			     pow((qiz-qjz), 2) ); // norm of normal vector
                eijx = remainder(qix-qjx, SX)/norm;
                eijz = (qiz-qjz)/norm; // eij is the normal vector to the contact

                //~ if((i==6)||(j==6)){
                    //~ printf("->i=%d,  j=%d\n",i,j);
                    //~ printf("Dzvec=%e\n",Dzvec[numrow]);
                    //~ printf("->gluedi=%d,  gluedj=%d\n",gluedi,gluedj);
                //~ };

                Dzvec[numrow] = norm - (SRi+SRj); // distance between the spheres i and j

                if(gluedi==0){ // bead i is not glued
                    Bsubj[counterB] = 2*i;    //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB] = -eijx;   // x component of normal vector for i

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*i;    //
                    Tval[counterT] = -eijz;   // x component of tangential vector for i
                    counterB++;
                    counterT++;

                    Bsubj[counterB] = (2*i+1); //
                    Bsubi[counterB] = numrow;  //
                    Bval[counterB] = -eijz;    // z component of normal vector for i

                    Tsubi[counterT] = numrow;  //
                    Tsubj[counterT] = (2*i+1); //
                    Tval[counterT] = eijx;     // z component of tangential vector for i
                    counterB++;
                    counterT++;

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*ND+i; //
                    Tval[counterT] = -SRi;    // axis of rotation for sphere i
                    counterT++;
                };

                if(gluedj==0){ // bead j is not glued
                    Bsubj[counterB] = 2*j;    //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB] = eijx;    // x component of normal vector for j

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*j;    //
                    Tval[counterT] = eijz;    // x component of tangential vector for j
                    counterB++;
                    counterT++;

                    Bsubj[counterB] = (2*j+1); //
                    Bsubi[counterB] = numrow;  //
                    Bval[counterB] = eijz;     // z component of normal vector for j

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*j+1;  //
                    Tval[counterT] = -eijx;   // z component of tangential vector for j
                    counterB++;
                    counterT++;

                    Tsubi[counterT] = numrow; //
                    Tsubj[counterT] = 2*ND+j; //
                    Tval[counterT] = -SRj;    // axis of rotation for sphere j
                    counterT++;
                };
            }else{
                numrow--;
            };
        };
        numrow++; // increment the number of the line for matrix B and T
    };

    Nbval[0] = counterB; //
    Nbval[1] = counterT; // save number of values

    return numrow;

};

// basis
int basis( real_t const *eij, real_t *fij, real_t *gij ){
    /*
     * The function build_basis can be used to create the orthonormal basis to the contact ij
     * from the given vector eij.
     *
     */
    unsigned Nbz = 0; // number of zeros in eij
    unsigned i, coord;
    real_t norm;

    for(i=0;i<3;i++){    //
        if(eij[i]==0.){  //
            Nbz++;       //
        };               // find zeros in eij and initialize fij and gij to zero vectors
        fij[i]=0.;       //
        gij[i]=0.;       //
    };                   //

    if(Nbz==0){ // there is no zeros                                         //
        norm   = sqrt( pow(eij[1],2) + pow(eij[2],2) ); // norm of fij       //
        fij[0] = 0.;           //                                            //
        fij[1] = -eij[2]/norm; //                                            //
        fij[2] = eij[1]/norm;  // fij unit                                   //
    }else if(Nbz==1){                                                        //
        if(eij[0]==0.){                                                      //
            norm   = sqrt( pow(eij[1],2) + pow(eij[2],2) ); // norm of fij   //
            fij[1] = -eij[2]/norm; //                                        //
            fij[2] = eij[1]/norm;  // fij unit                               //
        }else if(eij[1]==0.){                                                //
            norm   = sqrt( pow(eij[0],2) + pow(eij[2],2) ); // norm of fij   //
            fij[0] = -eij[2]/norm; //                                        //
            fij[2] = eij[0]/norm;  // fij unit                               //
        }else{                                                               //
            norm   = sqrt( pow(eij[0],2) + pow(eij[1],2) ); // norm of fij   // fij unit
            fij[1] = -eij[0]/norm; //                                        //
            fij[0] = eij[1]/norm;  // fij unit                               //
        };                                                                   //
    }else{                                                                   //
        for(i=0;i<3;i++){    //                                              //
            if(eij[i]!=0.){  // find coordinates                             //
                coord=i;     //                                              //
            };               //                                              //
        };                                                                   //
        if(coord==0){             //                                         //
            fij[1] = eij[0];      //                                         //
        }else{                    // fij unit                                //
            fij[0] = eij[coord];  //                                         //
        };                        //                                         //
    };                                                                       //

    gij[0] = eij[1]*fij[2]-eij[2]*fij[1]; //
    gij[1] = eij[2]*fij[0]-eij[0]*fij[2]; // gij unit as gij = eij x fij
    gij[2] = eij[0]*fij[1]-eij[1]*fij[0]; //

    return 0;
};

// cget_DBT_3D
int cget_DBT_3D( unsigned *Bsubi, unsigned *Bsubj, unsigned *Tsubi, unsigned *Tsubj,
                 real_t *Bval, real_t *Tval, real_t *Dzvec,
                 unsigned const *GLUED, unsigned const *indi, unsigned const *indj,
                 real_t const *q_n,
                 unsigned const ND, unsigned const Nbz, real_t const *SR,
		 real_t const SX, real_t const SY, unsigned *Nbval )
{
    /*
     * The function cget_DBT builds the vector of distances D and the matrices B and T.
     * The matrices B and T are built following the COO sparse format.
     *
     * Arguments:
     * Bsubi : unsgined * = row indices of nonzero values in matrix B,
     * Bsubj : unsgined * = column indices of nonzero values in matrix B,
     * Tsubi : unsgined * = row indices of nonzero values in matrix T,
     * Tsubj : unsgined * = column indices of nonzero values in matrix T,
     *  Bval :   real_t * = nonzero values in matrix B,
     *  Tval :   real_t * = nonzero values in matrix T,
     * Dzvec :   real_t * = values of distances,
     * GLUED : unsgined * = GLUED[k]=1 if sphere k is glued, 0 otherwise,
     *  indi : unsgined * = indi[k] is the number of sphere i in contact numbered k,
     *  indj : unsgined * = indj[k] is the number of sphere j in contact numbered k,
     *   q_n :   real_t * = configuration positions,
     *    ND :   unsgined = number of spheres,
     *   Nbz :   unsgined = number of new contacts,
     *    SR :   real_t * = radii of spheres,
     *    SX :     real_t = side box length,
     *    SY :     real_t = side box length.
     */

    unsigned numcon, numrow=0, i, j, k, counterB=0, counterT=0; // counters
    unsigned Trow=0;
    unsigned gluedi=0, gluedj=0, NSx3=3*ND;   //
    real_t qix, qiz, qjx, qjz, norm; // temporary variables
    real_t SRi, SRj; // values of spheres
    real_t qiy, qjy;
    real_t eij[3], fij[3], gij[3]; // the unitar vectors

    for(numcon=0;numcon<3;numcon++){ //
        eij[numcon] = 0.;            //
        fij[numcon] = 0.;            //
        gij[numcon] = 0.;            //
    };                               // initialize eij, fij, gij

    for(numcon=0;numcon<Nbz;++numcon) // loop over the set of new contacts
    {
        //~ printf("je suis ici %d", numcon);
        i = indi[numcon]; //
        j = indj[numcon]; // get the numbers of spheres i and j involved in contact k
        SRi = SR[i]; //
        SRj = SR[j]; // get values of radius of spheres i and j
        if(j<ND){              //
            gluedj = GLUED[j]; //
        }else{                 //
            gluedj = 0;        //
        };                     //
        gluedi = GLUED[i];     // handle glued spheres

        qix = q_n[3*i];    // x axis
        qiy = q_n[3*i+1];  // y axis
        qiz = q_n[3*i+2];  // z axis get position of sphere i

        if(j>=ND){
            if(gluedi==0){
                if(j==ND){ // contact with bottom

                    Dzvec[numrow] = fabs(qiz)-SRi; // distance between i and bottom

                    // eij = (0,  0,  1)
                    // fij = (0,  1,  0)
                    // gij = (-1, 0,  0)

                    Bsubj[counterB] = 3*i+2;  // -eij                       //
                    Bsubi[counterB] = numrow; //                            //
                    Bval[counterB]  = -1.;    // normal vector with bottom  // Eij
                    counterB++;               //                            //
                    numrow++;

                    Tsubi[counterT] = Trow;  // -fij                 //
                    Tsubj[counterT] = 3*i+1; //                      //
                    Tval[counterT]  = -1.;   // translational comp   //
                    counterT++;              //                      //
                                                                     // Fij
                    Tsubi[counterT] = Trow;     // ri*gij            //
                    Tsubj[counterT] = NSx3+3*i; //                   //
                    Tval[counterT]  = -SRi;     // rot. comp of -fij //
                    counterT++;                 //                   //
                                                                     //
                    Trow++;                                          //

                    Tsubi[counterT] = Trow;  // -gij                    //
                    Tsubj[counterT] = 3*i;   //                         //
                    Tval[counterT]  = 1.;    //                         //
                    counterT++;              // translational comp      //
                                                                        // Gij
                    Tsubi[counterT] = Trow;       // -ri*fij            //
                    Tsubj[counterT] = NSx3+3*i+1; //                    //
                    Tval[counterT]  = -SRi;       // rot. comp of -gij  //
                    counterT++;                   //                    //
                                                                        //
                    Trow++;                                             //

                }else if(j==ND+1){ // contact with left wall

                    Dzvec[numrow] = fabs(qix)-SRi; // distance between i and left wall

                    // eij = ( 1, 0, 0)
                    // fij = ( 0, 1, 0)
                    // gij = ( 0, 0, 1)

                    Bsubj[counterB] = 3*i;    // -eij                         //
                    Bsubi[counterB] = numrow; //                              //
                    Bval[counterB]  = -1.;    // normal vector with left wall // Eij
                    counterB++;                                               //
                    numrow++;

                    Tsubi[counterT] = Trow;  // -fij                   //
                    Tsubj[counterT] = 3*i+1; //                        //
                    Tval[counterT]  = -1.;   // translational comp     //
                    counterT++;              //                        //
                                                                       // Fij
                    Tsubi[counterT] = Trow;       // ri*gij            //
                    Tsubj[counterT] = NSx3+3*i+2; //                   //
                    Tval[counterT]  = SRi;        // rot. comp of -fij //
                    counterT++;                   //                   //
                                                                       //
                    Trow++;                                            //

                    Tsubi[counterT] = Trow;  // -gij                    //
                    Tsubj[counterT] = 3*i+2; //                         //
                    Tval[counterT]  = -1.;   //                         //
                    counterT++;              // translational comp      //
                                                                        // Gij
                    Tsubi[counterT] = Trow;       // -ri*fij            //
                    Tsubj[counterT] = NSx3+3*i+1; //                    //
                    Tval[counterT]  = -SRi;       // rot. comp of -gij  //
                    counterT++;                   //                    //
                                                                        //
                    Trow++;                                             //

                }else if(j==ND+2){ // contact with right wall

                    Dzvec[numrow] = fabs(qix-SX)-SRi; // distance between i and right wall

                    // eij = (-1,  0,  0)
                    // fij = ( 0, -1,  0)
                    // gij = ( 0,  0,  1)

                    Bsubj[counterB] = 3*i;    // -eij                         //
                    Bsubi[counterB] = numrow; //                              //
                    Bval[counterB]  = 1.;     // normal vector with left wall // Eij
                    counterB++;                                               //
                    numrow++;

                    Tsubi[counterT] = Trow;  // -fij                   //
                    Tsubj[counterT] = 3*i+1; //                        //
                    Tval[counterT]  = 1.;    // translational comp     //
                    counterT++;              //                        //
                                                                       // Fij
                    Tsubi[counterT] = Trow;       // ri*gij            //
                    Tsubj[counterT] = NSx3+3*i+2; //                   //
                    Tval[counterT]  = SRi;        // rot. comp of -fij //
                    counterT++;                   //                   //
                                                                       //
                    Trow++;                                            //

                    Tsubi[counterT] = Trow;  // -gij                    //
                    Tsubj[counterT] = 3*i+2; //                         //
                    Tval[counterT]  = -1.;   //                         //
                    counterT++;              // translational comp      //
                                                                        // Gij
                    Tsubi[counterT] = Trow;       // -ri*fij            //
                    Tsubj[counterT] = NSx3+3*i+1; //                    //
                    Tval[counterT]  = SRi;        // rot. comp of -gij  //
                    counterT++;                   //                    //
                                                                        //
                    Trow++;                                             //
                }else if(j==ND+3){ // contact with near wall

                    Dzvec[numrow] = fabs(qiy)-SRi; // distance between i and near wall

                    // eij = (0,  1,  0)
                    // fij = (1,  0,  0)
                    // gij = (0,  0, -1)

                    Bsubj[counterB] = 3*i+1;  // -eij                         //
                    Bsubi[counterB] = numrow; //                              //
                    Bval[counterB]  = -1.;    // normal vector with left wall // Eij
                    counterB++;                                               //
                    numrow++;

                    Tsubi[counterT] = Trow;  // -fij                   //
                    Tsubj[counterT] = 3*i;   //                        //
                    Tval[counterT]  = -1.;   // translational comp     //
                    counterT++;              //                        //
                                                                       // Fij
                    Tsubi[counterT] = Trow;       // ri*gij            //
                    Tsubj[counterT] = NSx3+3*i+2; //                   //
                    Tval[counterT]  = -SRi;       // rot. comp of -fij //
                    counterT++;                   //                   //
                                                                       //
                    Trow++;                                            //

                    Tsubi[counterT] = Trow;  // -gij                  //
                    Tsubj[counterT] = 3*i+2; //                       //
                    Tval[counterT]  = 1.;    //                       //
                    counterT++;              // translational comp    //
                                                                      // Gij
                    Tsubi[counterT] = Trow;     // -ri*fij            //
                    Tsubj[counterT] = NSx3+3*i; //                    //
                    Tval[counterT]  = -SRi;     // rot. comp of -gij  //
                    counterT++;                 //                    //
                                                                      //
                    Trow++;                                           //

                }else if(j==ND+4){ // contact with far wall

                    Dzvec[numrow] = fabs(qiy-SY)-SRi; // distance between i and far wall

                    // eij = (0, -1,  0)
                    // fij = (1,  0,  0)
                    // gij = (0,  0,  1)

                    Bsubj[counterB] = 3*i+1;  // -eij                         //
                    Bsubi[counterB] = numrow; //                              //
                    Bval[counterB]  = 1.;     // normal vector with left wall // Eij
                    counterB++;                                               //
                    numrow++;

                    Tsubi[counterT] = Trow;  // -fij                   //
                    Tsubj[counterT] = 3*i;   //                        //
                    Tval[counterT]  = -1.;   // translational comp     //
                    counterT++;              //                        //
                                                                       // Fij
                    Tsubi[counterT] = Trow;       // ri*gij            //
                    Tsubj[counterT] = NSx3+3*i+2; //                   //
                    Tval[counterT]  = SRi;        // rot. comp of -fij //
                    counterT++;                   //                   //
                                                                       //
                    Trow++;                                            //

                    Tsubi[counterT] = Trow;  // -gij                  //
                    Tsubj[counterT] = 3*i+2; //                       //
                    Tval[counterT]  = -1.;   //                       //
                    counterT++;              // translational comp    //
                                                                      // Gij
                    Tsubi[counterT] = Trow;     // -ri*fij            //
                    Tsubj[counterT] = NSx3+3*i; //                    //
                    Tval[counterT]  = -SRi;     // rot. comp of -gij  //
                    counterT++;                 //                    //
                                                                      //
                    Trow++;                                           //
                };
            };

        }else{ // contact between the spheres i and j

            if((gluedi==0)||(gluedj==0)){

                qjx = q_n[3*j];   // x axis
                qjy = q_n[3*j+1]; // y axis
                qjz = q_n[3*j+2]; // z axis get position of sphere i

                norm = sqrt(pow(remainder(qix-qjx, SX), 2) +
			    pow(remainder(qiy-qjy, SY), 2) +
			    pow((qiz-qjz),2));// norm of normal vector

                eij[0] = remainder(qix-qjx, SX)/norm;
                eij[1] = remainder(qiy-qjy, SY)/norm;
                eij[2] = (qiz-qjz)/norm;   // eij is the normal vector to the contact

                Dzvec[numrow] = norm - (SRi+SRj); // distance between the spheres i and j

                //~ if((gluedi==1)&&(gluedj==1)){
                    //~ if(qiz == min(qiz,qjz)){ gluedj = 0; }else{ gluedi = 0; }; };

                // Eij = ( 0...0 -eij 0...0 eij 0...0 | 0...............................0 )
                // Fij = ( 0...0 -fij 0...0 fij 0...0 | 0...0 ri.gij  0...0 rj.gij  0...0 )
                // Gij = ( 0...0 -gij 0...0 gij 0...0 | 0...0 -ri.fij 0...0 -rj.fij 0...0 )

                basis( eij, fij, gij ); // build fij and eij

                if((gluedi==0)&&(gluedj==0)){ // bead i and j are not glued

                    for(k=0;k<3;k++){                //
                        Bsubj[counterB] = 3*i+k;     //
                        Bsubi[counterB] = numrow;    //
                        Bval[counterB]  = -eij[k];   //
                        counterB++;                  //
                                                     // Eij
                        Bsubj[counterB] = 3*j+k;     //
                        Bsubi[counterB] = numrow;    //
                        Bval[counterB]  = eij[k];    //
                        counterB++;                  //
                    };                               //
                    numrow++;

                    for(k=0;k<3;k++){                 //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = 3*i+k;      //
                        Tval[counterT]  = -fij[k];    //
                        counterT++;                   //
                                                      //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = 3*j+k;      //
                        Tval[counterT]  = fij[k];     // Fij
                        counterT++;                   //
                                                      //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = NSx3+3*i+k; //
                        Tval[counterT]  = SRi*gij[k]; //
                        counterT++;                   //
                                                      //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = NSx3+3*j+k; //
                        Tval[counterT]  = SRj*gij[k]; //
                        counterT++;                   //
                    };                                //
                                                      //
                    Trow++;                           //

                    for(k=0;k<3;k++){                  //
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = 3*i+k;       //
                        Tval[counterT]  = -gij[k];     //
                        counterT++;                    //
                                                       //
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = 3*j+k;       //
                        Tval[counterT]  = gij[k];      //
                        counterT++;                    //
                                                       // Gij
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = NSx3+3*i+k;  //
                        Tval[counterT]  = -SRi*fij[k]; //
                        counterT++;                    //
                                                       //
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = NSx3+3*j+k;  //
                        Tval[counterT]  = -SRj*fij[k]; //
                        counterT++;                    //
                    };                                 //
                                                       //
                    Trow++;                            //

                }else if((gluedi==0)&&(gluedj==1)){ // j is glued

                    for(k=0;k<3;k++){                //
                        Bsubj[counterB] = 3*i+k;     //
                        Bsubi[counterB] = numrow;    // Eij
                        Bval[counterB]  = -eij[k];   //
                        counterB++;                  //
                    };                               //
                    numrow++;

                    for(k=0;k<3;k++){                 //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = 3*i+k;      //
                        Tval[counterT]  = -fij[k];    //
                        counterT++;                   //
                                                      // Fij
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = NSx3+3*i+k; //
                        Tval[counterT]  = SRi*gij[k]; //
                        counterT++;                   //
                    };                                //
                                                      //
                    Trow++;                           //

                    for(k=0;k<3;k++){                  //
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = 3*i+k;       //
                        Tval[counterT]  = -gij[k];     //
                        counterT++;                    //
                                                       // Gij
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = NSx3+3*i+k;  //
                        Tval[counterT]  = -SRi*fij[k]; //
                        counterT++;                    //
                                                       //
                    };                                 //
                                                       //
                    Trow++;                            //

                }else{ // i is glued

                    for(k=0;k<3;k++){                //
                        Bsubj[counterB] = 3*j+k;     //
                        Bsubi[counterB] = numrow;    // Eij
                        Bval[counterB]  = eij[k];    //
                        counterB++;                  //
                    };                               //
                    numrow++;

                    for(k=0;k<3;k++){                 //
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = 3*j+k;      //
                        Tval[counterT]  = fij[k];     //
                        counterT++;                   //
                                                      // Fij
                        Tsubi[counterT] = Trow;       //
                        Tsubj[counterT] = NSx3+3*j+k; //
                        Tval[counterT]  = SRj*gij[k]; //
                        counterT++;                   //
                    };                                //
                                                      //
                    Trow++;                           //

                    for(k=0;k<3;k++){                  //
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = 3*j+k;       //
                        Tval[counterT]  = gij[k];      //
                        counterT++;                    //
                                                       // Gij
                        Tsubi[counterT] = Trow;        //
                        Tsubj[counterT] = NSx3+3*j+k;  //
                        Tval[counterT]  = -SRj*fij[k]; //
                        counterT++;                    //
                                                       //
                    };                                 //
                                                       //
                    Trow++;                            //
                };
            };
        };
    };

    Nbval[0] = counterB; //
    Nbval[1] = counterT; // save number of values

    return numrow;

};

/*
 * Normal problem part
 *
 * */

unsigned wtd_NPro(  real_t const WH, unsigned const SD,
                    real_t const *q_n,
                    int const *indices, int const *indptr,
                    unsigned const ND,
                    real_t const *SR, real_t const SX, real_t const SY, real_t const CRI        )
{
    /*
     * The function wtd_nPro loops over all the set of possible contacts and detects whether
     * there is a contact. When it is true, it stops the loop and return we must work with
     * the set of neighbourgs given through indices and indptr arrays.
     * It returns then un integer wtd, 1 if there is a contact, 0 otherwise.
     *
     * Arguments:
     *   WH : real_t = right wall height,
     *  q_n :   real_t * = configuration positions,
     * indices : unsigned * = NBGl.indices in a CSR format, see scipy.sparse documentation,
     *  indptr : unsigned * = NBGl.indices in a CSR format, see scipy.sparse documentation,
     *                      = More precisely, the bead i has got a set of neighbourgs which
     *                        is described by the arrays indices and indptr. Indeed, the
     *                        beads in contact with the bead i are given by the set
     *                        indices[indptr[i]:indptr[i+1]]
     *   ND :   unsigned = number of spheres,
     *   SR : real_t array* = radii of spheres,
     *   SX :     real_t = side box length,
     *   SY :     real_t = side box width,
     *  CRI :     real_t = overlap criteria.
     *
     * Returns:
     *  wtd : int = 1 there is a contact, 0 otherwise.
     *
     * Description:
     * If (Dist >= 0) ====> there is no contact.
     * Elseif (-CRI < Dist < 0) ===> there is no contact.
     *
     * Else ===> there is a contact.
     */
    unsigned k, i, j;  // counters
    unsigned wtd=0; //
    //~ real_t Dist, Eps=1.E-15; // temporary variables, Nbz is the
    //~ real_t CrimEps=CRI-Eps;  // number of effective contacts. The parameter Eps is
                             // useful to handle problem of troncature. Especially
                             // when we want a contact when Dist = Cri in the
                             // equality.
    //~ unsigned contact=0; // boolean variable
    real_t Dist, SRi, SRj;    // the values of radii

    i = 0;
    while((wtd==0)&&(i<ND)){ // we take a bead i
        SRi = SR[i];         // get radius of i
        k=indptr[i];
        while((wtd==0)&&(k<indptr[i+1])){ // we loop over the neighbourgs of bead i

            j = indices[k]; // we take the number of the neighbourg
            SRj = SR[j];    // get radius of j
            if (j<ND){      // j is a bead
                if(SD==2){
		  Dist = sqrt( pow(remainder(q_n[2*i]  -q_n[2*j], SX), 2) +
                               pow((q_n[2*i+1]-q_n[2*j+1]),2) )-(SRi+SRj); // get the distance
                                                                             // between
                                                                             // grains i and j
                }else{
		  Dist = sqrt( pow(remainder(q_n[3*i]  -q_n[3*j]  , SX),2) +
                               pow(remainder(q_n[3*i+1]-q_n[3*j+1], SY),2) +
			       pow((q_n[3*i+2]-q_n[3*j+2]),2) )-(SRi+SRj); // get the distance
                };
                if(Dist<CRI){    //
                    wtd++;      //
                };              // if the distance is below the criteria we save this contact
            };
            if (j==ND){ // j is the bottom
                Dist = fabs(q_n[SD*i+(SD-1)])-SRi; // get the distance with bottom
                if(Dist<CRI){    //
                    wtd++;      //
                };              // if the distance is below the criteria we save this contact
            };
            if (j==ND+1){ // j is the left wall
                Dist = fabs(q_n[SD*i])-SRi; // get the distance with left wall
                if(Dist<CRI){    //
                    wtd++;      //
                };              // if the distance is below the criteria we save this contact
            };
            if (j==ND+2){ // if there are 3 walls
                Dist = fabs(q_n[SD*i]-SX)-SRi; // get the distance with right wall
                if(((SD==2)&&(q_n[SD*i+(SD-1)]>WH))||(SD==3)){
                    if(Dist<CRI){    //
                        wtd++;      //
                    };              // if the distance is below the criteria we save it
                };
            };
            if (SD==3){
                if (j==ND+3){ // j is the near wall
                    Dist = fabs(q_n[3*i+1])-SRi; // get the distance with near wall
                    if(Dist<CRI){    //
                        wtd++;      //
                    };              // if the distance is below the criteria we save this contact
                };
                if (j==ND+4){ // j is the far wall
                    Dist = fabs(q_n[3*i+1]-SY)-SRi; // get the distance with far wall
                    if(Dist<CRI){    //
                        wtd++;      //
                    };              // if the distance is below the criteria we save this contact
                };
            };
            k++; // increment k
        }; // end of loop over the set of neighbourgs

        i++; // increment i
    }; // end of the loop over the beads

    return wtd; // return the number of contacts we have detected
};
// cget_DBT
int cget_DB_Npro( unsigned *Bsubi, unsigned *Bsubj, real_t *Bval, real_t *Dzvec,
                  unsigned const *GLUED, unsigned *indi, unsigned const *indj,
                  real_t const *q_n, real_t const WH,
                  unsigned const ND, unsigned const SD, unsigned const Nbz, real_t const *SR,
                  real_t const SX, real_t const SY, unsigned *NBval )
{
    /*
     * The function cget_DBT builds the vector of distances D and the matrices B and T.
     * The matrices B and T are built following the COO sparse format.
     *
     * Arguments:
     * Bsubi : unsgined * = row indices of nonzero values in matrix B,
     * Bsubj : unsgined * = column indices of nonzero values in matrix B,
     *  Bval :   real_t * = nonzero values in matrix B,
     * Dzvec :   real_t * = values of distances,
     * GLUED : unsgined * = GLUED[k]=1 if sphere k is glued, 0 otherwise,
     *  indi : unsgined * = indi[k] is the number of sphere i in contact numbered k,
     *  indj : unsgined * = indj[k] is the number of sphere j in contact numbered k,
     *   q_n :   real_t * = configuration positions,
     *    WH :     real_t = right wall height,
     *    ND :   unsgined = number of spheres,
     *   Nbz :   unsgined = number of new contacts,
     *    SR :   real_t * = radii of spheres,
     *    SX :     real_t = side box length,
     *    SY :     real_t = side box width,
     * NBval :        int = number of values in the arrays Bsubi, Bsubj and Bval.
     *
     * Returns:
     * numrow : unsigned = number of row in matrix B (or number of contacts really implemented)
     */

    unsigned numcon, i, j, counterB=0, numrow=0; // counters
    unsigned gluedi=0, gluedj=0;   //
    real_t qix, qiz, qjx, qjz, norm, eijx=0., eijz=0.; // temporary variables
    real_t SRi, SRj; // values of spheres
    real_t qiy=0., qjy=0., eijy=0.;

    for(numcon=0;numcon<Nbz;++numcon) // loop over the set of new contacts
    {
        i = indi[numcon]; //
        j = indj[numcon]; // get the numbers of spheres i and j involved in contact k
        SRi = SR[i]; //
        SRj = SR[j]; // get values of radius of spheres i and j
        if(j<ND){              //
            gluedj = GLUED[j]; //
        }else{                 //
            gluedj = 0;        //
        };                     //
        gluedi = GLUED[i];     // handle glued spheres

        qix = q_n[SD*i];        // x axis
        if(SD==3){
            qiy = q_n[ 3*i+1 ]; // y axis
        };
        qiz = q_n[SD*i+(SD-1)]; // z axis get position of sphere i

        if(j>=ND){
            if(gluedi==0){
                if(j==ND){ // contact with bottom
                    Dzvec[numrow] = fabs(qiz)-SRi; // distance between i and bottom

                    Bsubj[counterB] = (SD*i+(SD-1)); //
                    Bsubi[counterB] = numrow;        //
                    Bval[counterB]  = -1.;            // normal vector with bottom
                    counterB++;

                }else if(j==ND+1){ // contact with left wall
                    Dzvec[numrow] = fabs(qix)-SRi; // distance between i and left wall

                    Bsubj[counterB] = SD*i;    //
                    Bsubi[counterB] = numrow;  //
                    Bval[counterB]  = -1.;      // normal vector with left wall
                    counterB++;

                }else if(j==ND+2){ // there is a contact with right wall
                    if(((SD==2)&&(q_n[SD*i+(SD-1)]>WH))||(SD==3)){ // the grain is not below the right wall
                        Dzvec[numrow] = fabs(qix-SX)-SRi; // distance between i and right wall

                        Bsubj[counterB] = SD*i;   //
                        Bsubi[counterB] = numrow; //
                        Bval[counterB]  = 1.;      // normal vector with right wall
                        counterB++;
                    }else{
                        indi[numcon] = indj[numcon]; // set to be equals in order to be removed
                        numrow--; // we remove this contact since it doesn't exist
                    };
                }else if(j==ND+3){
                    Dzvec[numrow] = fabs(qiy)-SRi; // distance between i and near wall

                    Bsubj[counterB] = 3*i+1;    //
                    Bsubi[counterB] = numrow;  //
                    Bval[counterB]  = -1.;      // normal vector with left wall
                    counterB++;

                }else if(j==ND+4){
                    Dzvec[numrow] = fabs(qiy-SY)-SRi; // distance between i and far wall

                    Bsubj[counterB] = 3*i+1;    //
                    Bsubi[counterB] = numrow;  //
                    Bval[counterB]  = 1.;      // normal vector with left wall
                    counterB++;
                };

            }else{ // grain i is glued
                indi[numcon] = indj[numcon]; // set to be equals in order to be removed
                numrow--;
            };

        }else{ // contact between the spheres i and j

            if((gluedi==0)||(gluedj==0)){
	      
                qjx = q_n[SD*j];        // x axis
                if(SD==3){
                    qjy = q_n[ 3*j+1 ]; // y axis
                };
                qjz = q_n[SD*j+(SD-1)]; // z axis get position of sphere j

                norm = sqrt(pow(remainder(qix-qjx, SX), 2) +
			    pow(remainder(qiy-qjy, SY), 2) +
			    pow((qiz-qjz),2)); // norm of normal vector

                eijx = remainder(qix-qjx, SX)/norm;   // x axis
                eijz = (qiz-qjz)/norm;   // z axis eij is the normal vector to the contact

                Dzvec[numrow] = norm - (SRi+SRj); // distance between the spheres i and j

                if(gluedi==0){ // bead i is not glued

                    Bsubj[counterB] = SD*i;   //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB]  = -eijx;  // x component of normal vector for i
                    counterB++;

                    if(SD==3){
		      eijy = remainder(qiy-qjy, SY)/norm;    // y axis for eij
                        Bsubj[counterB] = 3*i+1;  //
                        Bsubi[counterB] = numrow; //
                        Bval[counterB]  = -eijy;  // y component of normal vector for i
                        counterB++;
                    };

                    Bsubj[counterB] = (SD*i+(SD-1)); //
                    Bsubi[counterB] = numrow;        //
                    Bval[counterB]  = -eijz;         // z component of normal vector for i
                    counterB++;
                };

                if(gluedj==0){ // bead j is not glued
                    Bsubj[counterB] = SD*j;   //
                    Bsubi[counterB] = numrow; //
                    Bval[counterB]  = eijx;   // x component of normal vector for j
                    counterB++;

                    if(SD==3){
                        eijy = (qiy-qjy)/norm;  // y axis for eij
                        Bsubj[counterB] = 3*j+1;  //
                        Bsubi[counterB] = numrow; //
                        Bval[counterB]  = eijy;   // y component of normal vector for i
                        counterB++;
                    };

                    Bsubj[counterB] = (SD*j+(SD-1)); //
                    Bsubi[counterB] = numrow;        //
                    Bval[counterB]  = eijz;          // z component of normal vector for j
                    counterB++;
                };
            }else{
                indi[numcon] = indj[numcon]; // set to be equals in order to be removed
                numrow--;
            };
        };
        numrow++; // increment the number of the line for matrix B
    };
    NBval[0] = counterB;
    return numrow;
};

/*
 *
 * function for pov
 *
 * */

// make_pov
int make_pov(  double const *q_n, double const *u_n,
               double const WH, unsigned const SD,
               unsigned const ND, unsigned const NW, unsigned const fnum,
               double const SX, double const SY, double const SH,
	       double const GLCRI,double const *SR, double const SRmed,
               double const Xcam, double const Ycam, double const Zcam,
               double const Xeye, double const Yeye, double const Zeye,
               double const coeff,
               double const lwidth                  )
{
    /*
     * Write pov files to make outputs with povray software.
     *
     * Arguments:
     *       q_n = double array : positions values vector
     *       u_n = double array : velocities values vector
     *        WH = double : right wall height
     *        SD = unsigned : space dimension
     *        ND = unsigned : number of spheres
     *        NW = unsigned : number of walls
     *      fnum = unsigned : file number
     *        SX = double : side length
     *        SY = double : side width
     *        SR = double * : radii of spheres
     *      Xcam = double : X camera location
     *      Ycam = double : Y canera location
     *      Zcam = double : distance between camera and eye
     * lwidth = double : line width
     */

    unsigned i;      // counter
    FILE *fpos;      // file
    char vlname[50]; // file name
    double vitesse, conv=180./M_PI; // temporary variables
    double qx, qy, qz, rx, ry, rz, rt, tenSX;

    sprintf(vlname,"./GCD_%d.pov",fnum); // create a name
    fpos=fopen(vlname,"w");              // open pov file

    fprintf(fpos, "#include \"colors.inc\"\n"  ); // graphic librairies

    if( SD==2 ){
        fprintf(fpos,"background { color Black }\n"); // background color

        // camera location
        fprintf(fpos,"camera { location <%.3e, %.3e, %.3e> look_at <%.3e, %.3e, %.3e> }\n",
                                         Xcam, Zcam, Ycam,         Xeye, Zeye, Yeye    );

        // light source
        fprintf(fpos,"light_source { <%.3e, %.3e, %.3e> color White}\n",1000.,1000.,-1000.);

        for(i=0;i<ND;i++){ // loop over grains set
            // vitesse = log(1+norm(u))
            vitesse = coeff*log(1+(sqrt(  pow(u_n[2*i],2) +
                                          pow(u_n[2*i+1],2) )/(sqrt(9.81e3*5.*SR[i]))))/log(4);

            // draw sphere i
            fprintf(fpos,"sphere { <0., 0., 0.>, %.6e",SR[i]);
            fprintf(fpos," texture { pigment { checker color White");
            fprintf(fpos," color rgb<%.6e,0,%.6e> scale %.3e } }",vitesse ,1.-vitesse,SR[i] );
            fprintf(fpos," rotate <0., 0., %.6e>", q_n[2*ND+i]*conv);
            fprintf(fpos," translate <%.6e, %.6e, 0.> }\n",q_n[2*i],q_n[2*i+1]);
        };

        // wall 1
        //~ fprintf(fpos, "cylinder { <%lf,%lf,0.>, <%lf, %lf, 0. >,", -20*SX, 0., 10*SX, 0.);
        //~ fprintf(fpos, " %lf texture { pigment { color White}} }\n", lwidth );

        fprintf(fpos, "cylinder { <%lf,%lf,0.>, <%lf, %lf, 0. >,", -20*SX, 140., 10*SX, 140.);
        fprintf(fpos, " %lf texture { pigment { color White}} }\n", lwidth );

        fprintf(fpos, "cylinder { <%lf,%lf,0.>, <%lf, %lf, 0. >,", -20*SX, 70., 10*SX, 70.);
        fprintf(fpos, " %lf texture { pigment { color White}} }\n", lwidth );

        // wall 3
        if(NW==3){
            if(WH<SH){
                fprintf(fpos, "cylinder { <%.6e,%.6e,0>, <%.6e, %.6e, 0 >,", SX,WH, SX,4*SX+WH);
                fprintf(fpos, " %.6e texture { pigment { color White} } }\n",
                lwidth);
            };
        };

        // wall 2
        if(NW>=2){
            fprintf(fpos, "cylinder { <%.6e,%.6e,0>, <%.6e, %.6e, 0 >,",0., 0.,0.,4*SX);
            fprintf(fpos, " %.6e texture { pigment { color White}} }\n", lwidth );
        };

    }
    else{

        fprintf(fpos, "#include \"quaternions.inc\"\n"  ); // quaternions library

        fprintf(fpos,"background { color MediumGoldenrod }\n"); // background color

        // camera location
        fprintf(fpos,"camera { location <%.3e, %.3e, %.3e> look_at <%.3e, %.3e, %.3e> }\n",
                                         Xcam, Zcam, Ycam,          Xeye, Zeye, Yeye      );
        // light source
        fprintf(fpos,"light_source { <500., 500., 500.> color White}\n" );

        tenSX = 10.*SX;
        // x axis
        fprintf(fpos, "cylinder { <%lf,%lf,%lf>, <%lf,%lf,%lf>,",0.,SRmed,0.,tenSX,SRmed,0.);
        fprintf(fpos, " %lf texture { pigment { color Black }} }\n", lwidth );
        fprintf(fpos, "cylinder { <%lf,%lf,%lf>, <%lf,%lf,%lf>,",-tenSX,SRmed,-tenSX,2*tenSX,SRmed,-tenSX);
        fprintf(fpos, " %lf texture { pigment { color Black }} }\n", 5*lwidth );

        // y axis ( in povray it is the z axis )
        fprintf(fpos, "cylinder { <%lf,%lf,%lf>, <%lf,%lf,%lf>,",0.,SRmed,0.,0.,SRmed,tenSX);
        fprintf(fpos, " %lf texture { pigment { color Black }} }\n", lwidth );
        fprintf(fpos, "cylinder { <%lf,%lf,%lf>, <%lf,%lf,%lf>,",-tenSX,SRmed,-tenSX,-tenSX,SRmed,2*tenSX);
        fprintf(fpos, " %lf texture { pigment { color Black }} }\n", 5*lwidth );

        // z axis ( in povray it is the y axis )
        fprintf(fpos, "cylinder { <%lf,%lf,%lf>, <%lf,%lf,%lf>,",-tenSX,0.,-tenSX,-tenSX,2*tenSX,-tenSX);
        fprintf(fpos, " %lf texture { pigment { color Black }} }\n", 5*lwidth );

        // bottom
        fprintf(fpos, "box { < -%lf, %lf, -%lf>, < %lf, %lf, %lf> pigment { checker color Grey color White scale 0.4 } }\n", tenSX, SRmed, tenSX, tenSX, SRmed, tenSX );

        //~ fprintf(fpos, "plane { < 0., 1.000e-5, 0.> 0 pigment { checker color Grey");
        //~ fprintf(fpos, " color White scale 0.4 }} \n"  );

        // wall 2
        if(NW>=2){
            fprintf(fpos, "box { < 0, 0, 0>, < 0, %lf, %lf> texture { pigment { color MidnightBlue transmit 0.7 } }  no_shadow }\n", 1.2*SX, 5.*SX);
            // wall 3
            if(NW>=3){
                fprintf(fpos, "box { < %lf, 0, 0>, < %lf, %lf, %lf> texture { pigment { color MidnightBlue transmit 0.7 } } no_shadow }\n", SX, SX, 1.2*SX, 5.*SX);
                // wall 4
                if(NW>=4){
                    fprintf(fpos, "box { < 0, 0, 0>, < %lf, %lf, 0> texture { pigment { color MidnightBlue transmit 0.7 } } no_shadow }\n", SX, 1.2*SX);
                    // wall 5
                    if(NW==5){
                      fprintf(fpos, "box { < 0, 0, %lf>, < %lf, %lf, %lf> texture { pigment { color Blue transmit 0.7 } } no_shadow }\n", SX, SX, 1.2*SX, SX);
                    };
                };
            };
        };

        for(i=0;i<ND;i++){ // loop over grains set
            vitesse = coeff*log(1+(sqrt(  pow(u_n[3*i],2) +
                                          pow(u_n[3*i+1],2) +
                                          pow(u_n[3*i+2],2) )/(sqrt(9.81e3*5.*SR[i]))))/log(4);

            // draw sphere i
            qx = q_n[3*i];        //
            qy = q_n[3*i+1];      //
            qz = q_n[3*i+2];      // positions
            rx = q_n[3*ND+4*i];   //
            ry = q_n[3*ND+4*i+1]; //
            rz = q_n[3*ND+4*i+2]; //
            rt = q_n[3*ND+4*i+3]; // quaternion

            fprintf(fpos, "sphere { <0., 0., 0.>, %.15e", SR[i] );
            fprintf(fpos, " texture { pigment { checker");
            if(qz>=GLCRI){
                fprintf(fpos, " color rgb<0.,0.,0.> color rgb<1.,%.15e,0.> scale %.15e } }", 1.-vitesse,SR[i]);
                fprintf(fpos, " matrix QToMatrix( <%.15e, %.15e, %.15e, %.15e> )",rx,rz,ry, -rt);
            }else{
                fprintf(fpos, " color rgb<0.,0.,0.> color MidnightBlue scale %.15e } }", SR[i]);
            };
            fprintf(fpos, " finish { phong 1 }");
            fprintf(fpos, " translate <%.15e, %.15e, %.15e> no_shadow }\n", qx, qz, qy );
        };
    };

    fclose(fpos); // file closure

    return 0;
};










