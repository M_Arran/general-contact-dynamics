"""
file name: normal.py
language: Python3.6
date: 24/05/2018
author: Hugo Martin
email: martin.hugo@live.com
This module python contains the functions used to solve the frictionless problem.
"""

# imports
from gcd.get_F_DBT import get_DB_Npro
from numpy import zeros, sum as npsum, sqrt as npsqrt, concatenate, float64, linspace
from numpy import asarray, sort, max as npmax, abs as npabs, nonzero
from scipy.sparse import block_diag, coo_matrix, hstack, eye, dia_matrix
from sys import stdout
import mosek

def n_pro( Prm, Stn, rcwn):
    """
    The function n_pro solves the normal problem and return a contact network Cind for the
    tangential problem.

    Returns:
    Cind
    """

    Nbzn = 0
    # get new contacts and update Dzvec and Bzmat
    Cind, Bzmat, Dzvec, wtd = get_DB_Npro(  Stn.NBGl, Stn.GLUED,  \
                                            Stn.q_n, Stn.u_n,      \
                                            Stn.WH, Prm.ND, Prm.NW, \
                                            Prm.DT, Prm.SR,          \
                                            Prm.SX, Prm.SY            \
                                            Prm.OCRI                   )

    wtd = wtd>0 # convert into a boolean
    if wtd: # if there is new contacts, we must work
        print("\n> Normal problem solvation")
        Np = Dzvec.size
        print("> Nbz for Npro:",Np)
        Az, bz = make_Npro( Bzmat,     \
                            Dzvec,      \
                            Np,          \
                            Stn.u_n,      \
                            Prm.ND,        \
                            Prm.DT, Prm.DM  ) # make the min problem
                                              # objects

        lambdaz = solve_Npro(Az, bz, Np, Prm.ND, Prm.INFO) # solve minimization problem

        normlam = lambdaz/(npmax(lambdaz)) # normalize

        if not rcwn:
            Cind = Cind[(normlam <= 0.9)*(normlam >= 1.E-3)] # All contact forces
                                                             # which are below the
        else:
            Cind = Cind[(normlam <= 0.9)*(normlam >= 1.E-2)]
        Nbzn = Cind.shape[0]

    return Cind, wtd, Nbzn

# make_Npro
def make_Npro( Bzmat, Dzvec, Np, Uvec, ND, DT, DM ):
    """
    The function make_Npro makes the objects necessary to create a minimization problem,
    devoted to solve the frictionless problem.
    See below for their definitions.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      Np : int = number of effective contacts. It is also the size of indi and indj,
     Uvec : float array = non optimized velocity configuration,
       ND :   int = number of discs,
       DT : float = time step value,
       DM : float sparse matrix = mass matrix,

    Returns:
       Az : sparse float matrix = the matrix involved in the linear constraint,
       cz : float array = the vector involved in the linear objective function.

    Definitions:
    The linear coeeficiont in objectivr functional is cz.
    cz := ( 1 0 0...0 [(Dzvec - DT B *Uvec)] )  -> 2*ND+2+Nbz variables

    The linear constraint is
            Az*X  = (1 0...0) -> 2*ND+1 constraints (vector is directly given in mosek fun)

                1  1  <- 2 ND -> <--------Nbz --------->
              _                                         _
             |                                            |  ^
             |  0  1  0.................................0 |  | 1
             |  :  :  -1 0.......0                        |  V
             |  :  :  0 -1 0     :                        |  ^
        Az = |  :  :  : 0  .     :    DT* M^{-1/2} B^t    |  |
             |  :  :  :      .   :                        |  | 2 ND
             |  :  :  :        . 0                        |  |
             |  0  0  0.......0 -1                        |  v
             |_                                          _|

    """

    tmp = (DT/npsqrt(DM.data[:2*ND]))
    cz = concatenate(                                             \
            (   zeros(2*ND+2),                                     \
                npsum([Dzvec,-DT*(Bzmat.dot(Uvec[:2*ND]))],axis=0)  \
            ),axis=0                                                 )
    cz[0] = 1.

    Az = block_diag((                                                   \
            coo_matrix([0.,1.]),                                         \
            hstack(                                                       \
                [ -eye(2*ND),                                              \
                  dia_matrix((tmp, [0]),shape=(2*ND,2*ND),dtype=float64)*   \
                  (Bzmat.transpose())                                        \
                ],format="coo")                                               \
        ),format="coo"                                                         )

    return Az, cz

# solve_Npro
def solve_Npro(Az, cz, Nbz, ND, INFO):
    """
    The function solve_Npro returns the solution of the minimization problem which is
    given by frictionless model.

    Arguments:
      Az : sparse float matrix = the matrix involved in the linear constraint, see make_obj
                                function,
      cz : float array = the vector involved in the linear objective function,
     Nbz : int = number of effective contacts,
    INFO : boolean = True whether we want to plot information when solving min problem.

    Retunrs:
    lambdaz : float array = solution of minimization problem.

    Problem description:
    The minimization problem is defined as:

                minimize  cz . (t s y lambdaz)
                s.t.
                        Az *(t s y lambdaz) = (1 0...0 )
                        lambdaz >= 0
                        (t,s,y) \in \Quad_r{ 2*ND+2 } <- rotational quadratic cone
    """

    inf = 0.0 # a value for the optimizer
    numvar = Nbz+2*ND+2 # number of variables
    numcon = 2*ND+1     # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task

            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )
            # increase precision
            task.putdouparam( mosek.dparam.intpnt_co_tol_pfeas, 1.E-10 )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( Az.row, Az.col, Az.data ) # put Az to task
            del Az

            """ 3/ append cones """
            indices = (linspace(0, 2*ND+1, 2*ND+2).astype(int)).tolist()
            task.appendcone(mosek.conetype.rquad, 0.0, indices ) # append the cone

            """ 4/ append bounds for variables and constraint """
            # type of bounds
            bkc = [ mosek.boundkey.fx ]*numcon
            bkx = [ mosek.boundkey.fr ]*(2*ND+2) + [ mosek.boundkey.lo ]*Nbz
            # bounds for variables
            task.putboundlist( mosek.accmode.var,                               \
                               (linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, [inf]*numvar , [inf]*numvar                   )
            del bkx
            # bounds for linear constraints
            bz = zeros(numcon)
            bz[0] = 1.
            task.putboundlist( mosek.accmode.con,                                \
                               (linspace(0,numcon-1,numcon,dtype=int)).tolist(),  \
                               bkc, bz, bz                                         )
            del bz, bkc

            """ 5/ input linear objective """
            task.putclist((linspace(0,numvar-1,numvar,dtype=int)).tolist(), cz.tolist())
            del cz

            """ 6/ solvation """
            task.optimize() # optimize

            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary
            xx = [0.]*numvar                 #
            task.getxx(mosek.soltype.itr,xx) # get the solution value

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                lambdaz = xx[2*ND+2:]
            else:
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                exit()

    return asarray(lambdaz, float64)


# streamprinter
def streamprinter(text):
    '''
    It Defines a stream printer to grab output from MOSEK.
    '''
    stdout.write(text)
    stdout.flush()













