"""
file name: classes.py
language: Python3.6
date: 26/09/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains the classes used by GCD.
"""

"""
IMPORTS
"""
import numpy as np
from sys import exit
from scipy.sparse import csr_matrix, dia_matrix, isspmatrix_csr
from math import pi, sin, cos

# types
real_t = np.float64
uint_t = np.uint32
bool_t = np.bool_

# Pb_Params
class Parameters:
    """
    The members with a > are supposed to be hidden for the user.
    Members:
       SD : uint_t = space dimension,
       NS : uint_t = number of grains,
       NW : uint_t = number of walls,
       SX : real_t = side box length,
       SY : real_t = side box width,
    SLOPE : real_t = slope between bottom and horizontal axis,
       WV : real_t = right wall velocity,
       DT > real_t = time step value,
    DTref : real_t = reference time step value,
       FT : real_t = final time,
      RHO : real_t = grains density (the same for all grains),
       EN : real_t = normal restitution coefficient,

      SRf : real_t = spheres radii value if they are considered as monodisperse,
       SR : real_t numpy array             = radii of spheres,
       SM > real_t scipy.sparse dia_matrix = system mass matrix (dia format),
      iSM > real_t scipy.sparse dia_matrix = system inverse mass matrix (dia format),
   sqrtSM > real_t scipy.sparse dia_matrix = system square root of mass matrix (dia format),

       MU : real_t = Coulomb's law coefficient of friction,
      EPS : real_t = epsilon parameter in penalized model,
       Gc : real_t = gravity constant,
    GLCRI : real_t = glued criteria (q_z^i < GLCRI ==> grain number i is glued),
     OCRI : real_t = overlap criteria,
    OSTEP : uint_t = step between two outputs,
     INFO :   bool = True whether we want minimization problem information, False otherwise,
    VARDT :   bool = True whether we consider a variable time step,
     NPRO >   bool = True whether only the normal problem is gonna be used.
    """
    # init
    def __init__(   self, name, sd, ns, nw, sx, sy,    \
                    slope, wv, dt, ft, rho, en, sr,   \
                    mu, eps, gc, glcri, ocri,          \
                    ostep, info, vardt                 ):
        """
        GCD_Params constructor.
        """
        self.NAME  = name
        self.SD    = uint_t(sd)
        self.NS    = uint_t(ns)
        self.NW    = uint_t(nw)
        self.SX    = real_t(sx)
        self.SY    = real_t(sy)
        self.SLOPE = real_t(slope)
        self.WV    = real_t(wv)
        self.DT    = real_t(dt)
        self.DTref = self.DT
        self.FT    = real_t(ft)
        self.RHO   = real_t(rho)
        self.EN    = real_t(en)

        # build SR and SM arrays
        if (isinstance(sr, list)):
            self.SR = sr.astype(real_t)
        else:
            self.SR = sr*np.ones(self.NS,dtype=real_t)
        self.SRf = np.mean(self.SR)
        # set mass matrix
        self.set_mass_matrix() #     SM, iSM, sqrtSM

        self.MU    = real_t(mu)
        self.EPS   = real_t(eps)
        self.Gc    = real_t(gc)
        self.GLCRI = real_t(glcri)
        self.OCRI  = real_t(ocri)
        self.OSTEP = uint_t(ostep)
        self.INFO  = bool(info)
        self.VARDT = bool(vardt)
        self.NPRO  = bool(self.MU==0.)
    # methods
    # check
    def check( self ):
        """
        The method check do a type check of all the parameters values.
        """
        if type(self.SD)!=uint_t:
            raise TypeError("Wrong type.")
        if type(self.NS)!=uint_t:
            raise TypeError("Wrong type.")
        if type(self.NW)!=uint_t:
            raise TypeError("Wrong type.")
        if type(self.SX)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.SY)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.SLOPE)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.WV)!=real_t:
            raise TypeError("Wrong type.")
        if (self.WV<0.):
            print("\n The wall velocity can't be negative.")
            exit()
        if type(self.DT)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.DTref)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.FT)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.RHO)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.EN)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.SRf)!=real_t:
            raise TypeError("Wrong type.")
        if (self.SR.dtype!=real_t):
            raise TypeError("Wrong type.")
        if type(self.MU)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.EPS)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.Gc)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.GLCRI)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.OCRI)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.OSTEP)!=uint_t:
            raise TypeError("Wrong type.")
        if type(self.INFO)!=bool:
            raise TypeError("Wrong type.")
        if type(self.VARDT)!=bool:
            raise TypeError("Wrong type.")
        if ((self.SD<2)or(self.SD>3)):
            print("\n> Space dimension SD must be equal to 2 or 3.")
            exit()
        if ((self.SD==2)and((self.NW<1)or(self.NW>3))):
            print("\n> Number of walls must between 1 and 3 in 2D")
            exit()
        if ((self.SD==3)and((self.NW<1)or(self.NW>5))):
            print("\n> Number of walls must between 1 and 5 in 3D")
            exit()
        if(self.OCRI>self.DT):
            print("\n> OCRI cannot be greater than DT.")
            exit()
        if(self.DT > self.FT):
            print("\n> DT cannot be greater than FT.")
            exit()
        if(np.nonzero( self.SR )[0].size < self.NS):
            print("\n> At least one sphere radius is zero.")
            exit()

        # set mass matrix
        self.set_mass_matrix() #     SM, iSM, sqrtSM

        if ((((self.NW!=3)and(self.SD==2))or((self.NW!=5)and(self.SD==3)))and(self.WV!=0.)):
            print("\n\n> Warning, wall velocity is nonzero but number of walls is below 3.")
            print("    Are you sure to continue?")
            print("    Enter: y to continue,")
            print("           n to stop.")
            REP=input("> ")
            if ((REP!="y")and(REP!="n")):
                print("> Wrong answer value.")
                exit()
            elif REP=="n":
                exit()

        print("\n> Class Parameters has been checked.")
    # print_prm
    def print_prm( self ):
        """
        Print in terminal the Parameters class members value.
        """
        print("\nParameters:")
        print("SD",self.SD)
        print("NS",self.NS)
        print("NW",self.NW)
        print("SX",self.SX)
        print("SY",self.SY)
        print("SLOPE",self.SLOPE)
        print("WV",self.WV)
        print("DT",self.DTref)
        print("FT",self.FT)
        print("RHO",self.RHO)
        print("EN",self.EN)
        print("SRf",self.SRf)
        print("MU",self.MU)
        print("EPS",self.EPS)
        print("Gc",self.Gc)
        print("GLCRI",self.GLCRI)
        print("OCRI",self.OCRI)
        print("OSTEP",self.OSTEP)
        print("INFO",self.INFO)
        print("VARDT",self.VARDT)
        print(" ")
    # set_mass_matrix
    def set_mass_matrix( self ):
        """
        The method set_mass_matrix updates the mass matrix SM in the class Parameters in
        function of its arguments SR and RHO.

        Description:
        SD = 2 ==> M = (pi R^2)*RHO,    I = 1/2 M R^2

        SD = 3 ==> M = (4/3 pi R^3)*RHO,    I = 2/5 M R^2
        """
        if (self.SD==2):
            cstm = real_t(1.)
            csti = real_t(0.5)
            tnb  = 2
            rnb  = 1
        elif (self.SD==3):
            cstm = real_t(4./3)
            csti = real_t(0.4)
            tnb  = 3
            rnb  = 3
        else:
            print("\> Space dimension value problem. Must be 2 or 3.")
            exit()

        totnb = (tnb + rnb)*self.NS

        mass = (cstm*(pi*self.RHO)*(self.SR)**(self.SD)).astype(dtype=real_t)

        # mass matrix
        self.SM = dia_matrix(( np.hstack(( np.repeat( mass, tnb ),                      \
                                           np.repeat( csti*mass*(self.SR**2), rnb ) )), \
                            [0]), shape=( totnb , totnb ),dtype=real_t                  )
        self.SM.astype(real_t)

        # inverse of mass matrix
        self.iSM = self.SM.copy()
        self.iSM.data = 1./self.iSM.data
        self.iSM.astype(real_t)

        # square root of tangential mass matrix
        self.sqrtSM = dia_matrix((np.sqrt(np.repeat( mass, tnb )),[0]), shape=(tnb*self.NS, tnb*self.NS), dtype=real_t)
        self.sqrtSM.astype(real_t)

    # save_rad
    def save_rad( self ):
        """
        The method save_rad save the Parameters member SR in a txtfile.
        """
        np.savetxt("./"+self.NAME+"/dat_files/radii.dat",self.SR,fmt="%.15e")
        print("\n> radii are saved")

# State
class State:
    """
    The members with a > are supposed to be hidden for the user.
    Members:
    CTIME : real_t = current time value,
     ITER : uint_t = number of current iteration,
     FNUM : uint_t = number of current data file,
       WH : real_t = right wall height,
      q_n : real_t numpy array = positions of current time configuration,
      u_n : real_t numpt array = velocities of current time configuration,
    GLUED : bool_t numpy array = GLUED[i] = True whether grain i is glued, False otherwise,
     NBGl : bool_t scipy.sparse csr_matrix =
                            it is a matrix which contains the neighbourgs of each
                            grains in CSR compressed format. Then the neighbourgs of the
                            grain i are referenced in the array
                            NGBl.indices[NGBl.indptd[i]:NGBl.indptd[i+1]], see scipy.sparse
                            manual for more informations.
    """
    # init
    def __init__( self, tin, iin, fin, wh, SD, NS, q, u ):
        """
        State constructor.
        """
        if SD==3:
            qnb = 7
        elif SD==2:
            qnb = 3
        else:
            print("\n> SD must be 2 or 3.")
            exit()
        self.SD = SD
        self.NS = NS

        qsize = qnb*NS
        usize = 3*(SD-1)*NS

        if(qsize != q.size):
            raise TypeError("Mismatch between NS and q_n array size.")
        if(usize != u.size):
            raise TypeError("Mismatch between NS and u_n array size.")
        self.CTIME = real_t(tin)
        self.ITER  = uint_t(iin)
        self.FNUM  = uint_t(fin)
        self.WH    = real_t(wh)
        self.q_n   = q.astype(dtype=real_t)
        self.U_n   = u.astype(dtype=real_t)
        self.u_o   = self.U_n.copy()
        self.NBGl  = csr_matrix((0,0),dtype=bool_t)
        self.Nbz   = uint_t(0)
        self.PLOT  = True
        self.OLDT  = self.CTIME
        self.GLUED = np.zeros( self.NS, dtype=bool_t)
        self.Uvec  = np.empty(usize, dtype=real_t)
        self.E_o   = np.zeros(3)
        self.E_n   = np.zeros(3)
    """
    METHODS
    """
    # ini_Uu
    def ini_Uu( self, DT, Gc, SLOPE ):
        """
        The function ini_Uu initializes the free fall velocity Uvec with rebounds effects
        and set optimized velocity u_o to Uvec.

        Arguments:
           DT : float = time step,
           Gc : float = gravity constant value,
        SLOPE : float = slope of bottom.
        """
        dtgc = DT*Gc

        if self.SD==2:                                                     #
            self.Uvec[0:2*self.NS-1:2] = real_t(dtgc*sin(SLOPE))  # x axis #
            self.Uvec[1:2*self.NS  :2] = real_t(-dtgc*cos(SLOPE)) # z axis #
        else:                                                              # Ext
            self.Uvec[0:3*self.NS-2:3] = real_t(dtgc*sin(SLOPE))  # x axis # forces
            self.Uvec[1:3*self.NS-1:3] = real_t(0.)               # y axis # 
            self.Uvec[2:3*self.NS  :3] = real_t(-dtgc*cos(SLOPE)) # z axis #
        self.Uvec[self.SD*self.NS:] = real_t(0.) # for rotations           #

        np.add(self.U_n, self.Uvec, out=self.Uvec, dtype=real_t) # Uvec = u_n + dt M-1 Fext

        # Uvec[ glued ] = 0.
        tmp = np.hstack((   np.repeat(self.GLUED,self.SD),     \
                            np.repeat(self.GLUED,2*self.SD-3)) )
        self.Uvec[tmp] = real_t(0.)

        # initialization of optimized velocity
        # as non optimized velocity
        self.u_o = self.Uvec.copy()

    # set_en
    def set_en( self, option, SM, Gc, SLOPE, Hgts ):
        """
        This method computes the energy with positions q_n and optimized velocity u_o
        """

        if option:
            vu = self.u_o
            vE = self.E_o
        else:
            vu = self.U_n
            vE = self.E_n

        # kinetic energy
        vE[2] = real_t(0.5*np.asscalar(np.dot(SM.dot(vu),vu)))

        # get vertical displacement
        if (SLOPE>0.):
            np.multiply(    sin(SLOPE),                                \
                            self.q_n[0:self.SD*(self.NS-1)+1:self.SD], \
                                     out=Hgts, dtype=real_t            )
            np.subtract( 1.E4, Hgts, out=Hgts, dtype=real_t )
        else:
            np.copyto(Hgts, self.q_n[(self.SD-1):self.SD*self.NS:self.SD])

        # potential energy
        vE[1] = real_t(Gc*np.asscalar(                            \
        np.dot( SM.data[0,(self.SD-1):self.SD*self.NS:self.SD], Hgts )) )

        # total energy
        vE[0] = real_t(vE[1]+vE[2])



    # print_Stn
    def print_stn( self ):
        """
        Print in terminal the State class members value.
        """
        print("\nState:")
        print("CTIME",self.CTIME)
        print("ITER",self.ITER)
        print("FNUM",self.FNUM)
        print("q_n")
        print(self.q_n)
        print("u_n")
        print(self.u_n)
        print("WH", self.WH)
        print("GLUED")
        print(self.GLUED)
        print("NBGl")
        print((self.NBGl).toarray())
        print(" ")
    # check
    def check( self ):
        """
        The method check do a type check of all the parameters values.
        """
        if self.SD==3:
            qnb = 7
        elif self.SD==2:
            qnb = 3
        else:
            print("\n> SD must be 2 or 3.")
            exit()

        qsize = qnb*self.NS
        usize = 3*(self.SD-1)*self.NS

        if(qsize != self.q_n.size):
            raise TypeError("Mismatch between NS and q_n array size.")
        if(usize != self.U_n.size):
            raise TypeError("Mismatch between NS and u_n array size.")
        if(self.q_n.dtype != real_t):
            raise TypeError("Wrong type of q_n array elements.")
        if(self.U_n.dtype != real_t):
            raise TypeError("Wrong type of u_n array elements.")
        if(self.NBGl.dtype != bool_t):
            raise TypeError("Wrong type of NBGl matrix elements.")
        if(not (isspmatrix_csr(self.NBGl))):
            raise TypeError("Wrong type of compressed storage of NBGl matrix. CSR requiered.")
        if (self.NBGl.data).size > 0:
            if ((npmax(self.NBGl.indices) > NS)or(npmin(self.NBGl.indices)<0)):
                raise TypeError("NBGl matrix doest not correspond to a contact network of",\
                            NS,"grains.")
        if type(self.CTIME)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.ITER)!=uint_t:
            raise TypeError("Wrong type.")
        if type(self.WH)!=real_t:
            raise TypeError("Wrong type.")
        if type(self.FNUM)!=uint_t:
            raise TypeError("Wrong type.")
        print("\n> Class State has been checked.")
    # integrate
    def integrate( self, SX, SY, DT, WV, buffv3ND ):
        """
        Integration of position vector q_n with velocity u_o.
        """
        NSx2 = 2*self.NS
        NSx3 = 3*self.NS
        np.multiply(DT, self.u_o[:NSx3], out=buffv3ND[:NSx3], dtype=real_t)
        np.add(self.q_n[:NSx3], buffv3ND[:NSx3], out=self.q_n[:NSx3], dtype=real_t)
        if(self.SD==3):
            np.remainder(self.q_n[:NSx3], np.tile(np.array([SX, SY, np.inf]), self.NS), out=self.q_n[:NSx3], dtype=real_t)
            for i in range(self.NS):
                rqqi = quaterint( DT,                                       \
                                  self.q_n[3*self.NS+4*i:3*self.NS+4*i+4],  \
                                  self.u_o[3*self.NS+3*i:3*self.NS+3*i+3]   )
                self.q_n[3*self.NS+4*i:3*self.NS+4*i+4] = rqqi.copy()
        else:
            np.remainder(self.q_n[:NSx2], np.tile(np.array([SX, np.inf]), self.NS), out=self.q_n[:NSx2], dtype=real_t)

        self.WH = self.WH + DT*WV
                
                # glued
    def glued( self, GLCRI ):
        """
        The method glued updates which spheres are glued, according to height criterion GLCRI
        """
        # update glued beads array self.GLUED
        id_glue = (self.q_n[self.SD - 1 : self.SD * self.NS: self.SD] <= GLCRI)
        self.GLUED[id_glue] = True
        
    # incrementation
    def incrementation( self, DT, DTref, OSTEP ):
        """
        Time incrementation. This function takes into account the variable time step.
        """
        nextplot   = self.OLDT + OSTEP*DTref
        self.CTIME = self.CTIME + DT #
        self.ITER  = self.ITER  + 1  # update current time and iter

        if (self.CTIME >= nextplot):
            self.PLOT = True
            self.OLDT = nextplot
        else:
            self.PLOT = False

    def copy( self ):
        """
        Return a new State built as a copy of self.
        """
        SD = self.SD
        NS = self.NS
        Stnm = State(   self.CTIME, self.ITER, self.FNUM, self.WH, SD,            \
                        NS, self.q_n.copy(), self.u_n.copy(), self.GLUED.copy(),  \
                                                                                  )
        Stnm.NBGl = self.NBGl.copy()
        Stnm.uel  = self.uel.copy()

        return Stnm





"""
A ECRIRE EN C !!!
"""
# quaterint
def quaterint( DT, rqi, uwi):
    q_uwi = 0.5*DT*np.asarray([ uwi[0], uwi[1], uwi[2], 0. ])
    norm  = np.linalg.norm(q_uwi)

    if norm>0.:
        expi    = (1./norm)*(q_uwi)*sin(norm)
        expi[3] = cos(norm)

    else:
        expi  = np.asarray([ 0., 0., 0., 1. ])
    rqqi  = q_mult(expi, rqi)

    return rqqi

# q_mult
def q_mult(qa, qb):
    newq = np.asarray( [ qa[3]*qb[0] + qa[0]*qb[3] + qa[1]*qb[2] - qa[2]*qb[1],   \
                         qa[3]*qb[1] + qa[1]*qb[3] + qa[2]*qb[0] - qa[0]*qb[2],   \
                         qa[3]*qb[2] + qa[2]*qb[3] + qa[0]*qb[1] - qa[1]*qb[0],   \
                         qa[3]*qb[3] - qa[0]*qb[0] - qa[1]*qb[1] - qa[2]*qb[2]  ] )

    return newq









# Buff
class Buffer:
    """
    vND
    v2ND
    v3ND
    Uvec
    """
    # init
    def __init__( self, SD, NS, SM ):
        """
        Buff class constructor.
        """
        usize = NS*3*(SD-1)

        self.vND1 = np.empty(NS, dtype=real_t)
        self.vND2 = np.empty(NS, dtype=real_t)

        tmp = np.empty(SD*NS, dtype=real_t)
        np.reciprocal( np.sqrt( SM.data[0,:SD*NS], dtype=real_t), out=tmp, dtype=real_t)
        self.DM2 = dia_matrix((tmp, [0]), shape=(SD*NS,SD*NS), dtype=real_t)

        self.v3ND = np.empty(usize, dtype=real_t)

# FM_obj
class FM_obj:
    """
    Cind
    Bzmat
    Tzmat
    Dz
    first
    Nbz
    wtd
    Iloop
    Nbzn
    MiAzt
    lambdaz
    """
    # init
    def __init__( self, DS, NS ):
        """
        FM_obj class constructor.
        """

        usize = 3*(DS-1)*NS

        self.Cind  = np.empty((0,2), dtype=uint_t)
        self.Bzmat = csr_matrix((0,usize), dtype=real_t) #
        self.Tzmat = csr_matrix((0,usize), dtype=real_t) #
        self.Dzvec = np.zeros(0, dtype=real_t) # initializations

        self.lambdaz = np.zeros(0, dtype=real_t)
        self.MiAzt   = np.zeros((0,0), dtype=real_t)

        self.Iloop  = 0     # loop iteration number
        self.wtd    = True
        self.Nbz    = 0

        self.Az = np.zeros((0,0), dtype=real_t)
        self.bz = np.zeros((0,0), dtype=real_t)









