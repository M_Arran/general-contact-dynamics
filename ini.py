"""
file name: ini.py
language: Python3.6
date: 17/04/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains functions to initializes positions aNS velocities.
"""

# imports
from gcd.classes import State
from gcd.dat_t import save_quf
import numpy as np
from random import random

# ini_box
def ini_box( Prm ):
    """
    The function ini_box creates a box of size SX by SY in which there are all beads
    disposed in a mesh with small perturbations on each disc.

    Arguments:
    Prm : python class = gcd.Parameters class.
    """

    SD    = Prm.SD
    NS    = Prm.NS
    SX    = Prm.SX
    SY    = Prm.SY
    GLCRI = Prm.GLCRI
    SR    = Prm.SR

    print("\n> Radii:")
    print("> min SR =", np.min(Prm.SR))
    print("> max SR =", np.max(Prm.SR))

    if SD==2: # If space dimension
              # is 2. The box is composed
              # of 3 walls.

        q_n = np.zeros(3*NS, dtype=np.float64) #
        u_n = q_n.copy()                       # allocate memory

        xmin=1.E6  #
        ymin=1.E6  #
        xmax=-1.E6 #
        ymax=-1.E6 # initial values of ymin, xmin, ymax aNS xmax

        SRM = np.amax(SR)
        step = 2.*SRM + SRM/6. # distance between two grains
        posx_0 = 1.5*SRM         #
        posz_0 = 1.1*SRM         # position of the current grain
        h = 0 # number of vertical lines
        posx = posx_0 - step
        posz = posz_0 + step

        j=0
        glcri = -1.E6
        posxg = SR[j]+0.01
        while( (posxg+SR[j]) < SX ):
            q_n[2*j]   = posxg
            q_n[2*j+1] = SR[j]
            glcri = max( glcri, SR[j]+0.001 )
            posxg = posxg+SR[j]+1.5*SR[j+1]
            j=j+1

        for i in range(j,NS): # loop over the set of grains
            posx = posx + step # change x position

            if( (posx+SRM) > (SX-0.01)): #
                posz = posz + step  #
                posx = posx_0       # if we are at the eNS of the horizontal line then we
                                    # start a new line

            q_n[2*i] = posx + 0.02*SR[i]*random()*pow(-1,i)
            q_n[2*i+1] = posz + 0.02*SR[i]*random()*pow(-1,i) # initialization with a small
                                                           # perturbation of 2% SR

            # min max etc.
            if q_n[2*i]<xmin:   #
                xmin=q_n[2*i]   #
            if q_n[2*i]>xmax:   #
                xmax=q_n[2*i]   #
            if q_n[2*i+1]<ymin: #
                ymin=q_n[2*i+1] #
            if q_n[2*i+1]>ymax: #
                ymax=q_n[2*i+1] # update min, max values

        GLUED = q_n[1:2*NS:2] < glcri # create glued beads

    else: # If the space dimension
          # is 3. We must build a
          # box composed of 5 walls.

        q_n = np.zeros(7*NS, dtype=np.float64) #
        u_n = np.zeros(6*NS, dtype=np.float64) # allocate memory

        for i in range(NS):      #
            q_n[3*NS+4*i+3] = 1. # quaternion initialization

        xmin = 1.E6  #
        ymin = 1.E6  #
        xmax = -1.E6 #
        ymax = -1.E6 # initial values of ymin, xmin, ymax aNS xmax

        SRM = np.amax(SR)
        step = 2.*SRM + SRM/6. # distance between two grains
        posx_0 = 1.5*SRM
        posz_0 = 1.1*SRM
        posy_0 = 1.5*SRM
        h = 0 # the number of vertical lines

        j=0                                           #
        glcri = -1.E6                                 #

        posx = posx_0
        while((j<NS)and((posx+SRM)<(SX-0.01))): #
            posy = posy_0
            while((j<NS)and((posy+SRM)<(SY-0.01))):
                q_n[3*j]   = posx + 0.02*SR[j]*random()*pow(-1,j)
                q_n[3*j+1] = posy + 0.02*SR[j]*random()*pow(-1,j)
                q_n[3*j+2] = SR[j]
                glcri = max( glcri, SR[j]+0.001 )
                j = j+1
                posy = posy + step
            posx = posx + step # change x position

        posx = posx_0
        posy = posy_0
        posz = 3.01*SRM
        i = j

        while(i<NS): # loop over the set of grains

            if((posx+SRM)>(SX-0.01)): #
                posx = posx_0         # if we are at the eNS of the horizontal line then we
                posz = posz + step         # start a new line

            while((i<NS)and((posy+SRM)<(SY-0.01))):
                q_n[3*i]   = posx + 0.02*SR[i]*random()*pow(-1,i)
                q_n[3*i+1] = posy + 0.02*SR[i]*random()*pow(-1,i) # initialization with a small
                                                           # perturbation of 2% SR
                q_n[3*i+2] = posz + 0.02*SR[i]*random()*pow(-1,i)
                i = i+1
                posy = posy + step
            posy = posy_0
            posx = posx + step # change x position

            # min max etc.
            if q_n[2*i]<xmin:   #
                xmin=q_n[2*i]   #
            if q_n[2*i]>xmax:   #
                xmax=q_n[2*i]   #
            if q_n[2*i+1]<ymin: #
                ymin=q_n[2*i+1] #
            if q_n[2*i+1]>ymax: #
                ymax=q_n[2*i+1] # update min, max values

        GLUED = q_n[2:3*NS:3] < glcri # create glued beads

    print("\n> Box caracteritics:")
    #~ print(  "> min x_i =",xmin, "    max x_i =",xmax ,"\n> min y_i =",  \
            #~ ymin, "    max y_i =", ymax) # print

    ini_stn = State( 0., 0, 0, 0., SD, NS, q_n, u_n, GLUED ) # initialize an initial State

    save_quf( SD, q_n, u_n, u_n, 0) # save quf file

    Prm.GLCRI = glcri
    print("\nNew GLCRI = %f" % Prm.GLCRI)

    return ini_stn










