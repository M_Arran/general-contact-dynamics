"""
file name: disp.py
language: Python3.6
date: 29/05/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains functions to make simple displays.
"""

# imports
from gcd.get_F_DBT import clib, cuint, cdouble, v_double
from gcd.dat_t import load_quf, test_dir, load_rad

import matplotlib.pyplot as plt
from matplotlib.cm import ScalarMappable
from numpy import savetxt, dot, loadtxt, linspace, sqrt as npsqrt, sum as npsum, asarray
from numpy import max as npmax, mean as npmean
from math import cos, sin, sqrt
import matplotlib.patches as mpatches
from sys import exit
import os

# pngfiles
def make_png( Startf, Endf, step, params, plot_pam, Sname ):
    """
    Loop over a set of GCD_*.dat files and create GCD_*.png files.

    Arguments:
      Startf : int = the first file number,
        Endf : int = the last file number,
        step : int = step for plots,
      params : dict = the parameters of the GCD problem we want to plot,
    plot_pam : dict = plot parameters, it depends of the function used to makes png files,
                      for a number of discs below 500, see mpl_png function,
                      otherwise, see pov_png function.
    """
    print("\n> Make png:")

    if plot_pam["rep"]==0:
        test_dir("png", Sname) # test png_files directory

    SD = params.SD                          #
    NS = params.NS                          #
    NW = params.NW                          #
    SX = params.SX                          #
    SY = params.SY                          #
    SR = load_rad( Sname )                  #
    if SR.size!=NS:                         #
        print("radii file incompatible")    #
        exit()                              #
    elif SR.size==1:                        #
        SR = asarray([SR])                  #
    params.SR = SR                          #
    params.set_mass_matrix()                # update the mass matrix
    SM = params.SM                          #
    MU = params.MU                          #
    Gc = params.Gc                          #
    SLOPE = params.SLOPE                    # get parameters values
    GLCRI = params.GLCRI

    DT = params.DT*params.OSTEP

    WV = params.WV             # wall velocity
    WH = Startf*WV*DT

    if((NS<50)and(SD==2)):#
        png = mpl_png #
    else:             #
        if(SD==3):
            os.system("cp ./gcd/lib/quaternions.inc ./"+Sname+"/png_files/")
        png = pov_png # choose the function to make png files
        plot_pam["SD"] = params.SD

    totz = len(str(Endf)) # for adding the right number of zeros
    nb = Endf-Startf+1 #
    ostep = nb//10 + 1 # for display in command line
    print(" ")

    plot_pam["FNUM"] = 0  # add new item (file number) to plot_param dict
    plot_pam["WH"] = WH
    plot_pam["GLCRI"] = GLCRI

    for i in range(Startf, Endf+1, step): # loop over set of file
        plot_pam["FNUM"] = i    # save the file number
        leni = len(str(i))      #
        add = "0"*(totz - leni) # for adding the right number of zeros

        namefile = "./"+Sname+"/png_files/GCD_" +add+str(i)+".png"
        NDD, q_n, u_n, cf_n = load_quf(i, SD, Sname) # get GCD data

        if NDD!=NS:
            print("/> Number of discs is different from file:",namefilebis)
            print(NDD,"==",NS)
            exit()

        png( plot_pam, q_n, u_n, cf_n, namefile, \
             NS, NW, SR, SM, SX, SY, Gc, SLOPE, Sname     ) # create png file

        plot_pam["WH"] = plot_pam["WH"] + DT*WV

        if((i%ostep)==0):            #
            print("\tFile",namefile) # display in command line

    print("\n> All png files are made.\n\n") # end

# mpl_png
def mpl_png( plot_pam, q_n, u_n, f_n, namefile, NS, NW, SR, SM, SX, SY, Gc, SLOPE, Sname ):
    """
    The function mpl_png makes the png files using matplotlib.pyplot.

    Arguments:
    plot_pam : dict = contains the plot parameters, see below,
         q_n : float array = position configuration,
         u_n : float array = velocity configuration,
         f_n : float array = contact forces vector,
    namefile : str = name of png file,
          NS : int = number of discs,
          NW : int = number of walls,
          SR : float = discs radius,
          SM : float = discs mass,
          SX : float = side box length,
          SY : float = side box width,
          Gc : float = gravity constant,
       SLOPE : float = slope between bottom and horizontal axis.

    plot_pam = { "XMIN": float = left side of png file window,
                 "XMAX": float = right side of png file window,
                 "YMIN": float = upper side of png file window,
                 "YMAX": float = ground side of png file window,
                "DISPF": bool = True whether we want display contact forces, False otherwise,
               "fscale": float = coefficient for forces scaling,
                 "vmax": float = maximum of velocity scaling,
                 "vmin": float = minimum of velocity scaling.
              }
    """
    XMIN = plot_pam["XMIN"]     #
    XMAX = plot_pam["XMAX"]     #
    YMIN = plot_pam["YMIN"]     #
    YMAX = plot_pam["YMAX"]     #
    DISPF = plot_pam["DISPF"]   #
    vmax = plot_pam["vmax"]     #
    vmin = plot_pam["vmin"]     #
    fscale = plot_pam["fscale"] # get values in dictionary of plot parameters

    fig, ax = plt.subplots(1,1) # create the figure

    DMtemp = SM.data[0, linspace(0,2*NS-2,NS).astype(int) ]
    gi = fscale*(Gc*sin(SLOPE))*DMtemp   #
    gip = -fscale*(Gc*cos(SLOPE))*DMtemp # gravity force value

    # create velocity EC
    lin = linspace(0,2*NS-2,NS).astype(int)
    u_x = u_n[ lin ]    #
    DMx = SM.data[0, lin ]
    lin = linspace(1,2*NS-1,NS).astype(int)
    u_z = u_n[ lin ]    #
    DMz = SM.data[0, lin ]
    lin = linspace(2*NS,3*NS-1,NS).astype(int)
    u_t = u_n[ lin ] # get values of u_n
    DMt = SM.data[0, lin ]
    # get the kinetic energy by beads
    EC = 0.5*npsum([DMx*(u_x**2), DMz*(u_z**2), DMt*(u_t**2)],axis=0)
    EC[vmin/(EC+1.e-14) > 1.] = 0. #
    EC = EC/vmax                   #
    EC[(EC > 1.)] = 1.                # scale EC wrt vmax and vmin
    colors = ScalarMappable(cmap='coolwarm').to_rgba(EC) #
    colors = colors[:,:3]                                   # create colors

    # loop over the set of the discs
    for i in range(NS):
        qi = q_n[2*i]       #
        qip = q_n[2*i+1]    #
        theta = q_n[2*NS+i] # getting the coordinates of discs

        DRi = SR[i] # get values of radius of grain i
        x_0 = DRi*cos(theta) #
        y_0 = DRi*sin(theta) # point on the cirle

        color_i = colors[i,:] # get color of bead i

        # create a discs with a segment for rotation
        plt.gca().add_patch(mpatches.Circle((qi, qip), DRi, facecolor="white", \
                                              edgecolor=color_i,  linewidth=1)  ) #
        plt.plot([qi,qi+x_0],[qip,qip+y_0],color=color_i)                        # plot disc

        if DISPF:
            plt.arrow(qi, qip, gi[i], gip[i] , width=0.1, color='sandybrown') # plot gravity
                                                                              # force

            # contact forces
            fi = fscale*f_n[2*i]      #
            fip = fscale*f_n[2*i+1]   #
            fipp = fscale*f_n[2*NS+i] # get contact force value

            if (sqrt((pow(fi,2)+pow(fip,2))>=1.E-9)):
                plt.arrow(qi, qip, fi, fip ,width=0.1,color='mediumseagreen')  # force
                plt.arrow(qi, qip-DRi, fipp, 0. ,width=0.1, color='r')         # torque

    # create walls
    ax.plot([-2.*SX, 2.*SX],[0., 0.],'lightgray', label='Walls') # plot bottom

    if(NW>=3):
        ax.plot([SX, SX],[0., SX],'lightgray'); # plot right wall

    if(NW>=2):
        ax.plot([0., 0.],[0., SX],'lightgray'); # plot left wall

    plt.axis('equal') # to have axis equal
    ax.set_xlim([XMIN,XMAX])
    ax.set_ylim([YMIN,YMAX])

    # save and close
    plt.savefig(namefile)
    plt.close(fig)

clib.make_pov.argtypes = ( v_double, v_double,                  \
                           cdouble, cuint,                      \
                           cuint, cuint, cuint,                 \
                           cdouble, cdouble, cdouble,           \
                           cdouble, v_double, cdouble,          \
                           cdouble, cdouble,                    \
                           cdouble, cdouble, cdouble, cdouble,  \
                           cdouble, cdouble                     )

# pov_png
def pov_png( plot_pam, q_n, u_n, f_n, namefile, NS, NW, SR, SM, SX, SY, Gc, SLOPE, Sname ):
    """
    The function pov_png makes the png files using povray software.

    Arguments:
    plot_pam : dict = contains the plot parameters, see below,
         q_n : float array = position configuration,
         u_n : float array = velocity configuration,
         f_n : float array = contact forces vector,
    namefile : str = name of png file,
          NS : int = number of discs,
          NW : int = number of walls,
          SR : float array = radii of discs,
          SM : float = discs mass,
          SX : float = side box length,
          SY : float = side box width,
          Gc : float = gravity constant,
       SLOPE : float = slope between bottom and horizontal axis.

    plot_pam = { "XCAM": float = x component of the center point of window in png file,
                 "YCAM": float = z component of the center point of window in png file,
                 "COEFF": float = scale of velocity norm,
                 "DIST": float = distance from the plane (x,z),
                 "LWIDTH": bool = line width,
                 "FNUM": float = file number,
                   "WV" : right wall height,
               }
    """

    SD     = plot_pam[   "SD"   ] #
    SH     = plot_pam[   "SH"   ] #

    XCAM   = plot_pam[  "XCAM"  ] #
    ZCAM   = plot_pam[  "ZCAM"  ] #
    YCAM   = plot_pam[  "YCAM"  ] #

    if SD==3:
        XEYE   = plot_pam[  "XEYE"  ] #
        YEYE   = plot_pam[  "YEYE"  ] #
        ZEYE   = plot_pam[  "ZEYE"  ] #
    else:
        XEYE   = XCAM
        YEYE   = 0.
        ZEYE   = ZCAM

    FNUM   = plot_pam[  "FNUM"  ] #
    WH     = plot_pam[   "WH"   ] #
    COEFF  = plot_pam[  "COEFF" ] #
    LWIDTH = plot_pam[ "LWIDTH" ] #
    GLCRI  = plot_pam["GLCRI"]


    if(not ((SD==2)or(SD==3))):
        print("SD must be equal to 2 or 3.")
        exit()

    SRmed = npmean(SR)
    os.chdir("./"+Sname+"/png_files/") # go to png_file dir

    clib.make_pov( q_n, u_n,               \
                   WH, SD,                 \
                   NS, NW, FNUM,           \
                   SX, SY, SH,             \
                   GLCRI, SR, SRmed,       \
                   XCAM, YCAM,             \
                   ZCAM, XEYE, YEYE, ZEYE, \
                   COEFF,                  \
                   LWIDTH                  ) # make a GCD_*.pov file

    povname = "./GCD_"+str(FNUM)+".pov"           #
    #~ os.system("povray "+povname+" Display=off &")
    os.system("povray "+povname+" Height=900 Width=1200 Quality=11 Display=off &")

    os.chdir("../..") # go to ../ dir

# plot_rad
def plot_rad(step, name_radf, numf, SR, NW, SX, SY):
    """
    The function plot_rad plots circles given by the file GCD_numf.dat
    with the radius given in the file name_radf.

    Arguments:
    step : int = number of step between two radius plot,
    numf : int = number of GCD_*.dat file
    name_radd : string = name of the radius file,
    SR : float = real radius of beads,
    NW : int = number of walls,
    SX : float = side box length,
    SY : float = side box width.

    """
    if type(name_radf)!=str:
        raise TypeError("\n> name_radf must be a string.")
    if type(numf)!=int:
        raise TypeError("\n> numf must be an int.")

    if step==0:
        step=1

    fig, ax = plt.subplots(1,1) # create the figure

    maxDR = npmax(SR)

    XMIN = -10.*maxDR        #
    XMAX = SX+10.*maxDR      #
    YMIN = -10.*maxDR        #
    YMAX = SY+10.*maxDR   # get plot parameters

    namefilebis = "./dat_files/GCD_"+str(numf)+".dat" # GCD dat file name
    Radius = loadtxt(name_radf)
    NS, q_n, u_n, cf_n = load_quf( namefilebis )
    if NS!=Radius.size:
        print("/n> The number of discs in the two files is not the same.")
        exit()
    elif NS==1:
        Radius = [Radius]

    # loop over the set of the discs
    for i in range(NS):
        qi = q_n[2*i]       #
        qip = q_n[2*i+1]    # positions

        Ri = Radius[i] # radius
        DRi = SR[i] # radius of disc i

        # plot grain and radius
        plt.gca().add_patch(mpatches.Circle((qi, qip), DRi, facecolor="none", \
                                              edgecolor="r", linewidth=1)      ) # plot disc

        if ((i % step)==0):
            plt.gca().add_patch(mpatches.Circle((qi, qip),Ri,facecolor="none", \
                                              edgecolor="b", linewidth=1)       ) # plot disc

    # create walls
    ax.plot([-2.*SX, 2.*SX],[0., 0.],'lightgray', label='Walls') # plot bottom

    if(NW>=3):
        ax.plot([SX-2*SR, SX-2*SR],[0., SX-2*SR],'lightgray'); # plot right wall

    if(NW>=2):
        ax.plot([0., 0.],[0., SX-2*SR],'lightgray'); # plot left wall

    plt.axis('equal') # to have axis equal
    ax.set_xlim([XMIN,XMAX])
    ax.set_ylim([YMIN,YMAX])

    # save and close
    namefile = "./dat_files/Plot_rad_"+str(numf)+".png"
    plt.savefig(namefile)
    plt.close(fig)



























