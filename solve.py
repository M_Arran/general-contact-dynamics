"""
file name: solve.py
language: Python3.6
date: 16/06/2018
author: Hugo Martin
email: martin.hugo@live.com
This module corresponds to the time loop.
"""

# gcd imports
from gcd.classes import State, Parameters, Buffer, real_t, uint_t, bool_t
from gcd.integrate import integrate
from gcd.dat_t import save_dat, test_dir, save_en
# other imports
from time import asctime, localtime, clock, time
import numpy as np
from numpy import zeros
from sys import exit

# solve
def solve( Prm, Stn ):
    """
    The function solve launch the time evolution through a time loop until we have did all
    the iterations.

    Arguments:
    Prm : python class = parameters of a gcd_problem,
    Stn : python class = corresponds here to the initial state of a gcd problem.
    """
    print("\n\n\n    *** Global Contact Dynamics Program ***")

    # check classes, directories etc.
    last_check( Prm, Stn )

    # glued beads
    Stn.glued( Prm.GLCRI )

    # save_dat
    save_dat( Stn, np.zeros( Stn.u_n.size , dtype=real_t),   \
                   np.zeros((0,2), dtype=real_t),            \
                   np.zeros((0,2), dtype=real_t), Prm.NAME   )
    Prm.save_rad()

    # initialize a Buffer
    Buff = Buffer( Prm.SD, Prm.NS, Prm.SM )

    # get the hour of loop start
    Eti = start_run( Stn.CTIME )

    # set energies and save it
    Stn.set_en( True, Prm.SM, Prm.Gc, Prm.SLOPE, Buff.vND1 )
    Stn.set_en( False, Prm.SM, Prm.Gc, Prm.SLOPE, Buff.vND1 )
    save_en( True, Stn.CTIME, Stn.E_n, Prm.NAME )

    # time incrementations
    Stn.incrementation( Prm.DT, Prm.DTref, Prm.OSTEP )

    # time step
    time_step( Prm, Stn.u_n )

    while(Stn.CTIME<Prm.FT):                               #
                                                           #

        runprint( Prm, Stn ) # print time

        # integrate from t_n to t_{n+1}                    #
        integrate( Prm, Stn, Buff )                        #
                                                           #
        # time incrementations                             #
        Stn.incrementation( Prm.DT, Prm.DTref, Prm.OSTEP ) #
                                                           #
        # time step                                        #
        time_step( Prm, Stn.u_n )                          #
                                                           #

    # closing
    close_run( Eti )

# runprint
def runprint( Prm, Stn ):
    """
    The function runprint displays information during the execution at each iteration in
    the time loop.
    """

    if Stn.PLOT:
        print("\n>    Current time: %.3e" % Stn.CTIME)
        print("       DT = %.3e" % Prm.DT)
        print("      EPS = %.3e" % Prm.EPS)

# start_run
def start_run( CTIME ):
    """
    The function start_run displays the initial time and save it in exec.log file.

    Arguments:
    CTIME : float = current time value.

    Returns:
    Exectime_ini : float = date and hour of the execution start.
    """

    mon_fichier = open("./exec_info.log", "a")
    string  = "\n\n\n> Start date: " + str(asctime(localtime()))
    print(string)
    print("\n>    Initial time: %.3e" % CTIME)
    Exectime_ini = clock() # save the time at the beginning of the time loop
    mon_fichier.write(string+"\n\n\n")
    mon_fichier.close()

    return Exectime_ini

# close_run
def close_run( Exectime_ini ):
    """
    The function close_run compute the total computation time.
    Arguments:
    Exectime_ini : float = date and hour of the execution start.
    """

    Exectime_final = clock()
    string = "\n\n\n> End date: " + str(asctime(localtime()))
    print(string)
    Nbsec = Exectime_final - Exectime_ini
    h = Nbsec//3600
    mn = Nbsec//60 - h*60
    sec = Nbsec - mn*60 - h*3600
    print("\n> Execution duration:",int(h),"h",int(mn),"mn",int(sec),"s")
    mon_fichier = open("./exec_info.log", "a")
    mon_fichier.write(string)
    mon_fichier.write(   "\n> The execution time duration has been :\n\n>         " + \
                                                str(int(h)) + " h " +                 \
                                               str(int(mn)) + " mn " +                \
                                              str(int(sec)) + " s " + "\n\n\n"        )
    mon_fichier.close()
    print("\n")

# last_check
def last_check( Prm, Stn ):
    """
    The function last_check prints the GCD problem parameters and ask to continue.

    Arguments:
    Prm : python class = parameters of a gcd_problem,
    Stn : python class = corresponds here to the initial state of a gcd problem.
    """
    # check the classes
    Prm.check()
    Stn.check()

    # test dat dir
    test_dir("dat", Prm.NAME)

    # writting a data file
    infos_file = open("./exec_info.log", "w")
    infos_file.write("\n> GCD .log file\n\n> simulation resume:")

    infos_file.write(" "+Prm.NAME+"\n")

    # bodies parameters
    infos_file.write("\n     < number of discs > \n")
    infos_file.write("       %d SD\n" % Prm.SD)
    infos_file.write("       %d NS\n" % Prm.NS)

    # domain parameters
    infos_file.write("\n     < domain parameters > \n")
    infos_file.write("       %d NW\n" % Prm.NW)
    infos_file.write("       %f SX\n" % Prm.SX)
    infos_file.write("       %f SY\n" % Prm.SY)
    infos_file.write("       %f SLOPE\n" % Prm.SLOPE)
    infos_file.write("       %f WV\n" % Prm.WV)

    # discs parameters
    infos_file.write("\n     < discs parameters > \n")
    infos_file.write("       %f RHO\n" % Prm.RHO)
    infos_file.write("       %f MU\n" % Prm.MU)
    infos_file.write("       %f EN\n" % Prm.EN)

    # time parameters
    infos_file.write("\n     < time parameters > \n")
    infos_file.write("       %f DT\n" % Prm.DTref)
    infos_file.write("       %f FT\n" % Prm.FT)

    # EPS and gravity
    infos_file.write("\n     < EPS and gravity > \n")
    infos_file.write("       %f EPS\n" % Prm.EPS)
    infos_file.write("       %f Gc\n" % Prm.Gc)

    # criteria parameters
    infos_file.write("\n     < criteria parameters > \n")
    infos_file.write("       %f GLCRI\n" % Prm.GLCRI)
    infos_file.write("       %f OCRI\n" % Prm.OCRI)

    # display parameters
    infos_file.write("\n     < display parameters > \n")
    infos_file.write("       %d OSTEP\n" % Prm.OSTEP)
    infos_file.write("       %d INFO\n" % Prm.INFO)
    infos_file.write("       %d VARDT\n" % Prm.VARDT)

    infos_file.close()

    # print infos.dat on sreen
    infos_file = open("./exec_info.log", "r")
    file_contents = infos_file.read()
    print(file_contents)
    infos_file.close()

    # last check
    print("> Do you want to start the execution?")
    print("  Enter: y to begin execution,\n         n to exit.")
    REP=input("> ")
    if REP=="y":
        print("\n> Running GCD program start:")
    elif REP=="n":
        print("\n> The execution is stopped.\n\n\n")
        exit()
    else:
        print("\n> Error: Wrong answer value.\n\n\n")
        exit()

# time_step
def time_step( Prm, Uvec ):
    """
    The function time_step changes the value of the time step according to a criteria of
    portion of mean radius over maximal relative normal velocity.
    newDT = max( ( cri*SR / ( max( Bzmat* u_n ) ) ), Prm.ref )

    Arguments:
     Prm : gcd class = the parameters of the problem,
     Stn : gcd class = the state of the problem at time t^n,
    """

    if Prm.VARDT:
        if Prm.SD==2:
            Umax = np.amax(np.sqrt(np.add(\
                        np.square(Uvec[0:2*Prm.NS-1:2]),np.square(Uvec[1:2*Prm.NS:2]))))
        else:
            Umax = np.amax(np.sqrt(np.sum([                        \
                        np.square(Uvec[0:3*Prm.NS-2:3 ]),          \
                        np.square(Uvec[1:3*Prm.NS-1:3 ]),          \
                        np.square(Uvec[2:3*Prm.NS  :3 ])],axis=0)) )

        if Umax>0.:
            newDT = 0.2*np.amin(Prm.SR)/Umax
            newDT = min(newDT, Prm.DTref)
            Prm.DT  = newDT
            Prm.EPS = set_EPS( newDT )

# set_EPS
def set_EPS( DT ):
    # DT = 5.0E-3 => EPS = 1.0E-4
    #      2.5E-3 => EPS = 5.0E-5
    #      1.0E-3 => EPS = 1.0E-5
    #
    #      7.5E-4 => EPS = 7.5E-6
    #      5.0E-4 => EPS = 2.5E-6
    #      2.5E-4 => EPS = 1.0E-6
    #      1.0E-4 => EPS = 5.0E-7
    #
    #      7.5E-5 => EPS = 2.5E-7
    #      5.0E-5 => EPS = 1.0E-7
    #      2.5E-5 => EPS = 7.5E-8
    #      1.0E-5 => EPS = 1.E-8

    #      7.5E-6 => EPS = 7.5E-9
    if ((DT<=5.0E-3)and(DT>=2.5E-3)):
        EPS = 5.0E-5
    elif((DT<2.5E-3)and(DT>=1.0E-3)):
        EPS = 2.0E-5
    elif((DT<1.0E-3)and(DT>=7.5E-4)):
        EPS = 7.5E-6
    elif((DT<7.5E-4)and(DT>=5.0E-4)):
        EPS = 2.5E-6
    elif((DT<5.0E-4)and(DT>=2.5E-4)):
        EPS = 7.5E-7
    elif((DT<2.5E-4)and(DT>=1.0E-4)):
        EPS = 5.0E-7
    elif((DT<1.0E-4)and(DT>=7.5E-5)):
        EPS = 5.E-7
    elif((DT<7.5E-5)and(DT>=5.0E-5)):
        EPS = 1.E-7
    elif((DT<5.0E-5)and(DT>=2.5E-5)):
        EPS = 7.5E-8
    elif((DT<2.5E-5)and(DT>=1.E-5)):
        EPS = 1.E-8
    elif((DT<1.E-5)and(DT>=7.5E-6)):
        EPS = 7.5E-9
    else:
        print("Don't know this time step value: %.3e" % DT)
        exit()

    return EPS





















