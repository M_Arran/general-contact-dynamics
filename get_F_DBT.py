"""
file name: get_F_DBT.py
language: Python3.6
date: 02/04/2018
author: Hugo Martin
email: martin.hugo@live.com
Module for Python functions used to create object using C functions in ./lib/cfun_gcd.so
library.
"""


# C definitions
from gcd.classes import real_t, uint_t, bool_t
import ctypes as ct
import numpy as np
clib = ct.cdll.LoadLibrary('./gcd/lib/gcd_C_func.so') # C library location

cuint   = ct.c_uint    # c unsigned
cint    = ct.c_int     # c int
cdouble = ct.c_double  # c_double
if real_t==np.float64:    # Warning C file must be compiled with corresponding type
    cfloat = ct.c_double  #
elif real_t==np.float32:  # c_float
    print("Attention ca ne marche pas en float32")
    cfloat = ct.c_float   #
else:                     #
    print("Unknown type") #
    raise TypeError       #

v_double = np.ctypeslib.ndpointer(dtype=cfloat, ndim=1, flags='C_CONTIGUOUS') # double array
v_uint = np.ctypeslib.ndpointer(dtype=cuint, ndim=1, flags='C_CONTIGUOUS') # uint array
v_int = np.ctypeslib.ndpointer(dtype=cint, ndim=1, flags='C_CONTIGUOUS') # int array

# imports The imports must be after the C library definition section
from scipy.sparse import coo_matrix, vstack, csr_matrix, triu
from numpy import sum as npsum, sqrt as npsqrt
from sys import exit
from gcd.disp import plot_rad
from time import time

clib.cget_Nbz_NBG.argtypes = (  cuint,
                                v_uint, v_uint,              \
                                cdouble,                     \
                                v_double,                    \
                                v_uint, v_uint,              \
                                cuint,                       \
                                v_double, cdouble,           \
                                cdouble, cdouble, cdouble    ) # cget_Nbz_NBG argtypes

clib.cNewNBGl.argtypes = ( v_uint, v_uint, v_uint,     \
                           v_double,                   \
                           v_double,                   \
                           cuint, cuint, cuint,        \
                           cdouble, cdouble, cdouble   ) # cNewNBGl argtypes

clib.cNew_neigh_rad.argtypes = (v_double,                           \
                                v_double, v_double,                 \
                                cuint, cuint, cuint,                \
                                cdouble, cdouble, cdouble,          \
                                cdouble, cdouble                    ) # cNew_neigh_rad
                                                                               # argtypes

clib.cget_DBT.argtypes = (  v_uint, v_uint, v_uint, v_uint,         \
                            v_double, v_double, v_double,           \
                            v_uint, v_uint, v_uint,                 \
                            v_double,                               \
                            cuint, cuint,                           \
                            v_double, cdouble, cdouble,             \
                            v_uint                                  ) # cget_DBT argtypes

clib.cget_DBT_3D.argtypes = (   v_uint, v_uint, v_uint, v_uint,         \
                                v_double, v_double, v_double,           \
                                v_uint, v_uint, v_uint,                 \
                                v_double,                               \
                                cuint, cuint, v_double,                 \
                                cdouble, cdouble, v_uint                ) # cget_DBT argtypes

clib.wtd_NPro.argtypes = (  cdouble, cuint,                        \
                            v_double,                              \
                            v_int, v_int,                          \
                            cuint,                                 \
                            v_double, cdouble, cdouble, cdouble    ) # wtd_nPro argtypes

clib.cget_DB_Npro.argtypes = (  v_uint, v_uint, v_double, v_double,     \
                                v_uint, v_uint, v_uint,                 \
                                v_double, cdouble,                      \
                                cuint, cuint, cuint, v_double,          \
                                cdouble, cdouble, v_uint                ) # cget_DB_Npro argtypes

# get_DBT
def get_DBT( Stn, Prm, Buff, Fmj ):
    """
    The function get_DBT detects the new contacts and builds the vector of distances D and
    the matrices B et T of this new set of contacts. It then returns the objects D B and T
    with the precedent contacts and with the new ones detected.

    Arguments:
     Cind : 2dim int arrays = Cind = [ indi, indj ] with indi the numbers of discs i and
            shape=(Nbz,2)     indj the number of discs j. The contact number k is then
                              between the discs numbered indi[k] and indj[k], indi and
                              indj are then two arrays of integers,
    Bzmat : sparse float matrix = the matrix B of the gcd model,
    Tzmat : sparse float matrix = the matrix T of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      Nbz : int = number of effective contacts. It is also the size of indi and indj,
     NBG  : True whether we want use a neighbourg array, False otherwise,
     NBGl : sparse matrix = it is a matrix which contains the neighbourgs of each grains in
                            CSS compressed format. Then the neighbourgs of the grain i are
                            referenced in NGBl.indices[NGBl.indptd[i]:NGBl.indptd[i+1]],
                            see scipy.sparse manual for more informations.
    GLUED :   int array = GLUED[i]=1 if bead i is glued, 0 otherwise,
      q_n : float array = position configuration,
      u_n : float array = velocity configuration,
       WH : float = right wall height,
       NS :   int = number of discs,
       NW :   int = number of walls,
       DT : float = time step value,
       SR : float array = radii of discs,
       SX : float = side box length,
       SY : float = side box width,
     OCRI : float = overlap criteria,
     firstime : bool = True whether it is the first time we work for this time iteration,
     CNMEM : bool = True whether we want to start with the last contact network.

    Returns:
     Cind,
    Bzmat,
    Tzmat,
    Dzvec,
      wtd : boolean = work to do, True implies there are new contacts, False otherwise,
      Nbz,
    firstime.

    Definitions
             < ----------- 3ND ----------->
              _                           _
             |    _                    _   |  ^
             |   |                      |  |  | newNbz
             |   | Bzmat (new contacts) |  |  |
             |   |_                    _|  |  v
             |    _                    _   |
             |   |                      |  |  ^
             |   | Bzmat (old contacts) |  |  |
             |   |_                    _|  |  | oldNbz
             |                             |  v
    Bzmat := |    0 ...  ...  ...  ... 0   |  ^
             |    :                    :   |  |
             |    :                    :   |  | newNbz
             |    0 ...  ...  ...  ... 0   |  v
             |    0 ...  ...  ...  ... 0   |  ^
             |    :                    :   |  |
             |    :                    :   |  | oldNbz
             |    0 ...  ...  ...  ... 0   |  v
             |_                           _|

             < ----------- 3ND ----------->
              _                           _
             |    0 ...  ...  ...  ... 0   |  ^
             |    :                    :   |  |
             |    :                    :   |  | newNbz
             |    0 ...  ...  ...  ... 0   |  v
             |    0 ...  ...  ...  ... 0   |  ^
             |    :                    :   |  |
             |    :                    :   |  | oldNbz
             |    0 ...  ...  ...  ... 0   |  v
    Tzmat := |    _                    _   |  ^
             |   |                      |  |  | newNbz
             |   | Tzmat (new contacts) |  |  |
             |   |_                    _|  |  v
             |    _                    _   |
             |   |                      |  |  ^
             |   | Tzmat (old contacts) |  |  |
             |   |_                    _|  |  | oldNbz
             |_                           _|  v

    Dzvec := ( Dzvec(new contacts), Dzvec(old contacts) )
    """

    Cind  = Fmj.Cind   #
    Bzmat = Fmj.Bzmat  #
    Tzmat = Fmj.Tzmat  #
    Dzvec = Fmj.Dzvec  # remove Bzmat, Tzmat and Dzvec
    Nbz   = Fmj.Nbz    #
    wtd   = Fmj.wtd    # Fmj pointers

    Buff3ND = Buff.v3ND # Buffer pointers

    NBGl  = Stn.NBGl  #
    GLUED = Stn.GLUED #
    q_n   = Stn.q_n   #
    u_n   = Stn.u_o   #
    WH    = Stn.WH    # Stn pointers

    SDf  = 2.*Prm.SRf #
    SD   = Prm.SD     #
    NS   = Prm.NS     #
    NW   = Prm.NW     #
    DT   = Prm.DT     #
    SR   = Prm.SR     #
    SX   = Prm.SX     #
    SY   = Prm.SY     #
    OCRI = Prm.OCRI   # Prm pointers

    newNbz, indi, indj = get_indi_j( NBGl, Buff3ND, q_n, u_n, SD, WH, NS, NW, DT, SR, SDf, SX, SY, -1.E-8 )
    wtd = newNbz>0

    if wtd:
        nvB = 2*SD*newNbz             # max number of nonzero values in matrix B
        nvT = SD*(SD+1)*(SD-1)*newNbz # max number of nonzero values in matrix T

        Dvec  = np.zeros(newNbz, dtype=real_t) # allocation memory for distances
        Bi    = np.zeros(nvB, dtype=cuint)     # allocation memory for row indices for B
        Bj    = np.zeros(nvB, dtype=cuint)     # allocation memory for column indices for B
        Bv    = np.zeros(nvB, dtype=real_t)    # allocation memory for nonzero values for B
        Ti    = np.zeros(nvT, dtype=cuint)     # allocation memory for row indices for T
        Tj    = np.zeros(nvT, dtype=cuint)     # allocation memory for column indices for T
        Tv    = np.zeros(nvT, dtype=real_t)    # allocation memory for nonzero values for T
        Nbval = np.zeros(2, dtype=cuint)       # allocation fo number of values

        glued = np.zeros(NS, dtype=cuint) #
        glued[GLUED] = cuint(1)           # to handle glued beads

        if(SD==2):
            numrow = clib.cget_DBT( Bi, Bj, Ti, Tj,         \
                                    Bv, Tv, Dvec,           \
                                    glued, indi, indj,      \
                                    q_n,                    \
                                    NS, newNbz, SR, SX, SY, \
                                    Nbval                   ) # create vector D and matrices B and T of new
                                                              # contacts given by indi and indj
        else:
            numrow = clib.cget_DBT_3D( Bi, Bj, Ti, Tj,         \
                                       Bv, Tv, Dvec,           \
                                       glued, indi, indj,      \
                                       q_n,                    \
                                       NS, newNbz, SR, SX, SY, \
                                       Nbval                   ) # create vector D and matrices B and T of new
                                                                 # contacts given by indi and indj

        nvB = np.asscalar(Nbval[0]) #
        nvT = np.asscalar(Nbval[1]) # real number of values in matrices B and T

        Dvec = np.resize(Dvec, newNbz)
        Bi = np.resize(Bi, nvB) #
        Bj = np.resize(Bj, nvB) #
        Bv = np.resize(Bv, nvB) #
        Ti = np.resize(Ti, nvT) #
        Tj = np.resize(Tj, nvT) #
        Tv = np.resize(Tv, nvT) # resize to the True size of B and T

        if(SD==2):
            # build matrix Bzmat in adding new matrix B to old one. See comment above.
            Bzmat = vstack([                                                                \
                            csr_matrix((Bv, (Bi, Bj)), shape=(newNbz, 3*NS), dtype=real_t), \
                            Bzmat,                                                          \
                            csr_matrix((newNbz,3*NS),dtype=real_t)                          \
                            ],format="csr",dtype=real_t                                     )

            # build matrix Tzmat in adding new matrix T to old one. See comment above.
            Tzmat = vstack([                                            \
                            csr_matrix((2*newNbz,3*NS), dtype=real_t),  \
                            Tzmat                                       \
                            ], format="csr", dtype=real_t               )
            Tzmat = (                                                                         \
                 csr_matrix((Tv,(Ti+Nbz+newNbz,Tj)),shape=(2*(newNbz+Nbz),3*NS),dtype=real_t) \
                 +  Tzmat                                                                     \
                    ).tocsr()
        else:
            Bzmat = vstack([                                                    \
                csr_matrix((Bv, (Bi, Bj)), shape=(newNbz, 6*NS), dtype=real_t), \
                Bzmat,                                                          \
                csr_matrix((2*newNbz,6*NS),dtype=real_t)                        \
                ],format="csr",dtype=real_t                                     )

            Tzmat = vstack([                                \
                csr_matrix((3*newNbz,6*NS), dtype=real_t),  \
                Tzmat                                       \
                ], format="csr", dtype=real_t               )

            Tzmat = (                                                                           \
                 csr_matrix((Tv,(Ti+Nbz+newNbz,Tj)),shape=(3*(newNbz+Nbz),6*NS),dtype=real_t) \
                 +  Tzmat                                                                       \
                    ).tocsr()

        Dzvec = np.hstack((Dvec, Dzvec)) # we add new distances to old ones,
                                         # see above

        # we update the list of contacts
        Cind = np.vstack((np.column_stack((indi,indj)), Cind))

        Nbz = Nbz + newNbz # the new number of contacts

    Fmj.Cind  = Cind  #
    Fmj.Bzmat = Bzmat #
    Fmj.Tzmat = Tzmat #
    Fmj.Dzvec = Dzvec #
    Fmj.Nbz   = Nbz   #
    Fmj.wtd   = wtd   # Fmj pointers

    return newNbz

# get_DB_n_pro
def get_DB_Npro( Prm, Stn, Buff, u_n_fls ):
    """
    The function get_DB_n_pro does exactly the same than the function get_DBT but applied
    to the frictionless problem.
    Arguments:
     NBGl : sparse matrix = it is a matrix which contains the neighbourgs of each grains in
                            CSS compressed format. Then the neighbourgs of the grain i are
                            referenced in NGBl.indices[NGBl.indptd[i]:NGBl.indptd[i+1]],
                            see scipy.sparse manual for more informations.
    GLUED :   int array = GLUED[i]=1 if bead i is glued, 0 otherwise,
      q_n : float array = position configuration,
      u_n : float array = velocity configuration,
       WH : float = right wall height,
       NS :   int = number of discs,
       NW :   int = number of walls,
       DT : float = time step value,
       SR : float array = radii of discs,
       SX : float = side box length,
       SY : float = side box width,
     OCRI : float = overlap criteria,

    Returns:
     Cind : 2dim int arrays = Cind = [ indi, indj ] with indi the numbers of discs i and
            shape=(Nbz,2)     indj the number of discs j. The contact number k is then
                              between the discs numbered indi[k] and indj[k], indi and
                              indj are then two arrays of integers,
    dtBzmat : sparse float matrix = the matrix B of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      wtd : boolean = work to do, True implies there are new contacts, False otherwise,

    """

    SD   = Prm.SD       #
    NS   = Prm.NS       #
    NW   = Prm.NW       #
    DT   = Prm.DT       #
    SR   = Prm.SR       #
    SX   = Prm.SX       #
    SY   = Prm.SY       #
    OCRI = Prm.OCRI     #
    SDf  = 2.*Prm.SRf   #
                        #
    WH = Stn.WH         #
    NBGl = Stn.NBGl     #
    GLUED = Stn.GLUED   #
    q_n = Stn.q_n       #
    u_n = Stn.Uvec      #
                        #
    Buff3ND = Buff.v3ND # pointers
    OCRI  = 0.

    np.multiply(DT, u_n[:3*NS], out=Buff3ND[:3*NS], dtype=real_t )       #
    np.add(q_n[:3*NS], Buff3ND[:3*NS], out=Buff3ND[:3*NS], dtype=real_t) # = q_{n+1} (temporary)
                                                                         # only for translational

    wtd = clib.wtd_NPro(    WH, SD, Buff3ND,            \
                            NBGl.indices, NBGl.indptr,  \
                            NS, SR, SX, SY, OCRI        ) # find if there is a violated contact
    wtd = wtd > 0 # convert wtd into a boolean

    if wtd: # if we must work

        Nbz = NBGl.data.size # this is the number of potential contacts between neighbourgs
        NBGl = NBGl.tocoo()             #
        indi = (NBGl.row).astype(cuint) #
        indj = (NBGl.col).astype(cuint) # get indi and indj from coo format of NBGl

        nvB = 2*SD*Nbz # max number of nonzero values in matrix B

        Dzvec = np.empty(Nbz, dtype=real_t) # allocation memory for distances
        Bi    = np.empty(nvB, dtype=cuint)  # allocation memory for row indices for B
        Bj    = np.empty(nvB, dtype=cuint)  # allocation memory for column indices for B
        Bv    = np.empty(nvB, dtype=real_t) # allocation memory for nonzero values for B

        glued = np.zeros(NS, dtype=cuint) #
        glued[GLUED] = cuint(1)           # to handle glued beads

        NBval = np.zeros(1, dtype=cuint) # NBval initialization

        Nbr = clib.cget_DB_Npro( Bi, Bj, Bv, Dzvec,       \
                                 glued, indi, indj,       \
                                 q_n, WH,                 \
                                 NS, SD, Nbz, SR, SX, SY, \
                                 NBval                    ) # create vector D and matrices
                                                            # B of new contacts given by NBGl

        NBGl = NBGl.tocsr() # convert NBGl in csr format again

        NBval = np.asscalar(NBval)   #
        Bi    = np.resize(Bi, NBval) #
        Bj    = np.resize(Bj, NBval) #
        Bv    = np.resize(Bv, NBval) # extract only values we need

        # build matrix dtBzmat and Dzvec
        dtBzmat = csr_matrix((Bv, (Bi, Bj)), shape=(Nbr, SD*NS), dtype=real_t)
        Dzvec = np.resize(Dzvec, Nbr)

        np.multiply(DT, dtBzmat.data, out=dtBzmat.data, dtype=real_t)

        tmp = np.not_equal(indi, indj) #
        indi = indi[tmp]               #
        indj = indj[tmp]               # remove false contacts

        # we create the list of contacts
        Cind = np.column_stack((indi,indj))
    else:
        Cind = np.zeros((0,2),dtype=uint_t) #
        dtBzmat = np.zeros(0,dtype=real_t)    #
        Dzvec = np.zeros(0,dtype=real_t)    # reset

    return Cind, dtBzmat, Dzvec, wtd

# NewNBGl
def NewNBGl( Prm, Stn, Buff ):
    """
    The function NewNBGl make an update of the sparse matrix Stn.NBGl.

    Arguments:
    Stn : python class = a gcd.State class, see module classes.py.
       NS :   int = number of discs,
       NW :   int = number of walls,
       SR : float array = radii of discs,
       SX : float = side box length,
       SY : float = side box width,
     OCRI : float = overlap criteria,
       DT : float = time step value,
     INFO : bool = True whether we want to plot radius,
     NBNBG : int = number of radius we use to calculate neighbourgs circles.

    Returns:
    Stn
    """

    SD    = Prm.SD    #
    NS    = Prm.NS    #
    NW    = Prm.NW    #
    SR    = Prm.SR    #
    SX    = Prm.SX    #
    SY    = Prm.SY    #
    OCRI  = Prm.OCRI  #
    DT    = Prm.DT    # The used is the reference one
    INFO  = Prm.INFO  #
    #~ NBNBG = Prm.NBNBG # Prm pointers

    Stn.NBGl = None   # remove memory of the old neighbourgs list

    # we are considering translational velocities in Uvec
    # BuffND1 = ( ||Uvec_1||, ||Uvec_2||, ||Uvec_3||, ... )
    np.square( Stn.Uvec[0:SD*NS-(SD-1):SD], out=Buff.vND1, dtype=real_t ) # x
    np.square( Stn.Uvec[(SD-1):SD*NS:SD], out=Buff.vND2, dtype=real_t )   # z get translational Uvec values
    np.add( Buff.vND1, Buff.vND2, out=Buff.vND1, dtype=real_t )
    if SD==3:
        np.square( Stn.Uvec[1:3*NS:3], out=Buff.vND2, dtype=real_t ) # y
        np.add( Buff.vND1, Buff.vND2, out=Buff.vND1, dtype=real_t )
    np.sqrt( Buff.vND1, out=Buff.vND1, dtype=real_t )

    # The reference radius is the distance which is covered by the bead with maximum velocity
    # in 5 time step
    Ref_rad = 2.5*np.amax( SR ) # np.asscalar( np.maximum( NBNBG*MDR, VelM*pif*DT, dtype=real_t ))

    # BuffND2 array initialization, minimimum is 10. MDR
    Buff.vND2[:] = real_t( Ref_rad )

    # compute the radius of neighbourgs for each bead
    Nb_nbg = clib.cNew_neigh_rad(   Buff.vND2,                 \
                                    Buff.vND1, Stn.q_n,        \
                                    NS, SD, NW,                \
                                    SX, SY, OCRI, Ref_rad, DT  ) # Nb_nbg is the max number of
                                                                 # neighbourgs detected

    indi  = np.empty(2*Nb_nbg, dtype=cuint) #
    indj  = np.empty(2*Nb_nbg, dtype=cuint) #
    valij = np.empty(2*Nb_nbg, dtype=cuint) # allocation for indices of new contacts

    # find the neighbourgs and save indices in indi and indj, 1 in valij
    newNbneigh = clib.cNewNBGl( indi, indj, valij,  \
                                Buff.vND2,          \
                                Stn.q_n,            \
                                NS, SD, NW,         \
                                SX, SY, OCRI        )

    indi  = np.resize(  indi, newNbneigh )                       #
    indj  = np.resize(  indj, newNbneigh )                       #
    valij = (np.resize(valij, newNbneigh )).astype(dtype=bool_t) # keep only the part we need

    # the new list of neighbourgs
    Stn.NBGl = csr_matrix((valij,(indi,indj)), shape=(NS,NS+NW), dtype=bool_t)
    Stn.NBGl.eliminate_zeros()

# get_indi_j
def get_indi_j( NBGl, Buff3ND, q_n, u_n, SD, WH, NS, NW, DT, SR, SDf, SX, SY, OCRI ):
    """
    The function get_indi_j is used to create the arrays indi and indj.

    Arguments:
      NBGl : sparse matrix = it is a matrix which contains the neighbourgs of each grains in
                            CSS compressed format. Then the neighbourgs of the grain i are
                            referenced in NGBl.indices[NGBl.indptd[i]:NGBl.indptd[i+1]],
                            see scipy.sparse manual for more informations.
      Buff3ND : float array = Buffer array, same size than q_n
      q_n : float array = position configuration,
      u_n : float array = velocity configuration,
       WH : float = right wall height,
       NS :   int = number of discs,
       NW :   int = number of walls,
       DT : float = time step value,
       SR : float array = radii of discs,
       SX : float = side box length,
       SY : float = side box width,
     OCRI : float = overlap criteria,

    Returns:
      newNbz : integer = number of
    """

    pif = NBGl.nnz # number of neighbourgs
    indi = np.empty(pif, dtype=cuint) #
    indj = np.empty(pif, dtype=cuint) # allocation for indices of new contacts

    np.multiply(DT, u_n[:3*NS], out=Buff3ND[:3*NS], dtype=real_t ) #
    np.add(q_n[:3*NS], Buff3ND[:3*NS], out=Buff3ND[:3*NS], dtype=real_t)  # = q_{n+1} (temporary)
                                                                          # only for translational

    # find the new contacts and save their indices in indi and indj
    newNbz = clib.cget_Nbz_NBG( SD,                           \
                                indi, indj,                   \
                                WH,                           \
                                Buff3ND,                      \
                                (NBGl.indices).astype(cuint), \
                                (NBGl.indptr).astype(cuint),  \
                                NS,                           \
                                SR, SDf, SX, SY, OCRI         )

    if(newNbz>pif):
        print("Number of contact too high and must be changed. pif variable in get_DBT.")
        exit()
    indi = np.resize(indi, newNbz) #
    indj = np.resize(indj, newNbz) # take only the contacts we have detected

    return newNbz, indi, indj























