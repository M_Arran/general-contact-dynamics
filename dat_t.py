"""
file name: dat_t.py
language: Python3.6
date: 02/10/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains the data treatment functions.
"""

"""
IMPORTS
"""
# gcd imports
from gcd.classes import real_t
# imports
import numpy as np
import os
from sys import exit

# save_radius
def save_rad( DR, Sname ):
    """
    The function save_rad is used to write a file with radii.

    Arguments:
    DR : float array = radii of discs.
    """

    np.savetxt("./data/"+Sname+"/dat_files/radii.dat",DR,fmt="%.15f")
    print("\n> radii are saved")

# save_dat
def save_dat( Stn, cf_n, Cind, lambdaz, Sname ):
    """
    The function save_dat is used to save arrays in a file.

    Arguments:
    q_n : float array = position configuration,
    u_n : float array = velocity configuration,
    cf_n : float array = contact forces vector,
    Cind : 2dim int arrays = Cind = [ indi, indj ] with indi the numbers of discs i and
            shape=(Nbz,2)     indj the number of discs j. The contact number k is then
                              between the discs numbered indi[k] and indj[k], indi and
                              indj are then two arrays of integers,
    lambdaz : float array = solution of minimization problem,
    FNUM : int = number of .dat file.
    """

    SD = Stn.SD     #
    q_n  = Stn.q_n  #
    u_n  = Stn.u_n  # Warning: We save the velocity with rebounds
    FNUM = Stn.FNUM # pointers

    save_quf( SD, q_n, u_n, cf_n, FNUM, Sname )
    save_CN( Cind, SD, lambdaz, FNUM, Sname )

    Stn.FNUM = Stn.FNUM + 1

# save_quf
def save_quf( SD, q_n, u_n, cf_n, FNUM, Sname ):
    """
    The function save_quf is used to save arrays in a file.

    Arguments:
    q_n : float array = position configuration,
    u_n : float array = velocity configuration,
    cf_n : float array = contact forces vector,
    FNUM : int = number of .dat file.
    """
    fname = "./data/"+Sname+"/dat_files/GCD_"+str(FNUM)+".dat"

    # a temporary thing to put at the end of the velocity since in 3 dimensions, the size of
    # a position vector for one bead is 7 but 6 for its velocity.
    if SD==3:
        tmp = np.zeros(q_n.size//7, dtype=real_t)
    else:
        tmp = np.zeros(0, dtype=real_t)

    np.savetxt(fname, np.vstack((np.stack((q_n,np.hstack(( u_n, tmp)))), \
                      np.hstack(( cf_n, tmp))))                          \
                    , fmt='%.15f'                                        ) # save file

# load_dat
def load_rad( Sname ):
    """
    The function load_rad loads the ./dat_files/radii.dat file.

    Return:
    DR : float array = radii of discs.
    """

    fname = "./data/"+Sname+"/dat_files/radii.dat"
    DR = np.loadtxt(fname)

    return DR

# load_dat
def load_quf( FNUM, SD, Sname ):
    """
    The function load_file can be used to load positions, velocities and contact forces.
    Arguments:
    FNUM : int = number of GCD dat file.

    Returns:
    NS : int = number of spheres
    q_n : float array = positions,
    u_n : float array = velocities,
    cf_n : float array = contact forces.
    """
    if SD==3:                           #
        qnb = 7                         #
        unb = 6                         #
    elif SD==2:                         #
        qnb = 3                         #
        unb = 3                         # In order to handle the difference in two or
    else:                               # three dimensions
        print("\n> SD must be 2 or 3.") #
        exit()                          #

    name = "./data/"+Sname+"/dat_files/"

    data = np.loadtxt( name+"GCD_"+str(FNUM)+".dat" ) # load data

    q_n  = data[0,:] #
    NS = q_n.size//qnb

    u_n  = np.resize(data[1,:], unb*NS) #
    cf_n = np.resize(data[2,:], unb*NS) # get positions, velocities and contact forces

    return NS, q_n, u_n, cf_n

# save_CN
def save_CN( Cind, SD, lambdaz, FNUM, Sname ):
    """
    The function save_CN writes the contact network Cind in the file Cind.dat and also the
    local value of contact forces lambdaz.

    Argument:
    Cind : 2dim int arrays = Cind = [ indi, indj ] with indi the numbers of discs i and
            shape=(Nbz,2)     indj the number of discs j. The contact number k is then
                              between the discs numbered indi[k] and indj[k], indi and
                              indj are then two arrays of integers,
    lambdaz : float array = solution of minimization problem,
    FNUM : int = number of .dat file.
    """

    fname = "./data/"+Sname+"/dat_files/CN_"+str(FNUM)+".dat"

    Nbz = lambdaz.size//SD

    if Nbz==0:
        np.savetxt( fname, np.zeros(2*SD, dtype=np.int32),fmt="%.15f"         )
    else:
        if SD==2:
            np.savetxt( fname, np.hstack((Cind,np.column_stack((lambdaz[:Nbz], \
                                            lambdaz[Nbz:])))),fmt="%.15f"       )
        else:
            np.savetxt( fname, np.hstack((Cind,np.column_stack((lambdaz[:Nbz], \
                    lambdaz[Nbz:2*Nbz],lambdaz[2*Nbz:])))),fmt="%.15f"       )

# test_dir
def test_dir( string, Sname ):
    """
    The function test_dir creates directories in which we save files.

    Argument:
    string : str = png to create png_files dir or dat to create dat_files dir.
    """

    if string=="png":                      #
        namedir = "./data/"+Sname+"/png_files"  #
        namef = "png"                      #
    elif string=="dat":                    #
        namedir = "./data/"+Sname+"/dat_files"  #
        namef = "dat"                      #
    else:                                  #
        print("\n\tWrong argument value.") #
        exit()                             # png or dat

    if os.path.exists("./data/"+Sname):
        if os.path.exists(namedir):
            print("\n> The directory",namedir,"does already exists.")
            print("  Are you sure to continue?")
            print("  Enter: y to erase all the old",namef,"files,\n         n to exit,")
            print("         k to keep files (not sure: names are not changed).")
            REP=input("> ")
            if REP=="y":
                if len(os.listdir(namedir))>0:
                    os.system("rm "+namedir+"*."+string)
            elif REP=="k":
                print("\n> Files are kept")
            elif REP!="n":
                print("\n> Error: Wrong answer value.\n\n\n")
                exit()
            else:
                print("\n\n")
                exit()
        else:
            os.mkdir(namedir)
    else:
        os.mkdir("./data/"+Sname)
        os.mkdir(namedir)

# save_en
def save_en( option, CTIME, E_n, Sname ):
    """
    The function save_en can be used to save the energy in a file named En.dat
    """

    name = "./data/"+Sname+"/dat_files/En.dat"

    if option:                        #
        mon_fichier = open(name, "w") #
    else:                             # create a new file or add a new line
        mon_fichier = open(name, "a") #

    # save the energies
    mon_fichier.write(str(CTIME)+" "+str(E_n[0])+" "+str(E_n[1])+" "+str(E_n[2])+"\n")
    mon_fichier.close()

    print("       En = Ep + Ec")
    print("     %.3e = %.3e + %.3e" % (E_n[0], E_n[1], E_n[2]))

