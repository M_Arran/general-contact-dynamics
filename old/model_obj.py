"""
file name: model_obj.py
language: Python3.6
date: 13/04/2018
author: Hugo Martin
email: martin.hugo@live.com
Module with Python functions used to create object for GCD model and for minimization
problem solvation.
"""

# imports
import mosek # the optimization solver
import numpy as np
from sys import exit
from scipy.sparse import coo_matrix, eye, hstack, vstack, dia_matrix, csc_matrix, csr_matrix
from math import sqrt, pow
from sys import stdout

# make_obj
def make_pen( Bzmat, Tzmat,       \
              Dzvec,               \
              Nbz,                  \
              Uvec,                  \
              ND,                     \
              DT, iEPS, iDM, iDJ, MU   ):
    """
    The function make_obj makes the objects necessary to create a minimization problem.
    See below for their definitions.

    Arguments:
    Bzmat : sparse float matrix = the matrix B of the gcd model,
    Tzmat : sparse float matrix = the matrix T of the gcd model,
    Dzvec : float array = Dzvec[k] is the distance between discs indi[k] and indj[k],
      Nbz : int = number of effective contacts. It is also the size of indi and indj,
     Uvec : float array = non optimized velocity configuration,
       ND :   int = number of discs,
       DT : float = time step value,
     iEPS : float = inverse of EPS,
      iDM : float = inverse of mass,
      iDJ : float = inverse of inertia moment,
       MU : float = Coulomb's law friction coefficient.

    Returns:
       Az : sparse float matrix = the matrix involved in the linear constraint,
    MiAzt : sparse float matrix = see definition below,
       bz : float array = the vector involved in the linear constraint.

    Definitions:
    The linear coeeficiont in objectivr functional is cz.
    cz := ( 1 0 ... ... 0 ) is directly given in the mosek function

    The linear constraint is
            Az*X <= bz for first Nbz equations,
            Az*X  = bz for last 2 Nbz equations.

    MiAzt := [ M^{-1} * [ B + mu. T ]^t ]

                1  <- 2 Nbz -> <--------2 Nbz--------->
              _                                                        _
             |  0  0.........0                                          |    ^
             |  :  :         :                                          |    |
             |  :  :         :             [ DT^2. B *MiAzt ]           |    | Nbz
             |  :  :         :                                          |    |
             |  :  0.........0                                          |    v
       Az := |  :                                                       |
             |  :  1 0.......0                                          |  ^
             |  :  0 1 0     :                                          |  |
             |  :  : 0 .     : [ DT. T *MiAzt + 1/EPS. DT^2. B *MiAzt ] |  |
             |  :  :     .   :                                          |  | 2 Nbz
             |  :  :       . 0                                          |  |
             |  0  0.......0 1                                          |  v
             |_                                                        _|

       bz := ( DT. B*U - D ,  T*U + 1/EPS. (DT. B*U - D) )
    """

    # MiAzt := [ M^{-1} * [ B + mu. T ]^t ]
    MiAzt = (Bzmat+MU*Tzmat).transpose() # [ B + mu. T ]^t
    Mi = dia_matrix(( np.concatenate((iDM*np.ones(2*ND),iDJ*np.ones(ND)), \
                      axis=0),[0]), shape=(3*ND,3*ND),dtype=np.float64     ) # M^{-1}
    MiAzt = Mi*MiAzt
    del Mi

    # bz := ( DT. B*U - D ,  T*U + 1/EPS. (DT. B*U - D) )
    temp = np.sum([ DT*Bzmat.dot(Uvec), np.concatenate(                     \
                    (-Dzvec,np.zeros(Nbz)),axis=0)],axis=0,dtype=np.float64  ) # DT. B*U - D
    bz = np.concatenate((temp[:Nbz],np.sum([ Tzmat.dot(Uvec),iEPS*temp],axis=0)),axis=0)
    del temp


    # Mateq := [  [ Id_2Nbz ]  , [ DT. T *MiAzt + 1/EPS. DT^2. B *MiAzt ] ]
    #~ Azbis = hstack([ eye(2*Nbz,format="csc"),                                             \
                    #~ (DT*Tzmat*MiAzt + iEPS*(pow(DT,2)*Bzmat*MiAzt)).tocsc() ], format="csr")

    #~ Mateq = hstack([ eye(2*Nbz) ,                                            \
                     #~ DT*Tzmat*MiAzt + iEPS*(pow(DT,2)*Bzmat*MiAzt).tocoo() ]  ).tocoo()
    #~ Az = coo_matrix((Mateq.data,(Mateq.row,Mateq.col+1)),shape=(2*Nbz,4*Nbz+1))
    #~ del Mateq

    #~ # Matineq := [ DT^2. B *MiAzt ]
    #~ Matineq = (pow(DT,2)*Bzmat*MiAzt).tocoo()
    #~ Az = vstack([ coo_matrix((Matineq.data,(Matineq.row,Matineq.col+2*Nbz+1)), \
                  #~ shape=(Nbz,4*Nbz+1)),Az]                                      ).tocoo()
    #~ del Matineq

    #~ Az = hstack([    csc_matrix((3*Nbz,1),dtype=np.float64),                     \
                        #~ vstack([ (pow(DT,2)*Bzmat*MiAzt), Azbis ], format="csc")     \
                                                                        #~ ],format="coo")

    Az = eye(3*Nbz,2*Nbz+1,-Nbz+1,format="csc")
    Az.data[0]=0.
    Az = hstack([ Az,
                                                                                   \
        vstack([ pow(DT,2)*Bzmat*MiAzt, csr_matrix((Nbz,2*Nbz)) ], format="csc") +    \
        vstack([ csr_matrix((Nbz,2*Nbz)), DT*Tzmat*MiAzt+iEPS*(pow(DT,2)*Bzmat*MiAzt) ],format="csc") \
                        ], format="coo" )


    #~ print(Az.toarray()-Azbisbis.toarray())

    #~ rephugo=input()

    return Az, MiAzt, bz

# solve_pen
def solve_pen(Az, bz, Nbz, INFO):
    """
    The function solve_pen returns the solution of the minimization problem which is
    described by penalized model.

    Arguments:
      Az : sparse float matrix = the matrix involved in the linear constraint, see make_obj
                                function,
      bz : float array = the vector involved in the linear constraint, see make_obj function,
     Nbz : int = number of effective contacts. It is also the size of indi and indj,
    INFO : boolean = True whether we want to plot information when solving min problem.

    Retunrs:
    lambdaz : float array = solution of minimization problem.

    Problem description:
    The minimization problem is defined as:

                minimize  cz . (t y lambdaz)
                s.t.
                      Az *(t y lambdaz) <= bz  | (for first Nbz equations)
                      Az *(t y lambdaz)  = bz  | (for last 2 Nbz equations)
                      (t, y) in Quad{ 2Nbz+1 }
                      (lambdaz[k], lambdaz[k+Nbz]) for k = 2Nbz+1 ... 3Nbz+1
    """

    inf = 0.0 # a value for the optimizer
    numvar = 4*Nbz+1 # number of variables
    numcon = 3*Nbz   # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task

            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( Az.row, Az.col, Az.data ) # put Az to task
            del Az

            """ 3/ append cones """
            indfun = (np.linspace(0, 2*Nbz,2*Nbz+1).astype(int)).tolist()
            task.appendcone( mosek.conetype.quad, 0.0, indfun ) # this is the cone for
                                                               # objective function
            for alpha in range(2*Nbz+1, 3*Nbz+1): # cones for Coulomb constraints
                task.appendcone( mosek.conetype.quad, 0.0, [alpha, alpha+Nbz ] )
            del indfun

            """ 4/ append bounds for variables and constraint """
            # type of bounds
            bkc = [ mosek.boundkey.lo ]*Nbz + [ mosek.boundkey.fx ]*2*Nbz
            bkx = [ mosek.boundkey.fr ]*numvar
            # bounds for variables
            task.putboundlist( mosek.accmode.var,                                  \
                               (np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, [inf]*numvar , [inf]*numvar                      )
            del bkx
            # bounds for linear constraints
            task.putboundlist( mosek.accmode.con,                                   \
                               (np.linspace(0,numcon-1,numcon,dtype=int)).tolist(),  \
                               bkc, bz, bz                                            )
            del bz
            del bkc

            """ 5/ input linear objective """
            task.putclist((np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), [0.]*numvar)
            task.putcj(0,1.) # c = ( 1 0 ... ... ... ... 0 )

            """ 6/ solvation """
            task.optimize() # optimize

            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary
            xx = [0.]*numvar                 #
            task.getxx(mosek.soltype.itr,xx) # get the solution value

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                lambdaz = xx[2*Nbz+1:]
                if INFO:
                    if(solsta==mosek.solsta.optimal):
                        print("Optimal solution: %s" % [ xx[0], lambdaz])
                    elif(solsta==mosek.solsta.near_optimal):
                        print("Near optimal solution: %s" % [ xx[0] , lambdaz])
            else:
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                exit() # !!!! Program is stopped when there is infeasibility

    return np.asarray(lambdaz, dtype=np.float64)

# streamprinter
def streamprinter(text):
    '''
    Define a streamer for mosek used for information ploting.
    '''
    stdout.write(text)
    stdout.flush()
















