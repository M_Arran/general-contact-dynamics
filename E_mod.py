"""
file name: E_mod.py
language: Python3.6
date: 02/10/2018
author: Hugo Martin
email: martin.hugo@live.com
This module contains the functions used to take into account an elastic behavior.
"""

"""
IMPORTS
"""
# gcd imports
from gcd.classes import real_t
# other imports
from scipy.sparse import eye, hstack, vstack, csr_matrix, csc_matrix, dia_matrix
import numpy as np
from sys import stdout, exit
import mosek

# E_mod
def E_mod( Prm, Stn, Fmj ):
    """
    The function E_mod creates objects and a task in order to solve an optimization problem and solves it. The optimization problem can be used to take into account the collisions
    and rebonds effects.
    """

    if ((Prm.EN>0.)and(Fmj.Nbz>0)):

        # make elast problem
        vA, vb  = make_el( Prm, Fmj, Stn.u_o, Stn.u_n )

        # solve elast problem
        SDNS = Prm.SD*Prm.NS
        vel = solve_el( vA, vb, Prm.INFO, SDNS )

        # update the elastic velocity
        np.copyto( Stn.u_n[:SDNS], vel ) # translational part
        np.copyto( Stn.u_n[SDNS:], Stn.u_o[SDNS:] ) # rotational part

    else:
        # If there is no elasticity or contacts we just take the optimization velocity.
        np.copyto( Stn.u_n, Stn.u_o )

# make_el
def make_el( Prm, Fmj, u_o, u_n ):
    """
    The function make_el creates the objects necessary to solve the elastic problem.

    Arguments:
    u_o : float array = optimized velocity
    u_n : float array = velocity from the precedent time step

    Returns:
    vA : sparse float matrix = the constraint matrix, see below,
    vb : float array = the constraint vector, see below,

    Description:

    ( 1 ) : Bzmat.u_{n+1}  = - diag( B.u_n ). En

    ( 2 ) : ycone1 = SM^1/2. ( u_{n+1} - u_o )

               tcone    ycone1         vuel

                1    <- SD*NS ->    <- SD*NS ->
              _                                  _
             |  0  0...........0                  |  ^
             |  :  :           :                  |  |
    ( 1 )    |  :  :           :       Bzmat      |  | Nbz
             |  :  :           :                  |  |
             |  0  0...........0                  |  v
             |                                    |
        A:=  |  0  1 0.........0                  |  ^
             |  :  0 1 0       :                  |  |
    ( 2 )    |  :  : 0  .      :                  |  |
             |  :  :       .   :     -SM^1/2      |  | SD*NS
             |  :  :         . 0                  |  |
             |  0  0.........0 1                  |  v
             |_                                  _|

    vb = ( -EN *Bzmat *u_n  , - SM^1/2 *u_o )
    """

    usize = 3*(Prm.SD-1)*Prm.NS                                          #
    Nbz   = Fmj.Nbz                                                      #
    if not Prm.NPRO:                                                     #
        #~ print(Fmj.Bzmat.shape, [Nbz, 2*Nbz], [usize, Prm.SD*Prm.NS])
        Bzmat = eye(Nbz, Prm.SD*Nbz)*Fmj.Bzmat*eye(usize, Prm.SD*Prm.NS)      #
        del Fmj.Bzmat                                                    #
    else:                                                                #
        Bzmat = Fmj.Bzmat                                                #
        np.multiply(1./Prm.DT, Bzmat.data, out=Bzmat.data, dtype=real_t) #
                                                                         #
    sqrtSM = Prm.sqrtSM                                                  #
    SDNS   = Prm.SD*Prm.NS                                               # pointer

    # matrix vA
    vA = eye(Nbz+SDNS,SDNS+1,-Nbz+1,format="csc",dtype=real_t)
    vA.data[0] = real_t(0.)
    vA = hstack([ vA,                                             \
                  vstack([Bzmat[:Nbz,:],csr_matrix((SDNS,SDNS))], \
                  format="csr",dtype=real_t) +                    \
                  vstack([csr_matrix((Nbz,SDNS)),-sqrtSM],        \
                  format="csr",dtype=real_t),                     \
                 ], format="coo"                                  )

    # vector vb
    vb = -Prm.EN*(Bzmat.dot(u_n[:SDNS]))
    vb = np.hstack(( vb, -sqrtSM.dot(u_o[:SDNS])))

    return vA, vb

# solve_el
def solve_el( vA, vb, INFO, SDNS ):
    """
    The problem to solve.

    Arguments:
    vA : float array = linear matrix for conic optimization problem,
    vb : float array = constant vector for conic optimization problem.

    Returns:
    vel : float array = the velocity with the rebonds.

    Description:
    This function solve the following optimization programm:

    min_v || v - u_o ||_M

    s.t.

        B v = -En B.u_n.

    This problm is put under a conic form:

    min_{( t y v )} (1 0...0 0...0). ( t y v )

    s.t.

        (t, y) \in Quad, a quadratic cone
        B.v = -En B.u_n

    """

    inf = 0.0 # a value for the optimizer

    numvar = vA.shape[1] # number of variables
    numcon = vA.shape[0] # number of constraints

    with mosek.Env() as env: # make a MOSEK environment
        env.set_Stream ( mosek.streamtype.log, streamprinter ) # attach a printer
        with env.Task() as task: # create a task
            task.puttaskname("Elastic problem")

            # to get a report of infeasibility if we need it
            task.putintparam( mosek.iparam.infeas_report_auto, mosek.onoffkey.on )

            if INFO: # attach a stream printer to the task
                task.set_Stream( mosek.streamtype.log, streamprinter )

            """ 1/ append variables and constraints """
            task.appendcons( numcon ) # append numcon empty constraints
            task.appendvars( numvar ) # append numvar variables
            task.putobjsense( mosek.objsense.minimize ) # set min or max

            """ 2/ input matrix of linear constraint """
            task.putaijlist( vA.row, vA.col, vA.data ) # put vA to task
            del vA

            """ 3/ append cone """
            indfun = (np.linspace(0, SDNS, SDNS+1).astype(int)).tolist()
            task.appendcone( mosek.conetype.quad, 0.0, indfun ) # this is the cone for
                                                                # objective function
            del indfun

            """ 4/ append bounds for variables and constraint """
            # bounds for variables
            bkx = [ mosek.boundkey.fr ]*numvar
            bkl = [inf]*numvar
            bku = [inf]*numvar
            task.putboundlist( mosek.accmode.var,                                   \
                               (np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), \
                               bkx, bkl , bku                                       )
            del bkx, bkl, bku
            # bounds for linear constraints
            bkc = [ mosek.boundkey.fx ]*numcon
            task.putboundlist( mosek.accmode.con,                                   \
                               (np.linspace(0,numcon-1,numcon,dtype=int)).tolist(), \
                               bkc, vb, vb                                          )
            del vb, bkc

            """ 5/ input linear objective """
            task.putclist((np.linspace(0,numvar-1,numvar,dtype=int)).tolist(), [0.]*numvar)
            task.putcj(0,1.) # c = ( 1 0 ... ... ... ... 0 )

            """ 6/ solvation """
            task.optimize() # optimize

            """ 7/ get solution """
            solsta = task.getsolsta(mosek.soltype.itr) # get the solution summary
            xx = [0.]*numvar                 #
            task.getxx(mosek.soltype.itr,xx) # get the solution value

            """ 8/ prints """
            if INFO:
                task.solutionsummary(mosek.streamtype.msg) # Print a summary containing
                                                           # information about the solution
                                                           # for debugging purposes

            """ 9/ check solution statut """
            if(solsta==mosek.solsta.optimal)or(solsta==mosek.solsta.near_optimal):
                vel = xx[SDNS+1:2*SDNS+1]
            else:
                print("Elast problem")
                if solsta == mosek.solsta.dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_dual_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.near_prim_infeas_cer:
                    print("Primal or dual infeasibility.\n")
                elif solsta == mosek.solsta.unknown:
                    print("Unknown solution status in Elast")
                else:
                    print("Other solution status")
                print("\n> The program is stopped...\n\n\n")
                exit()

    return np.asarray(vel, dtype=real_t)

# streamprinter
def streamprinter(text):
    '''
    Define a streamer for mosek used for information ploting.
    '''
    stdout.write(text)
    stdout.flush()






































