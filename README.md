# General Contact Dynamics

2/3D Discrete Element Modelling of spherical grains, using contact dynamics. Based on the theory of Bertrand Maury, and initially developed by Hugo Martin