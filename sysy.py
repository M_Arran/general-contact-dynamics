"""
file name: sysy.py
language: Python3.6
date: 15/06/2018
author: Hugo Martin
email: martin.hugo@live.com
The module sysy.py can be used to send data files under a special format to Sylvain
 Viroulet the best one.
"""
# gcd imports
from gcd.dat_t import load_quf
# imports
import numpy as np

# sysy_conv_dat
def sysy_conv_dat( FNUM, zeros, j ):
    """
    The function sysy_conv_dat can be used to convert a GCD_*.dat file under a form
    adapted to work with Sysy.

    Arguments:
    FNUM  : int    = number of GCD_dat file,
    zeros : string = string of zeros for sy2_*.dat files.
    j     : int    = number of sy2_*.dat file.
    """
    ND, q_n, u_n, cf_n = load_quf( FNUM, 2) # extract data
    del u_n, cf_n

    sysydat = np.column_stack(( q_n[0:2*ND-1:2], q_n[1:2*ND:2] )) # convert positions

    np.savetxt("./compfiles/gcdpos_"+ str(j) +".dat", sysydat, fmt="%.15e")

# sysy_loop
def sysy_loop( fstart, fend, fstep ):
    """
    The function sysy_loop can be used in order to make a loop over a set of GCD_*.dat
    files and create the sy2_*.dat files.

    Arguments:
    fstart : int = first file number,
      fend : int = last file number,
     fstep : int = step between two file number.
    """

    ztot = len(str(fend))

    print( "\n> Start loop over gcd_*.dat files.\n" )

    j = fstart # number of sy2_*.dat files

    for fnum in range( fstart, fend+1, fstep ):
        zn = ztot - len( str( fnum ) )
        zeros = "0"*zn
        sysy_conv_dat( fnum, zeros, j )
        print( "> File number "+zeros+str(fnum)+" done." )
        j = j+1
